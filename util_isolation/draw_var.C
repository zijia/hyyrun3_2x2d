#include "TLatex.h"
#include "TTree.h"
#include <TLine.h>
#include "/afs/cern.ch/user/z/zijia/MyUtils.h"
#include "/afs/cern.ch/user/z/zijia/AtlasStyle.h"
#include "/afs/cern.ch/user/z/zijia/MyLabels.h"
#include <TSpline.h>
#include <TH2.h>
#include <TStyle.h>
#include <vector>
#include <algorithm>
#include <TCanvas.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include "TLorentzVector.h"
#include "TLegend.h"
#include "TF1.h"
#include "TFile.h"
#include "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.222/InstallArea/x86_64-centos7-gcc8-opt/include/ElectronPhotonSelectorTools/egammaPIDdefs.h"

using namespace std;

int main(int argc, char** argv)
{

        SetAtlasStyle();
        TH1::SetDefaultSumw2();

        TString sample=argv[1];
        TString typeName=argv[2];
        TString region=argv[3];

        TString sampleName = Form("%s", sample.Data());
        TFile * sample_file = new TFile(sampleName, "read");
        TTree * sample_tree = (TTree *) sample_file->Get("SidebandInputs");

        float weight, m_yy, weight_lumiXsec, weightFinal;
        int isPassed;
        float y1_topoetcone20, y2_topoetcone20, y1_ptcone20, y2_ptcone20, y1_pt, y2_pt;
        int y1_isTight, y2_isTight;
        uint y1_isEMTight, y2_isEMTight;
        int cutFlow;
        //enum CutEnum { NxAOD = 0,
        // NDxAOD = 1,
        // ALLEVTS = 2,
        // DUPLICATE = 3,
        // GRL = 4,
        // TRIGGER = 5,
        // DQ = 6,
        // VERTEX = 7,
        // TWO_LOOSE_GAM = 8,
        // AMBIGUITY = 9,
        // TRIG_MATCH = 10,
        // GAM_TIGHTID = 11,
        // GAM_ISOLATION = 12,
        // RELPTCUTS = 13,
        // MASSCUT = 14,
        // PASSALL = 15 };

        sample_tree->SetBranchAddress("weight",&weight);
        sample_tree->SetBranchAddress("weight_lumiXsec",&weight_lumiXsec);
        sample_tree->SetBranchAddress("m_yy",&m_yy);
        sample_tree->SetBranchAddress("isPassed",&isPassed);
        sample_tree->SetBranchAddress("y1_topoetcone20",&y1_topoetcone20);
        sample_tree->SetBranchAddress("y2_topoetcone20",&y2_topoetcone20);
        sample_tree->SetBranchAddress("y1_ptcone20",&y1_ptcone20);
        sample_tree->SetBranchAddress("y2_ptcone20",&y2_ptcone20);
        sample_tree->SetBranchAddress("y1_pt",&y1_pt);
        sample_tree->SetBranchAddress("y2_pt",&y2_pt);
        sample_tree->SetBranchAddress("y1_isTight",&y1_isTight);
        sample_tree->SetBranchAddress("y2_isTight",&y2_isTight);
        sample_tree->SetBranchAddress("y1_isEMTight",&y1_isEMTight);
        sample_tree->SetBranchAddress("y2_isEMTight",&y2_isEMTight);
        sample_tree->SetBranchAddress("cutFlow",&cutFlow);

        TH1F * m_yy_Hist = new TH1F ("m_yy", ";m_{#gamma#gamma};", 55, 105, 160);
        TH1F * y1_topoetcone20_Hist = new TH1F ("y1_topoetcone20", ";E_{T}^{Iso,#gamma1} [GeV];", 50, -10, 10);
        TH1F * y2_topoetcone20_Hist = new TH1F ("y2_topoetcone20", ";E_{T}^{Iso,#gamma2} [GeV];", 50, -10, 10);
        TH1F * y1_ptcone20_Hist = new TH1F ("y1_ptcone20", ";p_{T}^{Iso,#gamma1} [GeV];", 50, 0, 10);
        TH1F * y2_ptcone20_Hist = new TH1F ("y2_ptcone20", ";p_{T}^{Iso,#gamma2} [GeV];", 50, 0, 10);
        TH1F * rel_y1_topoetcone20_Hist = new TH1F ("rel_y1_topoetcone20", ";E_{T}^{Iso,#gamma1} - 0.065p_{T} [GeV];", 50, -10, 10);
        TH1F * rel_y2_topoetcone20_Hist = new TH1F ("rel_y2_topoetcone20", ";E_{T}^{Iso,#gamma2} - 0.065p_{T} [GeV];", 50, -10, 10);
        TH1F * rel_y1_ptcone20_Hist = new TH1F ("rel_y1_ptcone20", ";p_{T}^{Iso,#gamma1} - 0.05p_{T} [GeV];", 50, -10, 10);
        TH1F * rel_y2_ptcone20_Hist = new TH1F ("rel_y2_ptcone20", ";p_{T}^{Iso,#gamma2} - 0.05p_{T} [GeV];", 50, -10, 10);
        TH2F * y1_y2_topoetcone20_Hist = new TH2F ("y1_y2_topoetcone20", ";E_{T}^{Iso,#gamma1} [GeV];E_{T}^{Iso,#gamma2} [GeV]", 50, -10, 10, 50, -10, 10);
        TH2F * y1_y2_ptcone20_Hist = new TH2F ("y1_y2_ptcone20", ";p_{T}^{Iso,#gamma1} [GeV];p_{T}^{Iso,#gamma2} [GeV]", 50, -10, 10, 50, -10, 10);
        TH2F * y1_y2_rel_topoetcone20_Hist = new TH2F ("y1_y2_rel_topoetcone20", ";E_{T}^{Iso,#gamma1}/p_{T};E_{T}^{Iso,#gamma2}/p_{T}", 50, -0.5, 0.5, 50, -0.5, 0.5);
        TH2F * y1_y2_rel_ptcone20_Hist = new TH2F ("y1_y2_rel_ptcone20", ";p_{T}^{Iso,#gamma1}/p_{T};p_{T}^{Iso,#gamma2}/p_{T}", 50, -0.5, 0.5, 50, -0.5, 0.5);

        int n_count=0;
        Int_t sample_numev = sample_tree->GetEntries();
        for( int ievv = 0; ievv < sample_numev; ievv++ )
        {
                sample_tree->GetEntry(ievv);
                weightFinal = weight * weight_lumiXsec;


                int y1_isIso = (y1_topoetcone20<y1_pt*0.065 && y1_ptcone20<y1_pt*0.05 )?1:0;
                int y2_isIso = (y2_topoetcone20<y2_pt*0.065 && y2_ptcone20<y2_pt*0.05 )?1:0;


                // ==================================================================================================================================
                // ================= for drawing isolation variables, simply look at 'Loose' cuts, ignore the following lines =======================
                // ==================================================================================================================================
                const unsigned int PhotonLoosePrime5 = egammaPID::PhotonLoose |
                        0x1 << egammaPID::ClusterStripsDeltaEmax2_Photon |  // = "kept for legacy reason"
                        0x1 << egammaPID::ClusterStripsEratio_Photon;       // = "f1"

                const unsigned int PhotonLoosePrime4 = PhotonLoosePrime5 |
                        0x1 << egammaPID::ClusterStripsWtot_Photon;         // = "Wstot"

                const unsigned int PhotonLoosePrime3 = PhotonLoosePrime4 |
                        0x1 << egammaPID::ClusterStripsDEmaxs1_Photon;      //  = "Eratio"

                const unsigned int PhotonLoosePrime2 = PhotonLoosePrime3 |
                        0x1 << egammaPID::ClusterStripsDeltaE_Photon;
                uint gam1_isTight = y1_isEMTight;
                uint gam2_isTight = y2_isEMTight;


                if (typeName.Contains("SR")) {
                        if (y1_isTight*y2_isTight*y1_isIso*y2_isIso==0) continue;
                        n_count++;
                }
                else if (typeName.Contains("GJ")) {
                        if (y1_isTight*y2_isTight!=0) continue;
                        if (y1_isIso*y2_isIso==0) continue;
                        n_count++;
                }
                else if (typeName.Contains("JJ")) {
                        if (y1_isTight+y2_isTight>0) continue;
                        if (y1_isIso*y2_isIso==0) continue;
                        n_count++;
                }
                else if (typeName.Contains("LP2")) {
                        if (y1_isIso*y2_isIso==0) continue;
                        uint isEMloose = PhotonLoosePrime2;
                        bool loose1 = !(gam1_isTight & isEMloose); // loose = LP2
                        bool loose2 = !(gam2_isTight & isEMloose); 
                        if (typeName.Contains("GG")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("GJ")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        else if (typeName.Contains("JG")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("JJ")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        n_count++;
                }
                else if (typeName.Contains("LP3")) {
                        if (y1_isIso*y2_isIso==0) continue;
                        uint isEMloose = PhotonLoosePrime3;
                        bool loose1 = !(gam1_isTight & isEMloose); // loose = LP3
                        bool loose2 = !(gam2_isTight & isEMloose); 
                        if (typeName.Contains("GG")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("GJ")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        else if (typeName.Contains("JG")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("JJ")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        n_count++;
                }
                else if (typeName.Contains("LP4")) {
                        if (y1_isIso*y2_isIso==0) continue;
                        uint isEMloose = PhotonLoosePrime4;
                        bool loose1 = !(gam1_isTight & isEMloose); // loose = LP4
                        bool loose2 = !(gam2_isTight & isEMloose); 
                        if (typeName.Contains("GG")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("GJ")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        else if (typeName.Contains("JG")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("JJ")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        n_count++;
                }
                else if (typeName.Contains("LP5")) {
                        if (y1_isIso*y2_isIso==0) continue;
                        uint isEMloose = PhotonLoosePrime5;
                        bool loose1 = !(gam1_isTight & isEMloose); // loose = LP5
                        bool loose2 = !(gam2_isTight & isEMloose); 
                        if (typeName.Contains("GG")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("GJ")) {
                                if (!(loose1==1&&y1_isTight==1)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        else if (typeName.Contains("JG")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==1)) continue;
                        }
                        else if (typeName.Contains("JJ")) {
                                if (!(loose1==1&&y1_isTight==0)) continue;
                                if (!(loose2==1&&y2_isTight==0)) continue;
                        }
                        n_count++;
                }
                // ==================================================================================================================================
                // ================= for drawing isolation variables, simply look at 'Loose' cuts, ignore the above lines ===========================
                // ==================================================================================================================================
                else if (typeName.Contains("Loose")) {
                        n_count++;
                }

                m_yy_Hist->Fill(0.001*m_yy, weightFinal);
                y1_topoetcone20_Hist->Fill(0.001*y1_topoetcone20, weightFinal);
                y2_topoetcone20_Hist->Fill(0.001*y2_topoetcone20, weightFinal);
                y1_ptcone20_Hist->Fill(0.001*y1_ptcone20, weightFinal);
                y2_ptcone20_Hist->Fill(0.001*y2_ptcone20, weightFinal);
                rel_y1_topoetcone20_Hist->Fill(0.001*(y1_topoetcone20 - 0.065*y1_pt), weightFinal);
                rel_y2_topoetcone20_Hist->Fill(0.001*(y2_topoetcone20 - 0.065*y2_pt), weightFinal);
                rel_y1_ptcone20_Hist->Fill(0.001*(y1_ptcone20 - 0.05*y1_pt), weightFinal);
                rel_y2_ptcone20_Hist->Fill(0.001*(y2_ptcone20 - 0.05*y2_pt), weightFinal);
                y1_y2_topoetcone20_Hist->Fill(0.001*y1_topoetcone20,0.001*y2_topoetcone20, weightFinal);
                y1_y2_ptcone20_Hist->Fill(0.001*y1_ptcone20,0.001*y2_ptcone20, weightFinal);
                y1_y2_rel_topoetcone20_Hist->Fill(y1_topoetcone20/y1_pt,y2_topoetcone20/y2_pt, weightFinal);
                y1_y2_rel_ptcone20_Hist->Fill(y1_ptcone20/y1_pt,y2_ptcone20/y2_pt, weightFinal);


        }
        cout<<typeName<<" passed Event = "<<n_count<<endl;

        TFile * file = new TFile("Hist/"+region+"_"+typeName+".root", "recreate");
        file->cd();

        m_yy_Hist->Write();
        y1_topoetcone20_Hist->Write();
        y2_topoetcone20_Hist->Write();
        y1_ptcone20_Hist->Write();
        y2_ptcone20_Hist->Write();
        rel_y1_topoetcone20_Hist->Write();
        rel_y2_topoetcone20_Hist->Write();
        rel_y1_ptcone20_Hist->Write();
        rel_y2_ptcone20_Hist->Write();
        y1_y2_topoetcone20_Hist->Write();
        y1_y2_ptcone20_Hist->Write();
        y1_y2_rel_topoetcone20_Hist->Write();
        y1_y2_rel_ptcone20_Hist->Write();

        file->Close();

        return 0;

}
