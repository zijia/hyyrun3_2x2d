#include "TLatex.h"
#include "TTree.h"
#include <TLine.h>
#include "/afs/cern.ch/user/z/zijia/MyUtils.h"
#include "/afs/cern.ch/user/z/zijia/AtlasStyle.h"
#include "/afs/cern.ch/user/z/zijia/MyLabels.h"
#include <TSpline.h>
#include <TH2.h>
#include <TStyle.h>
#include <vector>
#include <algorithm>
#include <TCanvas.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include "TLorentzVector.h"
#include "TLegend.h"
#include "TF1.h"
#include "TFile.h"
#include "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.222/InstallArea/x86_64-centos7-gcc8-opt/include/ElectronPhotonSelectorTools/egammaPIDdefs.h"

using namespace std;

int main(int argc, char** argv)
{

        SetAtlasStyle();
        TH1::SetDefaultSumw2();

        TString sample=argv[1];
        TString typeName=argv[2];
        TString region=argv[3];

        TString sampleName = Form("%s", sample.Data());
        TFile * sample_file = new TFile(sampleName, "read");
        TTree * sample_tree = (TTree *) sample_file->Get("skimTree");

        float weightFinal, m_yy;
        int y1_parentPdgId, y2_parentPdgId;
        int y1_matched, y2_matched;
        int y1_conversionType, y2_conversionType;
        float y1_eta, y2_eta;
        float y1_scaleFactor, y2_scaleFactor;

        sample_tree->SetBranchAddress("weightFinal",&weightFinal);
        sample_tree->SetBranchAddress("m_yy",&m_yy);
        sample_tree->SetBranchAddress("y1_parentPdgId",&y1_parentPdgId);
        sample_tree->SetBranchAddress("y2_parentPdgId",&y2_parentPdgId);
        sample_tree->SetBranchAddress("y1_matched",&y1_matched);
        sample_tree->SetBranchAddress("y2_matched",&y2_matched);
        sample_tree->SetBranchAddress("y1_conversionType",&y1_conversionType);
        sample_tree->SetBranchAddress("y2_conversionType",&y2_conversionType);
        sample_tree->SetBranchAddress("y1_eta",&y1_eta);
        sample_tree->SetBranchAddress("y2_eta",&y2_eta);
        sample_tree->SetBranchAddress("y1_scaleFactor",&y1_scaleFactor);
        sample_tree->SetBranchAddress("y2_scaleFactor",&y2_scaleFactor);

        TH1F * y1_matched_Hist = new TH1F ("y1_matched", ";leading photon Truth match status;", 2, -0.5, 1.5);
        TH1F * y2_matched_Hist = new TH1F ("y2_matched", ";subleading photon Truth match status;", 2, -0.5, 1.5);
        TH1F * y1_eta_conv_Hist = new TH1F ("y1_eta_conv", ";leading photon |#eta| conv.;", 20, 0, 2.5);
        TH1F * y2_eta_conv_Hist = new TH1F ("y2_eta_conv", ";subleading photon |#eta| conv.;", 20, 0, 2.5);
        TH1F * yy_eta_conv_Hist = new TH1F ("yy_eta_conv", ";photon |#eta| conv.;", 20, 0, 2.5);
        TH1F * y1_eta_unconv_Hist = new TH1F ("y1_eta_unconv", ";leading photon |#eta| unconv.;", 20, 0, 2.5);
        TH1F * y2_eta_unconv_Hist = new TH1F ("y2_eta_unconv", ";subleading photon |#eta| unconv.;", 20, 0, 2.5);
        TH1F * yy_eta_unconv_Hist = new TH1F ("yy_eta_unconv", ";photon |#eta| unconv.;", 20, 0, 2.5);
        TH1F * y1_frac_Hist = new TH1F ("y1_frac", ";leading photon conv. frac;", 20, 0, 2.5);
        TH1F * y2_frac_Hist = new TH1F ("y2_frac", ";subleading photon conv. frac;", 20, 0, 2.5);
        TH1F * yy_frac_Hist = new TH1F ("yy_frac", ";photon conv. frac;", 20, 0, 2.5);
        TH1F * y1_SF_conv_Hist = new TH1F ("y1_SF_conv", ";leading photon SF conv.;", 20, 0.8, 1.2);
        TH1F * y2_SF_conv_Hist = new TH1F ("y2_SF_conv", ";subleading photon SF conv.;", 20, 0.8, 1.2);
        TH1F * yy_SF_conv_Hist = new TH1F ("yy_SF_conv", ";photon SF conv.;", 20, 0.8, 1.2);
        TH1F * y1_SF_unconv_Hist = new TH1F ("y1_SF_unconv", ";leading photon SF unconv.;", 20, 0.8, 1.2);
        TH1F * y2_SF_unconv_Hist = new TH1F ("y2_SF_unconv", ";subleading photon SF unconv.;", 20, 0.8, 1.2);
        TH1F * yy_SF_unconv_Hist = new TH1F ("yy_SF_unconv", ";photon SF unconv.;", 20, 0.8, 1.2);

        Int_t sample_numev = sample_tree->GetEntries();
        for( int ievv = 0; ievv < sample_numev; ievv++ )
        {
                sample_tree->GetEntry(ievv);
                //cout<<weightFinal<<endl;

                y1_matched_Hist->Fill(y1_matched, weightFinal);
                y2_matched_Hist->Fill(y2_matched, weightFinal);

                if (y1_conversionType!=0) {
                        y1_eta_conv_Hist->Fill(fabs(y1_eta), weightFinal);
                        yy_eta_conv_Hist->Fill(fabs(y1_eta), weightFinal);
                        y1_SF_conv_Hist->Fill(y1_scaleFactor, weightFinal);
                        yy_SF_conv_Hist->Fill(y1_scaleFactor, weightFinal);
                }
                else if (y1_conversionType==0){
                        y1_eta_unconv_Hist->Fill(fabs(y1_eta), weightFinal);
                        yy_eta_unconv_Hist->Fill(fabs(y1_eta), weightFinal);
                        y1_SF_unconv_Hist->Fill(y1_scaleFactor, weightFinal);
                        yy_SF_unconv_Hist->Fill(y1_scaleFactor, weightFinal);
                }
                if (y2_conversionType!=0) {
                        y2_eta_conv_Hist->Fill(fabs(y2_eta), weightFinal);
                        yy_eta_conv_Hist->Fill(fabs(y2_eta), weightFinal);
                        y2_SF_conv_Hist->Fill(y2_scaleFactor, weightFinal);
                        yy_SF_conv_Hist->Fill(y2_scaleFactor, weightFinal);
                }
                else if (y2_conversionType==0){
                        y2_eta_unconv_Hist->Fill(fabs(y2_eta), weightFinal);
                        yy_eta_unconv_Hist->Fill(fabs(y2_eta), weightFinal);
                        y2_SF_unconv_Hist->Fill(y2_scaleFactor, weightFinal);
                        yy_SF_unconv_Hist->Fill(y2_scaleFactor, weightFinal);
                }

        }
        y1_frac_Hist->Add(y1_eta_conv_Hist, y1_eta_unconv_Hist, 1, 1);
        y2_frac_Hist->Add(y2_eta_conv_Hist, y2_eta_unconv_Hist, 1, 1);
        yy_frac_Hist->Add(yy_eta_conv_Hist, yy_eta_unconv_Hist, 1, 1);
        y1_frac_Hist->Divide(y1_eta_conv_Hist, y1_frac_Hist, 1, 1);
        y2_frac_Hist->Divide(y2_eta_conv_Hist, y2_frac_Hist, 1, 1);
        yy_frac_Hist->Divide(yy_eta_conv_Hist, yy_frac_Hist, 1, 1);

        TFile * file = new TFile("Hist/"+region+"_"+typeName+".root", "recreate");
        file->cd();

        y1_matched_Hist->Write();
        y2_matched_Hist->Write();
        y1_frac_Hist->Write();
        y2_frac_Hist->Write();
        yy_frac_Hist->Write();
        y1_SF_conv_Hist->Write();
        y2_SF_conv_Hist->Write();
        yy_SF_conv_Hist->Write();
        y1_SF_unconv_Hist->Write();
        y2_SF_unconv_Hist->Write();
        yy_SF_unconv_Hist->Write();

        file->Close();

        return 0;

}
