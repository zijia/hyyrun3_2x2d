#ifndef HGamBackgroundStudies_DiPhotonBkg2x2DSB_H
#define HGamBackgroundStudies_DiPhotonBkg2x2DSB_H

#include "HGamBackgroundStudies/Includes.h"
#include <EventLoop/Algorithm.h>
#include <HGamBackgroundStudies/SB2x2DParameters.h>
#include <HGamBackgroundStudies/SB2x2D.h>
#include <HGamBackgroundStudies/SB2x2DTruth.h>
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include <TKey.h>

using namespace SB;

struct SBInputs{
  float weight,pileupWeight,vertexWeight, yybb_lowMass_weight,
    weightInitial,weightCatCoupBDT,weight_XsecLumi, weightcatXS_VBF,
    weightcatXS_lepton, weightjvt_30, 
    weightcatXS_ttH, weight_catXS_Nlep,HiggsHF_weight,
    pT_y1,pT_y2,y1_pt,y1_eta,y1_phi,y1_m,
    y2_pt,y2_eta,y2_phi,y2_m,
    mu,
    y1_topoetcone20,y2_topoetcone20,
    y1_ptcone20,y2_ptcone20;
  int catCoup_dev,trig_loose,trig_medium,catHiggsHF,
    trig_2g50,y1_convType,y2_convType,
    y1_IsIso,y2_IsIso,y1_isTight,y2_isTight,
    y1_isTight_noFF,y2_isTight_noFF;
  int isPassedMassCut, isPassedRelPtCuts, isPassedTrigMatch_Loose, runNumber, catXS_VBF;
  unsigned int y1_isEMTight,y2_isEMTight;
  unsigned int y1_isEMTight_noFF,y2_isEMTight_noFF;  
  ///////////////////
  TChain *inTree;
  map<Str, TFile*> infiles;
  TH1D *cutFlow;
  vector<TH1D*> cutFlow_orig;
  map<Str,float> fMap;
  map<Str,int> iMap;
  ///////////////////
  Str name, type;
  int sol_mode;
  bool isMC;
  int pair_IsSignal;
  Str catMC;
  ///////////////////
  vector<int*> pointers;
  vector<SB2x2DTruth*> SB_truth;
  vector<SB2x2D*> SB_data;
};


class DiPhotonBkg2x2DSB : public EL::Algorithm
{
  
 public:

  void fatal(Str msg){printf("ERROR: %s ... aborting!",msg.Data()); abort();}
  
 public:

  TFile *m_fout; //!
  
  DiPhotonBkg2x2DSB (Str name="");

  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode finalize ();

  //////////////////////////////
  
  virtual EL::StatusCode SolveData(SBInputs &inSBdata, SBInputs &inSBtruth);
  virtual EL::StatusCode initializeInputTree (SBInputs &SBin);
  virtual EL::StatusCode initDataSideBand (SBInputs &inSB, Str type);
  virtual EL::StatusCode initMCSideBand (SBInputs &inSB, Str type);
  //virtual EL::StatusCode initMCSideBandFromFile ( SBInputs &inSB, TFile*f, Str type);
  virtual EL::StatusCode WriteMCResults (SBInputs &inSB);
  template <typename SB >
    EL::StatusCode fillSideBand (SBInputs &inSB, SB &SideBands, Str fillType);  
  virtual EL::StatusCode saveSystGraphs (SB2x2D *sb, Str systN);
  virtual EL::StatusCode saveAllGraphs(SB2x2D *sb);

  //////////////////////////////
  
  SBInputs SBin_data, SBin_data_lp5, SBin_data_lp4, SBin_data_lp3 ,SBin_data_lp2, SBin_data_gen, SBin_data_noFF;

  SBInputs SBin_mcNom, SBin_mc_lp5, SBin_mc_lp4, SBin_mc_lp3, SBin_mc_lp2;
  SBInputs SBin_mcSyst, SBin_mcFullSim;
  
  double myy_low, myy_high;
  bool doRenormalize;
    
 private:

  inline void setConfigVal(Str key, Str val){ m_config->SetValue(key,val); };
  inline bool confHasVal(Str key) { return m_config->Defined(key); }
  inline bool confHasEqualVal(Str key, Str val){ return m_config->GetValue(key,"")==val; } 
  void addCfgFile(Str conf);

  inline void resetBranches(SBInputs &inSB) {
    for (auto &e:inSB.iMap) e.second = -99;
    for (auto &e:inSB.fMap) e.second = -99.0;
  }
  
  TEnv *m_config;

  bool existingMCInputs;
  
  StrV inputs_F, inputs_I;
        
  StrV histo_names;
  map<Str,Str> histo_vars;
  map<Str,Str> histo_2dSel;
  map<Str,VecI> histo_2dSel_catIndex;
  map<Str,float> var_Scale;
  map<Str,VecD> var_bins;
  
  
  /////////////////////////////////
  
  void SetupInt(TTree &sample, Str bname, int &var)
  { if(!sample.FindBranch(bname))
      { fatal(Form("Cannot find branch: %s",bname.Data())); }
    sample.SetBranchStatus(bname,1); sample.SetBranchAddress(bname, &var); }
  
  void SetupUInt(TTree &sample,Str bname, unsigned int &var)
  { if(!sample.FindBranch(bname))
      { printf("Cannot find branch: %s \n",bname.Data()); abort(); }
    sample.SetBranchStatus(bname,1); sample.SetBranchAddress(bname, &var); }
  
  void SetupFloat(TTree &sample, Str bname, float &var)
  { if(!sample.FindBranch(bname))
      { printf("Cannot find branch: %s \n",bname.Data()); abort(); }
    sample.SetBranchStatus(bname,1); sample.SetBranchAddress(bname, &var); }

 public:

  ClassDef(DiPhotonBkg2x2DSB, 1);

};

#endif
