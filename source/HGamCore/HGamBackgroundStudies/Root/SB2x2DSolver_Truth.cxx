#include "HGamBackgroundStudies/SB2x2DSolver.h"

#define SQUARE(x) ((x)*(x))

namespace SB {

double SB2x2DSolver::GetEfficiency1IX1XI_Truth()
{
  double div = nAA + nBA;
  if(div==0) return 1;
  double num = nAA;
  return num/div;
}

double SB2x2DSolver::GetEfficiency2IX2XI_Truth()
{
  double div = nAA + nAB;
  if(div==0) return 1;
  double num = nAA;
  return num/div;
}

double SB2x2DSolver::GetEfficiency1TX1XT_Truth()
{
  double div = nAA + nCA;
  if(div==0) return 1;
  double num = nAA;
  return num/div;
}

double SB2x2DSolver::GetEfficiency2TX2XT_Truth()
{
  double div = nAA + nAC;
  if(div==0) return 1;
  double num = nAA;
  return num/div;
}

double SB2x2DSolver::GetEfficiency1IXI_Truth()
{// e1I
  double div = nXITI + nXNTI;
  if(div==0) return 1;
  double num = nXITI;
  return num/div;
}

double SB2x2DSolver::GetEfficiency2IXI_Truth()
{// e2I 2T:X
  double div = nTIXI + nTIXN;
  if(div==0) return 1;
  double num = nTIXI;
  return num/div;
}

double SB2x2DSolver::GetEfficiency1TXT_Truth()
{// e1T
  double div = nNXTI + nTXTI;
  if(div==0) return 1;
  double num = nTXTI;
  return num/div;
}

double SB2x2DSolver::GetEfficiency2TXT_Truth()
{// e2T
  double div = nTINX + nTITX;
  if(div==0) return 1;
  double num = nTITX;
  return num/div;
}

double SB2x2DSolver::GetEfficiency1I_Truth()
{// ep1I
  double div = n1I + n1N;
  if(div==0) return 1;
  double num = n1I;
  return num/div;
}

double SB2x2DSolver::GetEfficiency2I_Truth()
{// ep2I
  double div = n2I + n2N;
  if(div==0) return 1;
  double num = n2I;
  return num/div;
}

double SB2x2DSolver::GetCorrelationI_Truth()
{// XIGG = e(g1g2) / (eg1*eg2) . same div for e = (nXIXI + nXIXN + nXNXI + nXNXN)
  double div = (nXIXI + nXIXN)*(nXIXI + nXNXI);
  if(div==0) return 1;
  double num = nXIXI*(nXIXI + nXIXN + nXNXI + nXNXN);
  return num/div;
}

double SB2x2DSolver::GetCorrelation1_Truth()
{
  double div = (n1NI + n1TI)*(n1TI + n1TN);
  if(div==0) return 1;
  double num = n1TI*(n1NI + n1NN + n1TI + n1TN);
  return num/div;
}

double SB2x2DSolver::GetCorrelation2_Truth()
{
  double div = (n2NI + n2TI)*(n2TI + n2TN);
  if(div==0) return 1;
  double num = n2TI*(n2NI + n2NN + n2TI + n2TN);
  return num/div;
}

double SB2x2DSolver::GetCorrelationT_Truth()
{
  double div = (nNXTX + nTXTX)*(nTXNX + nTXTX);
  if(div==0) return 1;
  double num = nTXTX*(nNXNX + nNXTX + nTXNX + nTXTX);
  return num/div;
}

double SB2x2DSolver::GetEfficiency1T_Truth()
{// ep1T
  double div = n1L + n1T;
  if(div==0) return 1;
  double num = n1T;
  return num/div;
}

double SB2x2DSolver::GetEfficiency2T_Truth()
{// ep1T
  double div = n2L + n2T;
  if(div==0) return 1;
  double num = n2T;
  return num/div;
}

double SB2x2DSolver::GetEfficiency1IXIError2_Truth()
{
  double div = nXITI + nXNTI;
  if(div==0) return 0;
  div *= div;
  double dn1 = nXNTI/div;
  double dn2 = -(nXITI/div);
  double w1  = w2nAA + w2nCA;
  double w2  = w2nBA + w2nDA;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency2IXIError2_Truth()
{
  double div = nTIXI + nTIXN;
  if(div==0) return 0;
  div *= div;
  double dn1 = nTIXN/div;
  double dn2 = -(nTIXI/div);
  double w1  = w2nAA + w2nAC;
  double w2  = w2nAB + w2nAD;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency1TXTError2_Truth()
{
  double div = nNXTI + nTXTI;
  if(div==0) return 0;
  div *= div;
  double dn1 = nNXTI/div;
  double dn2 = -(nTXTI/div);
  double w1  = w2nAA + w2nBA;
  double w2  = w2nCA + w2nDA;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency2TXTError2_Truth()
{
  double div = nTINX + nTITX;
  if(div==0) return 0;
  div *= div;
  double dn1 = nTINX/div;
  double dn2 = -(nTITX/div);
  double w1  = w2nAA + w2nAB;
  double w2  = w2nAC + w2nAD;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency1IX1XIError2_Truth()
{
  double div = nAA + nBA;
  if(div==0) return 0;
  div *= div;
  double dn1 = nBA/div;
  double dn2 = -(nAA/div);
  double w1  = w2nAA;
  double w2  = w2nBA;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency2IX2XIError2_Truth()
{
  double div = nAA + nAB;
  if(div==0) return 0;
  div *= div;
  double dn1 = nAB/div;
  double dn2 = -(nAA/div);
  double w1  = w2nAA;
  double w2  = w2nAB;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency1TX2XTError2_Truth()
{
  double div = nAA + nCA;
  if(div==0) return 0;
  div *= div;
  double dn1 = nCA/div;
  double dn2 = -(nAA/div);
  double w1  = w2nAA;
  double w2  = w2nCA;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency2TX2XTError2_Truth()
{
  double div = nAA + nAC;
  if(div==0) return 0;
  div *= div;
  double dn1 = nAC/div;
  double dn2 = -(nAA/div);
  double w1  = w2nAA;
  double w2  = w2nAC;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency1IError2_Truth()
{
  double div = n1I + n1N;
  if(div==0) return 0;
  div *= div;
  double dn1 = n1N/div;
  double dn2 = -(n1I/div);
  double w1  = w2nAA + w2nAB + w2nAC + w2nAD + w2nCA + w2nCB + w2nCC + w2nCD;
  double w2  = w2nBA + w2nBB + w2nBC + w2nBD + w2nDA + w2nDB + w2nDC + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency2IError2_Truth()
{
  double div = n2I + n2N;
  if(div==0) return 0;
  div *= div;
  double dn1 = n2N/div;
  double dn2 = -(n2I/div);
  double w1  = w2nAA + w2nAC + w2nBA + w2nBC + w2nCA + w2nCC + w2nDA + w2nDC;
  double w2  = w2nAB + w2nAD + w2nBB + w2nBD + w2nCB + w2nCD + w2nDB + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency1TError2_Truth()
{
  double div = n1L + n1T;
  if(div==0) return 0;
  div *= div;
  double dn1 = n1L/div;
  double dn2 = -(n1T/div);
  double w1 = w2nAA + w2nAB + w2nAC + w2nAD + w2nBA + w2nBB + w2nBC + w2nBD;
  double w2 = w2nCA + w2nCB + w2nCC + w2nCD + w2nDA + w2nDB + w2nDC + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetEfficiency2TError2_Truth()
{
  double div = n2L + n2T;
  if(div==0) return 0;
  div *= div;
  double dn1 = n2L/div;
  double dn2 = -(n2T/div);
  double w1  = w2nAA + w2nAB + w2nBA + w2nBB + w2nCA + w2nCB + w2nDA + w2nDB;
  double w2  = w2nAC + w2nAD + w2nBC + w2nBD + w2nCC + w2nCD + w2nDC + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2;
}

double SB2x2DSolver::GetCorrelationIError2_Truth()
{
  double div = (nXIXI + nXIXN)*(nXIXI + nXNXI);
  if(div==0) return 0;
  double dn1 = (2*nXIXI*nXIXN*nXNXI - SQUARE(nXIXI)*nXNXN + nXIXN*nXNXI*(nXIXN + nXNXI + nXNXN))/(SQUARE(div));
  double dn2 = nXIXI/(div);
  double dn3 = -((nXIXI*(nXNXI + nXNXN))/(SQUARE(nXIXI + nXIXN)*(nXIXI + nXNXI)));
  double dn4 = -((nXIXI*(nXIXN + nXNXN))/((nXIXI + nXIXN)*SQUARE(nXIXI + nXNXI)));
  double w1  = w2nAA + w2nAC + w2nCA + w2nCC; // XIXI
  double w2  = w2nBB + w2nBD + w2nDB + w2nDD; // XNXN
  double w3  = w2nAB + w2nAD + w2nCB + w2nCD; // XIXN
  double w4  = w2nBA + w2nBC + w2nDA + w2nDC; // XNXI
  return dn1*dn1*w1+dn2*dn2*w2+dn3*dn3*w3+dn4*dn4*w4;
}

double SB2x2DSolver::GetCorrelation1Error2_Truth()
{
  double div = (n1NI + n1TI)*(n1TI + n1TN);
  if(div==0) return 0;
  double dn1 = (n1NI*n1TN*(n1NI + 2*n1TI + n1TN) + n1NN*(-SQUARE(n1TI) + n1NI*n1TN))/(SQUARE(div));
  double dn2 = -(((n1NI + n1NN)*n1TI)/((n1NI + n1TI)*SQUARE(n1TI + n1TN)));
  double dn3 = -((n1TI*(n1NN + n1TN))/(SQUARE(n1NI + n1TI)*(n1TI + n1TN)));
  double dn4 = n1TI/(div);
  double w1  = w2nAA + w2nAB + w2nAC + w2nAD;
  double w2  = w2nBA + w2nBB + w2nBC + w2nBD;
  double w3  = w2nCA + w2nCB + w2nCC + w2nCD;
  double w4  = w2nDA + w2nDB + w2nDC + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2+dn3*dn3*w3+dn4*dn4*w4;
}

double SB2x2DSolver::GetCorrelation2Error2_Truth()
{
  double div = (n2NI + n2TI)*(n2TI + n2TN);
  if(div==0) return 0;
  double dn1 = (n2NI*n2TN*(n2NI + 2*n2TI + n2TN) + n2NN*(-SQUARE(n2TI) + n2NI*n2TN))/(SQUARE(div));
  double dn2 = -(((n2NI + n2NN)*n2TI)/((n2NI + n2TI)*SQUARE(n2TI + n2TN)));
  double dn3 = -((n2TI*(n2NN + n2TN))/(SQUARE(n2NI + n2TI)*(n2TI + n2TN)));
  double dn4 = n2TI/(div);
  double w1  = w2nAA + w2nBA + w2nCA + w2nDA;
  double w2  = w2nAB + w2nBB + w2nCB + w2nDB;
  double w3  = w2nAC + w2nBC + w2nCC + w2nDC;
  double w4  = w2nAD + w2nBD + w2nCD + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2+dn3*dn3*w3+dn4*dn4*w4;
}

double SB2x2DSolver::GetCorrelationTError2_Truth()
{
  double div  = (nNXTX + nTXTX)*(nTXNX + nTXTX);
  if(div==0) return 0;
  double dn1 = (nNXTX*nTXNX*(nNXTX + nTXNX + 2*nTXTX) + nNXNX*(nNXTX*nTXNX - SQUARE(nTXTX)))/(SQUARE(div));
  double dn2 = -(((nNXNX + nNXTX)*nTXTX)/((nNXTX + nTXTX)*SQUARE(nTXNX + nTXTX)));
  double dn3 = -(((nNXNX + nTXNX)*nTXTX)/(SQUARE(nNXTX + nTXTX)*(nTXNX + nTXTX)));
  double dn4 = nTXTX/(div);
  double w1  = w2nAA + w2nAB + w2nBA + w2nBB;
  double w2  = w2nAC + w2nAD + w2nBC + w2nBD;
  double w3  = w2nCA + w2nCB + w2nDA + w2nDB;
  double w4  = w2nCC + w2nCD + w2nDC + w2nDD;
  return dn1*dn1*w1+dn2*dn2*w2+dn3*dn3*w3+dn4*dn4*w4;
}

}
