#include "HGamBackgroundStudies/HGam2x2DInputMaker.h"
#include "HGamBackgroundStudies/DiPhotonBkg2x2DSB.h"

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif

#ifdef __CINT__
#pragma link C++ class HGam2x2DInputMaker+;
#pragma link C++ class DiPhotonBkg2x2DSB+;
#endif
