#include <stdio.h>
#include <TMath.h>
#include "HGamBackgroundStudies/SB2x2DSolver.h"

#define SQUARE(x) ((x)*(x))
#define Power(x,y) pow(x,y)

namespace SB {

SB2x2DSolver::SB2x2DSolver() : TObject()
{
  Initialize();
}

SB2x2DSolver::~SB2x2DSolver()
{
}

void SB2x2DSolver::Initialize()
{
  mInput = SIMPLEST;

  p.SetDefaults();

  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      n[i][j]=0;
      w2n[i][j]=0;
    }
  }
}

void SB2x2DSolver::Fill(SB2x2D *t, int ibin1, int ibin2, int ibinIgnore)
{
  Initialize();
  if(t==0) return;
  if(ibin1<0 || ibin2<ibin1) return;

  mInput = t->GetInputMode();

  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      if(t->GetNHist(i,j)) {
	n[i][j] = 0;
	w2n[i][j] = 0;
	for(int ibin=ibin1;ibin<=ibin2;ibin++) {
	  if(ibin==ibinIgnore) continue;
	  double myn = t->GetNHist(i,j)->GetBinContent(ibin);
	  n[i][j] += myn;
	  w2n[i][j]+=t->GetNHist(i,j)->GetSumw2()->fArray[ibin];
	}
      } else{ 
	n[i][j]=t->GetN(i,j);
	w2n[i][j]=t->GetNError2(i,j);
      }
    }
  }

  SetValues(t);
}

void SB2x2DSolver::Fill(SB2x2D*t, int ibin, int ifixed)
{
  Initialize();
  if(t==0) return;

  mInput = t->GetInputMode();

  if(ibin<0) {
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	n[i][j]=t->GetN(i,j);
	w2n[i][j]=t->GetNError2(i,j);
      }
    }
  } else {
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	if(t->GetNHist(i,j)) {
	  n[i][j]=t->GetNHist(i,j)->GetBinContent(ibin);
	  w2n[i][j]=t->GetNHist(i,j)->GetSumw2()->fArray[ibin];
	} else{ 
	  n[i][j]=t->GetN(i,j);
	  w2n[i][j]=t->GetNError2(i,j);
	}
      }
    }
  }
  
  SetValues(t,ifixed);
}

void SB2x2DSolver::SetValues(SB2x2D*t, int ifixed)
{
  if(mInput==FIT || mInput==NOTHING) {
    p.SetValues(t->GetOutput());
    //std::cout<<" Zihang Fit mode, p.SetValues(t->GetOutput()); "<<std::endl;
    return;
  }

  p.SetValues(t->GetCurrent());
  //std::cout<<" Zihang not Fit mode, p.SetValues(t->GetCurrent()); "<<std::endl;
  if(mInput==ISOLATION) {
    p.SimplifyIsolationInputs(); // Simplify but XIJJ!=1
  } else {
    if(mInput==SIMPLEST) p.Simplest();
    else p.SimplifyEfficiencyInputs(); // No efficiency primes and only xIjj can be different from 1
  }
  
  if(mInput==ISOLATION) {
    p.SetValue(EP1T,e1T);
    p.SetValue(EP2T,e2T);
    p.SetValue(FP1T,f1T);
    p.SetValue(FP2T,f2T);
    if(ifixed!=E1T) p.SetValue(E1T,GetEfficiency1T_IsolationInput()); 
    if(ifixed!=E2T) p.SetValue(E2T,GetEfficiency2T_IsolationInput()); 
    if(ifixed!=F1T) p.SetValue(F1T,GetFakeRate1T_IsolationInput()); 
    if(ifixed!=F2T) p.SetValue(F2T,GetFakeRate2T_IsolationInput()); 
    p.SetError2(EP1T,0); p.SetError2(E1T,GetEfficiency1TError2_IsolationInput());
    p.SetError2(EP2T,0); p.SetError2(E2T,GetEfficiency2TError2_IsolationInput());
    p.SetError2(FP1T,0); p.SetError2(F1T,GetFakeRate1TError2_IsolationInput());
    p.SetError2(FP2T,0); p.SetError2(F2T,GetFakeRate2TError2_IsolationInput());
  } else if(mInput==SIMPLIFIED) { //Simplified method with possibly data-driven fake rates primes and xIjj
    p.SetValue(FP1T,f1T);
    p.SetValue(FP2T,f2T);
    if(ifixed!=F1I)  p.SetValue(F1I,GetFakeRate1I_Simplified());
    if(ifixed!=F2I)  p.SetValue(F2I,GetFakeRate2I_Simplified());
    if(ifixed!=F1T)  p.SetValue(F1T,GetFakeRate1T_Simplified()); 
    if(ifixed!=F2T)  p.SetValue(F2T,GetFakeRate2T_Simplified()); 
    p.SetError2(F1I,GetFakeRate1IError2_Simplified());
    p.SetError2(F2I,GetFakeRate2IError2_Simplified());
    p.SetError2(F1T,GetFakeRate1TError2_Simplified()); p.SetError2(FP1T,0); 
    p.SetError2(F2T,GetFakeRate2TError2_Simplified()); p.SetError2(FP2T,0); 
  } else { // SIMPLEST
    p.SetValue(FP1I,f1I);
    p.SetValue(FP2I,f2I);
    p.SetValue(FP1T,f1T);
    p.SetValue(FP2T,f2T);
    if(ifixed!=F1I) p.SetValue(F1I,GetFakeRate1I_Simplest()); 
    if(ifixed!=F2I) p.SetValue(F2I,GetFakeRate2I_Simplest()); 
    if(ifixed!=F1T)  p.SetValue(F1T,GetFakeRate1T_Simplified());
    if(ifixed!=F2T)  p.SetValue(F2T,GetFakeRate2T_Simplified()); 
    p.SetError2(F1I,GetFakeRate1IError2_Simplest()); p.SetError2(FP1I,0); 
    p.SetError2(F2I,GetFakeRate2IError2_Simplest()); p.SetError2(FP2I,0); 
    p.SetError2(F1T,GetFakeRate1TError2_Simplified()); p.SetError2(FP1T,0); 
    p.SetError2(F2T,GetFakeRate2TError2_Simplified()); p.SetError2(FP2T,0); 
  }
  
  p.SetValue(WGGTITI,GetWggTITI());
  p.SetValue(WGJTITI,GetWgjTITI());
  p.SetValue(WJGTITI,GetWjgTITI());
  p.SetValue(WJJTITI,GetWjjTITI());
}

#define SB SB2x2DSolver

const FMETHOD SB2x2DSolver::vtable[NPARAMS] = {
  &SB::GetWgg, &SB::GetWgj, &SB::GetWjg, &SB::GetWjj, &SB::GetWgjjg, &SB::GetPurity, &SB::GetAlpha,
  &SB::GetWggII, &SB::GetWgjII, &SB::GetWjgII, &SB::GetWjjII, &SB::GetWgjjgII, &SB::GetPurityII, &SB::GetAlphaII,
  //&SB::GetWggTITI, &SB::GetWgjTITI, &SB::GetWjgTITI, &SB::GetWjjTITI,
  0, 0, 0, 0, 
  &SB::GetWgjjgTITI, 0, 0, &SB::GetPurityTITI, &SB::GetAlphaTITI,
  0, 0, 0, 0,
  0, 0, 0, 0, 
  0, 0, 0, 0,
  0, 0, 0, 0, 
  0, 0, 0, 0,
  0, 0, 0, 0, 
  0, 0, 0, 0
};

const FMETHOD SB2x2DSolver::etable[NPARAMS] = {
  &SB::GetWggError2, &SB::GetWgjError2, &SB::GetWjgError2, &SB::GetWjjError2, &SB::GetWgjjgError2, &SB::GetPurityError2, &SB::GetAlphaError2,
  &SB::GetWggIIError2, &SB::GetWgjIIError2, &SB::GetWjgIIError2, &SB::GetWjjIIError2, &SB::GetWgjjgIIError2, &SB::GetPurityIIError2, &SB::GetAlphaIIError2,
  &SB::GetWggTITIError2, &SB::GetWgjTITIError2, &SB::GetWjgTITIError2, &SB::GetWjjTITIError2, &SB::GetWgjjgTITIError2, 0, 0, &SB::GetPurityTITIError2, &SB::GetAlphaTITIError2,
  0, 0, 0, 0,
  0, 0, 0, 0, 
  0, 0, 0, 0,
  0, 0, 0, 0, 
  0, 0, 0, 0,
  0, 0, 0, 0, 
  0, 0, 0, 0
};

const FMETHOD SB2x2DSolver::vtruthtable[NPARAMS] = {
  0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0,0,0,
  &SB::GetEfficiency1IXI_Truth, &SB::GetEfficiency2IXI_Truth, &SB::GetEfficiency1TXT_Truth, &SB::GetEfficiency2TXT_Truth,
  &SB::GetEfficiency1I_Truth, &SB::GetEfficiency2I_Truth, &SB::GetEfficiency1T_Truth, &SB::GetEfficiency2T_Truth,
  0, 0, 0, 0,
  0, 0, 0, 0,
  &SB::GetCorrelation1_Truth, &SB::GetCorrelation2_Truth, 0, 0,
  &SB::GetCorrelationI_Truth, 0, 0, 0, 
  &SB::GetCorrelationT_Truth, 0, 0, 0
};

const FMETHOD SB2x2DSolver::etruthtable[NPARAMS] = {
  0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0,0,0,
  &SB::GetEfficiency1IXIError2_Truth, &SB::GetEfficiency2IXIError2_Truth, &SB::GetEfficiency1TXTError2_Truth, &SB::GetEfficiency2TXTError2_Truth,
  &SB::GetEfficiency1IError2_Truth, &SB::GetEfficiency2IError2_Truth, &SB::GetEfficiency1TError2_Truth, &SB::GetEfficiency2TError2_Truth,
  0, 0, 0, 0,

  0, 0, 0, 0,
  &SB::GetCorrelation1Error2_Truth, &SB::GetCorrelation2Error2_Truth, 0, 0,
  &SB::GetCorrelationIError2_Truth, 0, 0, 0, 
  &SB::GetCorrelationTError2_Truth, 0, 0, 0
};

double SB2x2DSolver::GetValue(int i)
{
  FMETHOD f = vtable[i];
  if(f) return (this->*f)();
  return p.GetValue(i);  // Return input value
}

double SB2x2DSolver::GetError2(int i)
{
  FMETHOD f = etable[i];
  if(f) return (this->*f)();
  return p.GetError2(i);
}

double SB2x2DSolver::GetTruthValue(int i)
{
  FMETHOD f = vtruthtable[i];
  if(f) return (this->*f)();
  if(i<X1G) return 0; 
  return 1;
}

double SB2x2DSolver::GetTruthError2(int i)
{
  FMETHOD f = etruthtable[i];
  if(f) return (this->*f)();
  return 0;
}

double SB2x2DSolver::GetNTotal()
{
  double ntot = 0;
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      ntot += n[i][j];
    }
  }
  return ntot;
}

double SB2x2DSolver::GetNTotalError2()
{
  double var = 0;
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      var += w2n[i][j];
    }
  }
  return var;
}

double SB2x2DSolver::GetNTotalChi2()
{
  double v = GetNTotalError2();
  if(v==0) return 0;
  double p = GetNTotalPrediction();
  if(p<0) return -p*1e30;
  double n = GetNTotal();
  double dchi2 = n-p;
  dchi2 *= dchi2;
  dchi2 /= v;
  return dchi2;
}

Double_t SB2x2DSolver::GetNPrediction(int i, int j)
{
  if(i==0) {
    if(j==0) return GetNAAPrediction();
    if(j==1) return GetNABPrediction();
    if(j==2) return GetNACPrediction();
    if(j==3) return GetNADPrediction();
  }
  if(i==1) {
    if(j==0) return GetNBAPrediction();
    if(j==1) return GetNBBPrediction();
    if(j==2) return GetNBCPrediction();
    if(j==3) return GetNBDPrediction();
  }
  if(i==2) {
    if(j==0) return GetNCAPrediction();
    if(j==1) return GetNCBPrediction();
    if(j==2) return GetNCCPrediction();
    if(j==3) return GetNCDPrediction();
  }
  if(i==3) {
    if(j==0) return GetNDAPrediction();
    if(j==1) return GetNDBPrediction();
    if(j==2) return GetNDCPrediction();
    if(j==3) return GetNDDPrediction();
  }
  return GetNTotalPrediction();
}

double SB2x2DSolver::GetNChi2(int i, int j)
{
  double v = w2n[i][j];
  if(v==0) return 0;
  double p = GetNPrediction(i,j);
  if(p<0) return -p*1e30;
  double N = n[i][j];
  double dchi2 = N-p;
  dchi2 *= dchi2;
  dchi2 /= v;
  return dchi2;
}

double SB2x2DSolver::GetChi2()
{ 
  double chi2 = 0; //GetNTotalChi2();

  for(int i=0;i<NBOXES;i++)
    for(int j=0;j<NBOXES;j++)
      chi2 += GetNChi2(i,j);

  //std::cout << p[WGGTITI] << " " << GetNTotalChi2() << std::endl; 
  return chi2;
}


}
