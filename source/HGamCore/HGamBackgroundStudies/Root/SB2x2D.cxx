#include <stdio.h>
#include <iostream>
#include <iomanip> 
#include "TTimeStamp.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "HGamBackgroundStudies/SB2x2D.h"

#define SQUARE(x) ((x)*(x))

namespace SB {
 
SB2x2D::SB2x2D(TString name, TString title, std::vector<int*>& flags, Int_t nbinsx, double* xbins) :  TNamed(name,title)
{
  Initialize();
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      TString tmp_name;
      tmp_name.Append("N");
      tmp_name.Append(SB::BoxName[i]);
      tmp_name.Append(SB::BoxName[j]);
      TH1D *h = new TH1D(MakeName(name,tmp_name),MakeTitle(title,tmp_name),nbinsx,xbins);
      h->Sumw2();
      hN[i][j] = h;
    }
  }

  vFlags = flags;
  if(flags.size()<NbPointers) vFlags.resize(NbPointers,0);
}

SB2x2D::~SB2x2D()
{
  //for(int i=0;i<NBOXES;i++) {
  //  for(int j=0;j<NBOXES;j++) {
  //    if(hN[i][j]) delete hN[i][j];
  //  }
  //}
}
 
void SB2x2D::SetInputMode(Int_t mode) 
{
  mInput = mode; 
  if(mInput==SB2x2DSolver::ISOLATION)       mNFree =  8; // 17 parameters - 9 constraints
  else if(mInput==SB2x2DSolver::SIMPLEST)   mNFree =  8; // 12 parameters - 4 constraints
  else if(mInput==SB2x2DSolver::SIMPLIFIED) mNFree = 13;
}

void SB2x2D::Initialize()
{
  mDoGathering = 0;
  isyst = -1;
  SetInputMode(SB2x2DSolver::SIMPLEST);

  pin.SetDefaults();
  pcur = pin;
  pout.SetDefaults();

  for(int i=0;i<NPARAMS;i++) hInput[i] = 0;

  NTotPred = 0;
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      NPred[i][j] = 0;
    }
  }

  vFlags.clear();
}

TString SB2x2D::MakeName(TString gname, TString hname)
{
  TString name;
  name.Form("h%s_%s",hname.Data(),gname.Data());
  return name;
}

TString SB2x2D::MakeTitle(TString gtitle, TString htitle)
{
  TString title;
  title.Form("%s for %s",htitle.Data(),gtitle.Data());
  return title;
}

double SB2x2D::GetEntries() const
{
  if(hN[0][0]==0) return 0;
  double n = 0;
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      n+=GetHistContent(hN[i][j]);
    }
  }
  return n;
}

void SB2x2D::Print(Option_t* option) const
{
  printf( "%s.Print Name  = %s, Title = %s\n",IsA()->GetName(),fName.Data(),fTitle.Data());

  if(mInput==SB2x2DSolver::ISOLATION)       std::cout << "MODE = ISOLATION" << std::endl;
  else if(mInput==SB2x2DSolver::SIMPLEST)   std::cout << "MODE = SIMPLEST" << std::endl;
  else if(mInput==SB2x2DSolver::SIMPLIFIED) std::cout << "MODE = SIMPLIFIED" << std::endl;
  else if(mInput==SB2x2DSolver::FIT)        std::cout << "MODE = FIT" << std::endl;
  else                                             std::cout << "MODE = NOTHING" << std::endl;
  TString opt = option;
  opt.ToLower();

  if(opt.Contains("short")) {
    for(int ip=0;ip<E1I;ip++) {
      printf( "%-14.14s = %g +/- %g\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip));
    }
    return;
  }

  bool print_pred = false;
  if(opt.Contains("pred") || opt.Contains("all")) print_pred = true;

  if(opt.Contains("math")) {
    // Some output for easy cut and paste in Mathematica
    // i==j
    std::cout << "{ ";
    for(int i=0;i<NBOXES;i++) {
      if(i>0) std::cout << ", ";
      std::cout << "n" << SB::BoxName[i] << SB::BoxName[i];
    }
    std::cout << " } := { ";
    for(int i=0;i<NBOXES;i++) {
      if(i>0) std::cout << ", ";
      std::cout << GetN(i,i);
    }
    std::cout << " }" << std::endl;

    // i<j
    std::cout << "{ ";
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	if(i==j) continue;
	if(i>j) continue;
	if(i>0 || j>1) std::cout << ", ";
	std::cout << "n" << SB::BoxName[i] << SB::BoxName[j];
      }
    }
    std::cout << " } := { ";
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	if(i==j) continue;
	if(i>j) continue;
	if(i>0 || j>1) std::cout << ", ";
	std::cout << GetN(i,j);
      }
    }
    std::cout << " }" << std::endl;

    // i>j
    std::cout << "{ ";
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	if(i==j) continue;
	if(i<j) continue;
	if(j>0 || i>1) std::cout << ", ";
	std::cout << "n" << SB::BoxName[i] << SB::BoxName[j];
      }
    }
    std::cout << " } := { ";
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	if(i==j) continue;
	if(i<j) continue;
	if(j>0 || i>1) std::cout << ", ";
	std::cout << GetN(i,j);
      }
    }
    std::cout << " }" << std::endl;
    std::cout << "{ ";
    for(int i=0;i<NPARAMS;i++) {
      if(i>0) std::cout << ", ";
      std::cout << ParamName[i];
    }
    std::cout << " } := { ";
    for(int i=0;i<NPARAMS;i++) {
      if(i>0) std::cout << ", ";
      std::cout << pout.GetValue(i);
    }
    std::cout << " } " << std::endl;
  }

  if(opt.Contains("pred") || opt.Contains("all") || opt.Contains("raw")) {
    if(print_pred) printf( "NTOT           = %g +/- %g (Prediction: %g dchi2=%g)\n",GetNTot(),TMath::Sqrt(GetNTotError2()),GetNTotPrediction(),GetNTotChi2());
    else           printf( "NTOT           = %g +/- %g\n",GetNTot(),TMath::Sqrt(GetNTotError2()));
    for(int i=0;i<NBOXES;i++) {
      for(int j=0;j<NBOXES;j++) {
	if(print_pred) printf( "N%s%s            = %g +/- %g (Prediction: %g dchi2=%g)\n",SB::BoxName[i],SB::BoxName[j],GetN(i,j),TMath::Sqrt(GetNError2(i,j)),GetNPrediction(i,j),GetNChi2(i,j));
	else           printf( "N%s%s            = %g +/- %g\n",SB::BoxName[i],SB::BoxName[j],GetN(i,j),TMath::Sqrt(GetNError2(i,j)));
      }
    }
    if(print_pred) {
      PrintExtraConstraints();
      double chi2 = GetChi2();
      int nextra = vConstraints.size();
      std::cout << "CHI2           = "  << chi2 << " / (16+1+" << nextra << "-" << mNFree << "=" << 16+nextra-mNFree << ") = " << chi2/(16.0+nextra-mNFree) << std::endl;
    }
    printf( "----------------\n");
  }

  if(opt.Contains("results") || opt.Contains("all")) {
    printf( "// Extracted Yields and Paramaters:\n");
    if(opt.Contains("ll") || opt.Contains("all")) {
      for(int ip=0;ip<=ALPHA;ip++) {
	printf( "%-14.14s = %g +/- %g\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip));
      }
    }
    if(opt.Contains("ii") || opt.Contains("all")) {
      for(int ip=WGGII;ip<=ALPHAII;ip++) {
	printf( "%-14.14s = %g +/- %g\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip));
      }
    }
    if(opt.Contains("titi") || opt.Contains("all")) {
      
      for(int ip=WGGTITI;ip<=ALPHATITI;ip++) {
	if(pout.HasAsymmetricErrors(ip)) printf( "%-14.14s = %g +/- %g (+%g %g)\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip),pout.GetErrorHigh(ip),pout.GetErrorLow(ip));
      else printf( "%-14.14s = %g +/- %g\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip));
      }
    }
    for(int ip=E1I;ip<NPARAMS;ip++) {
      if(pout.HasAsymmetricErrors(ip)) printf( "%-14.14s = %g +/- %g (+%g %g)\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip),pout.GetErrorHigh(ip),pout.GetErrorLow(ip));
      else if(pout.GetError2(ip)>0) printf( "%-14.14s = %g +/- %g\n",ParamName[ip],pout.GetValue(ip),pout.GetError(ip));
    }
  }
}

double SB2x2D::GetNTot() const
{
  double ntot = 0;
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      ntot += GetN(i,j);
    }
  }
  return ntot;
}

double SB2x2D::GetNTotError2() const
{
  double var = 0;
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      var += GetNError2(i,j);
    }
  }
  return var;
}

double SB2x2D::GetPChi2(int i) const
{
  int idx = vConstraints.at(i);
  double n = pout.GetValue(idx);
  double p = pcur.GetValue(idx);
  double v = pcur.GetError2(idx);

  double dchi2 = n-p;
  dchi2 *= dchi2;
  dchi2 /= v;
  return dchi2;
}

double SB2x2D::GetNTotChi2() const
{
  double n = GetNTot();
  double p = GetNTotPrediction();
  double v = GetNTotError2();
  double dchi2 = n-p;
  dchi2 *= dchi2;
  dchi2 /= v;
  return dchi2;
}

double SB2x2D::GetNChi2(int i, int j) const
{
  double n = GetN(i,j);
  double p = GetNPrediction(i,j);
  double v = GetNError2(i,j);
  double dchi2 = n-p;
  dchi2 *= dchi2;
  dchi2 /= v;
  return dchi2;
}

double SB2x2D::GetExtraChi2() const
{
  double chi2 = 0;
  int nc = vConstraints.size();
  for(int i=0;i<nc;i++) {
    chi2 += GetPChi2(i);
  }
  return chi2;
}

double SB2x2D::GetChi2() const
{
  double chi2 = 0; //GetNTotChi2();
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      chi2 += GetNChi2(i,j);
    }
  }
  return chi2+GetExtraChi2();
}

void SB2x2D::Fill(double x, double weight)
{
  for(int i=0;i<NbPointers;i++) {
    //std::cout << "Flag " << i << " = " << vFlags.at(i) << std::endl; 
    if(vFlags.at(i)==0) {
      std::cout << IsA()->GetName() << " " << fName << ": you forget to set at least one pointer to quality values..." << std::endl;
      exit(1);
    }
  }
  if(hN[0][0]==0) {
    std::cout << IsA()->GetName() << " " << fName << ": your object has no associated histograms..." << std::endl;
    exit(1);
  }
  
  int i=-1;
  int j=-1;
  
  int* pQ1P1 = vFlags.at(Quality1Signal1);
  int* pQ2P1 = vFlags.at(Quality2Signal1);
  int* pQ1P2 = vFlags.at(Quality1Signal2);
  int* pQ2P2 = vFlags.at(Quality2Signal2);

  if(*pQ1P1) {
    if(*pQ2P1) i=0; // A
    else       i=1; // B
  } else {
    if(*pQ2P1) i=2; // C
    else       i=3; // D
  }
  if(*pQ1P2) {
    if(*pQ2P2) j=0; // A
    else       j=1; // B
  } else {
    if(*pQ2P2) j=2; // C
    else       j=3; // D
  }
  //std::cout << "Filling histogram " << i << "  " << j << std::endl;
  hN[i][j]->Fill(x,weight);
}

// Static methods

double SB2x2D::GetHistContent(TH1D *h)
{
  if(h==0) return 0;
  Int_t n = h->GetNbinsX();
  double igl=0;
  for(int i=0;i<=n+1;i++) {
    igl += h->GetBinContent(i);
  }
  return igl;
}

double SB2x2D::GetHistError2(TH1D *h)
{
  if(h==0) return 0;
  Int_t n = h->GetNbinsX();
  TArrayD *sumw2 = h->GetSumw2();
  double err2=0;
  for(int i=0;i<=n+1;i++) {
    err2 += sumw2->fArray[i];
  }
  return err2;
}

void SB2x2D::SetExtraTITI()
{
    double gjjg   = pout.GetValue(WGJTITI)+pout.GetValue(WJGTITI);
    double w2gjjg = pout.GetError2(WGJTITI)+pout.GetError2(WJGTITI)
      +pout.GetError(WGJTITI)*pout.GetError(WJGTITI);
    double pur1   = pout.GetValue(WGGTITI);
    double pur2   = gjjg+pout.GetValue(WJJTITI);
    double div    = pur1+pur2;
    double purity = 0;
    double w2pur  = 0;
    if(div!=0) {
      // pur = n1/(n1+n2) and n1, n2 are anti correlated (the sum is constant)
      // w2pur = dn1^2*D[pur,n1]^2+dn2^2x*D[pur,n2]^2-2*dn1*dn2*D[pur,n1]*D[pur,n2]
      // w2pur = (dn2*n1+dn1*n2)^2/(n1+n2)^4
      double dpur1 = TMath::Sqrt(pout.GetError2(WGGTITI));
      double dpur2 = TMath::Sqrt(w2gjjg+pout.GetError2(WJJTITI));
      purity = pur1/div;
      double wpur  = (dpur2*pur1+dpur1*pur2)/div/div;
      w2pur  = wpur*wpur;
    }

    
   
    double alpha  = 0;
    double w2alp  = 0;
    if(gjjg!=0) {
      double a1 = pout.GetValue(WJGTITI);
      double a2 = pout.GetValue(WGJTITI);
      double w2a1 = pout.GetError2(WJGTITI);
      double w2a2 = pout.GetError2(WGJTITI);
      alpha = a1/gjjg;
      double da1 = a2/gjjg/gjjg;
      double da2 = -a1/gjjg/gjjg;
      w2alp = w2a1*da1*da1+w2a2*da2*da2;
    }
    pout.SetValue(WGJJGTITI,gjjg);
    pout.SetError2(WGJJGTITI,w2gjjg);
    pout.SetValue(PURITYTITI,purity);
    pout.SetError2(PURITYTITI,w2pur);
    pout.SetValue(ALPHATITI,alpha);
    pout.SetError2(ALPHATITI,w2alp);



    double pur1_gj   = pout.GetValue(WGJJGTITI);
    double pur2_gj   = pout.GetValue(WGGTITI)+pout.GetValue(WJJTITI);
    double div_gj    = pur1_gj+pur2_gj;
    double purity_gj = 0;
    double w2pur_gj  = 0;
    if(div_gj!=0) {
      // pur = n1/(n1+n2) and n1, n2 are anti correlated (the sum is constant)
      // w2pur = dn1^2*D[pur,n1]^2+dn2^2x*D[pur,n2]^2-2*dn1*dn2*D[pur,n1]*D[pur,n2]
      // w2pur = (dn2*n1+dn1*n2)^2/(n1+n2)^4
      double dpur1_gj = TMath::Sqrt(pout.GetError2(WGJJGTITI));
      double dpur2_gj = TMath::Sqrt(pout.GetError2(WGGTITI)+pout.GetError2(WJJTITI));
      purity_gj = pur1_gj/div_gj;
      double wpur_gj  = (dpur2_gj*pur1_gj+dpur1_gj*pur2_gj)/div_gj/div_gj;
      w2pur_gj  = wpur_gj*wpur_gj;
    }


    pout.SetValue(PURITYGJTITI,purity_gj);
    pout.SetError2(PURITYGJTITI,w2pur_gj);



    double pur1_jj   = pout.GetValue(WJJTITI);
    double pur2_jj   = pout.GetValue(WGGTITI)+pout.GetValue(WGJJGTITI);
    double div_jj    = pur1_jj+pur2_jj;
    double purity_jj = 0;
    double w2pur_jj  = 0;
    if(div_jj!=0) {
      // pur = n1/(n1+n2) and n1, n2 are anti correlated (the sum is constant)
      // w2pur = dn1^2*D[pur,n1]^2+dn2^2x*D[pur,n2]^2-2*dn1*dn2*D[pur,n1]*D[pur,n2]
      // w2pur = (dn2*n1+dn1*n2)^2/(n1+n2)^4
      double dpur1_jj = TMath::Sqrt(pout.GetError2(WJJTITI));
      double dpur2_jj = TMath::Sqrt(pout.GetError2(WGGTITI)+pout.GetError2(WGJJGTITI));
      purity_jj = pur1_jj/div_jj;
      double wpur_jj  = (dpur2_jj*pur1_jj+dpur1_jj*pur2_jj)/div_jj/div_jj;
      w2pur_jj  = wpur_jj*wpur_jj;
    }

    pout.SetValue(PURITYJJTITI,purity_jj);
    pout.SetError2(PURITYJJTITI,w2pur_jj);
    

}

void SB2x2D::Solve(int ibin)
{
  if(hN[0][0]==0) return;

  SB2x2DSolver p;
  if(mInput==SB2x2DSolver::FIT) {
    Fit(ibin);
    p.Fill(this,ibin,isyst);

  } else if(mInput==SB2x2DSolver::NOTHING) {
    p.Fill(this,ibin,isyst);
    // Fill in WTITI normalized to NAA
    for(int i=0;i<E1I;i++) {
      pout.SetValue(i,p.GetValue(i));
      pout.SetError2(i,p.GetError2(i));
    }
  } else {
    pcur = pin;
    if(ibin>=0) {
      for(int ip=0;ip<NPARAMS;ip++) {
	if(hInput[ip]) {
	  TH1D *h = hInput[ip];
	  pcur.SetValue(ip,h->GetBinContent(ibin));
	  pcur.SetError(ip,h->GetBinError(ibin));
	  //std::cout << ParamName[ip] << " bin = " << ibin << " y = " << h->GetBinContent(ibin) << " +/- " << h->GetBinError(ibin) << std::endl;
	}
      }
    }
    p.Fill(this,ibin,isyst);
    for(int i=0;i<NPARAMS;i++) {
      pout.SetValue(i,p.GetValue(i));
      pout.SetError2(i,p.GetError2(i));
    }
  }
  NTotPred   = p.GetNTotalPrediction();
  for(int i=0;i<NBOXES;i++) {
    for(int j=0;j<NBOXES;j++) {
      NPred[i][j] = p.GetNPrediction(i,j);
    }
  }

  return;
}

static bool do_me = false;

double SB2x2D::GetComputedValue(int ibin, int i)
{
  //SB2x2DSolver p;
  Fit(ibin);

  do_me = true;
  Fit(ibin);
  //p.Fill(this,ibin,isyst);
  do_me = false;

  //if(i==WGGTITI) return p.GetWggTITI();
  if(i==WGGTITI) return GetOutputValue(i);
  if(i==ALPHATITI) return GetOutputValue(i);
  return 0;
}

TH1D* SB2x2D::GetHist(int i)
{
  if(hN[0][0]==0) {
    std::cout << IsA()->GetName() << " " << fName << ": your object has no associated histograms..." << std::endl;
    exit(1);
  }

  TH1D *fHisto = (TH1D*) (hN[0][0]->Clone());
  TString hname,htitle;
  hname.Form("h%s_%s",ParamName[i],fName.Data());
  htitle.Form("%s extracted %s",ParamName[i],fTitle.Data());
  fHisto->SetName(hname);
  fHisto->SetTitle(htitle);
  //std::cout << "GetHist for " << ParamName[i] << std::endl;

  Int_t    n = fHisto->GetNbinsX();
  SB2x2DSolver p;
  for(Int_t ibin=0;ibin<=n+1;ibin++) {
    if(mInput==SB2x2DSolver::FIT) {
      //if(ibin>=0 && ibin <2) std::cout << "Fit ibin = " << ibin << std::endl;
      //std::cout << "Zihang : Fit ibin "<<ibin<<std::endl;
      Fit(ibin);
      //p.Fill(this,ibin,isyst);
      double w    = pout.GetValue(i);
      double werr = pout.GetError(i);
      fHisto->SetBinContent(ibin,w);
      fHisto->SetBinError(ibin,werr);      
      //  std::cout << "Hist fit error per bin " << ibin << " " << w << " +/- " <<  werr << std::endl;
    } else {
      p.Fill(this,ibin,isyst);
      //std::cout << "Zihang : simply Fill ibin "<<ibin<<std::endl;
      double w    = p.GetValue(i);
      //      Double werr = TMath::Sqrt(p.GetError2(i));
      //std::cout << "Hist std error per bin " << ibin << " " << w << " +/- " << werr << std::endl;
      fHisto->SetBinContent(ibin,w);
      //fHisto->SetBinError(ibin,werr);
    }
  }

  //printf( "%s.GetHistoHist Name  = %s: %s, %s\n",IsA()->GetName(),fName.Data(),hname.Data(),htitle.Data());
  return fHisto;
}

TGraphAsymmErrors *SB2x2D::GetError(int i)
{
  if(hN[0][0]==0) {
    std::cout << IsA()->GetName() << " " << fName << ": your object has no associated histograms..." << std::endl;
    exit(1);
  }

  int nbin = GetNbinsX();
  const TArrayD* xbins = GetXbins(); // Arrays size should be nbin+1
  std::vector<double> vX;
  std::vector<double> vXE;
  std::vector<double> vY;
  std::vector<double> vYup;
  std::vector<double> vYlo;

  TGraphAsymmErrors *g = new TGraphAsymmErrors();
  // Add stat error to graph
  for(int ibin=0;ibin<nbin; ibin++) {
    
    SB2x2DSolver p;
    if(mInput==SB2x2DSolver::FIT) {
      Fit(ibin+1);
      g->SetPoint(ibin, (xbins->fArray[ibin]+xbins->fArray[ibin+1])/2.0, pout.GetValue(i));
      g->SetPointEXhigh(ibin,(xbins->fArray[ibin+1]-xbins->fArray[ibin])/2.0);
      g->SetPointEXlow(ibin,(xbins->fArray[ibin+1]-xbins->fArray[ibin])/2.0); 
   } else {
      p.Fill(this,ibin,isyst);
      double w    = p.GetValue(i);
      g->SetPoint(ibin,(xbins->fArray[ibin]+xbins->fArray[ibin+1])/2.0,w);
      g->SetPointEXhigh(ibin,(xbins->fArray[ibin+1]-xbins->fArray[ibin])/2.0);
      g->SetPointEXlow(ibin,(xbins->fArray[ibin+1]-xbins->fArray[ibin])/2.0);
    }

    double Ys = vStatErrByBin[i][ibin+1];
    g->SetPointEYhigh(ibin,Ys);
    g->SetPointEYlow(ibin,Ys);
  }
  
  
  TString gname, hname;
  gname.Append("g");
  gname.Append(ParamName[i]);
  hname.Append(ParamName[i]);
  gname.Append("Stat_");
  gname.Append(GetName());
  g->SetName(gname);

  return g;
}


TH1D *SB2x2D::GetHistogram(TString hname)
{
  //std::cout<<"Getting histo : "<<hname<<std::endl;

  // Parameters and yields
  for(int ip=0;ip<NPARAMS;ip++) {
    if(hname.EqualTo(ParamName[ip])) return GetHist(ip);
  }

  //std::cout<<"getting h "<<std::endl;

  // NAA, AB, BA, AC, CA, AD, DA, ... and signal extractions in NAA, NAB, NBA 
  if(hname.Contains("NAA")) return hN[0][0]; 
  if(hname.Contains("NAB")) return hN[0][1];
  if(hname.Contains("NBA")) return hN[1][0];
  if(hname.Contains("NAC")) return hN[0][2];
  if(hname.Contains("NAD")) return hN[0][3];
  if(hname.Contains("NCA")) return hN[2][0];
  if(hname.Contains("NDA")) return hN[3][0];
  if(hname.Contains("NBB")) return hN[1][1];
  if(hname.Contains("NBC")) return hN[1][2];
  if(hname.Contains("NBD")) return hN[1][3];
  if(hname.Contains("NCB")) return hN[2][1];
  if(hname.Contains("NDB")) return hN[3][1];
  if(hname.Contains("NCC")) return hN[2][2];
  if(hname.Contains("NCD")) return hN[2][3];
  if(hname.Contains("NCD")) return hN[3][2];
  if(hname.Contains("NDD")) return hN[3][3];

  // Default
  std::cout << IsA()->GetName() << " " << fName << ": could not find histogram " << hname << std::endl;
  return 0;
}

void SB2x2D::FillRange(int ifirst, int ibin)
{
  std::vector<double> yields;
  yields.resize(E1I);

  Solve(ibin); 
  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    yields[i] = pout.GetValue(i);
  }

  if(ifirst==1) {
    vRangeMax.clear();
    vRangeMin.clear();
    vRangeMax.resize(E1I);
    vRangeMin.resize(E1I);
    for(int i=WGGTITI;i<=PURITYTITI;i++) vRangeMax[i]   = vRangeMin[i]   = yields[i];
    return;
  }

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    if(yields[i]>vRangeMax[i]) vRangeMax[i] = yields[i];
    if(yields[i]<vRangeMin[i]) vRangeMin[i] = yields[i];
  }
}

void SB2x2D::GetSystRange(TString name, double xlow, double xup, double step)
{
  double *x=0;

  //std::cout<<" ---------------------- GET SYST RANGE"<<std::endl;

  name.ToLower();
  bool found = false;
  for(int i=0;i<NPARAMS;i++) {
    TString pname = ParamName[i];

    //std::cout<<" NAME : "<<name<<" PNAME : "<<pname<<std::endl; 

    pname.ToLower();
    if(name.Contains(pname))  {
      x = pin.GetAddress(i);
      isyst = i;
      found = true;
    }
  }
  if(!found) {
    std::cout << "AddSystematics: Unknown variable with name " << name << std::endl;
    exit(1);
  }

  double save;
  
  if(x) save = *x; else save = 0;
  int tmp = isyst;
  isyst = -1;  
  FillRange(1);
  if(x==0) return;
  isyst = tmp;

  *x=xlow;
  FillRange(0);

  for(double xcur=xlow+step;xcur<xup;xcur+=step) {
    *x = xcur;
    FillRange(0);
  }

  *x=xup;
  FillRange(0);

  // back to default state
  *x = save;
  
  return;
}

void SB2x2D::GetSystRangeByBin(int ibin, TString name, double xlow, double xup, double step)
{
  double *x=0;
  double *xbin=0;

  Int_t    n = GetNHist(0,0)->GetNbinsX();
  if(ibin<0 || ibin>n+2) return;

  name.ToLower();
  bool found = false;
  for(int i=0;i<NPARAMS;i++) {
    TString pname = ParamName[i];
    pname.ToLower();
    if(name.Contains(pname))  {
      x = pin.GetAddress(i);
      isyst = i;
      found = true;
    }
  }
  if(!found) {
    std::cout << "AddSystematics: Unknown variable with name " << name << std::endl;
    exit(1);
  }

  double save;
  
  if(x) save = *x; else if(*xbin) save = *xbin; else save = 0;
  int tmp = isyst;
  isyst = -1;  
  FillRange(1,ibin);
  if(x==0 && xbin==0) return;
  isyst = tmp;

  if(x) *x=xlow; 
  if(xbin) *xbin=xlow;
  FillRange(0,ibin);

  for(double xcur=xlow+step;xcur<xup;xcur+=step) {
    if(x) *x=xcur; 
    if(xbin) *xbin=xcur;
    FillRange(0,ibin);
  }

  if(x) *x=xup; 
  if(xbin) *xbin=xup;
  FillRange(0,ibin);

  // back to default state
  if(x) *x=save; 
  if(xbin) *xbin=save;
  
  return;
}

void SB2x2D::FillRange(SB2x2D * t, int ifirst, int ibin)
{
  std::vector<double> yields;
  yields.resize(E1I);

  t->Solve(ibin); 
  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    yields[i] = t->GetOutput().GetValue(i);
  }

  if(ifirst==1) {
    vRangeMax.clear();
    vRangeMin.clear();
    vRangeMax.resize(E1I);
    vRangeMin.resize(E1I);
    for(int i=WGGTITI;i<=PURITYTITI;i++) vRangeMax[i]   = vRangeMin[i]   = yields[i];
    return;
  }

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    if(yields[i]>vRangeMax[i]) vRangeMax[i] = yields[i];
    if(yields[i]<vRangeMin[i]) vRangeMin[i] = yields[i];
  }
}

///void SB2x2D::FillRange(std::vector<double> yields, int ifirst, int ibin)
void SB2x2D::FillRange(std::vector<double> yields, int ifirst)
{
  yields.resize(E1I);

  if(ifirst==1) {
    vRangeMax.clear();
    vRangeMin.clear();
    vRangeMax.resize(E1I);
    vRangeMin.resize(E1I);
    for(int i=WGGTITI;i<=PURITYTITI;i++) vRangeMax[i]   = vRangeMin[i]   = yields[i];
    return;
  }

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    if(yields[i]>vRangeMax[i]) vRangeMax[i] = yields[i];
    if(yields[i]<vRangeMin[i]) vRangeMin[i] = yields[i];
  }
}

void SB2x2D::GetSystRange(std::vector<SB2x2D *> v)
{
  FillRange(this,1);
  int n = v.size();
  if(n<=0) return;

  FillRange(v[0],0);

  for(int i=1;i<n;i++) FillRange(v[i],0);

  return;
}

void SB2x2D::GetSystRangeByBin(int ibin, std::vector<SB2x2D *> v)
{
  Int_t nbin = GetNHist(0,0)->GetNbinsX();
  if(ibin<0 || ibin>nbin+2) return;

  FillRange(this,1,ibin);
  int n = v.size();
  if(n<=0) return;

  FillRange(v[0],0,ibin);

  for(int i=1;i<n;i++) FillRange(v[i],0,ibin);
  
  return;
}

void SB2x2D::GetSystRange(std::vector<double> v)
{
  FillRange(this,1);
  int n = v.size();
  if(n<=0) return;

  FillRange(v,0);

  return;
}

void SB2x2D::GetSystRangeByBin(int ibin, std::vector<double> v)
{
  Int_t nbin = GetNHist(0,0)->GetNbinsX();
  if(ibin<0 || ibin>nbin+2) return;

  FillRange(this,1,ibin);
  int n = v.size();
  if(n<=0) return;

  ///// deprecated
  ////FillRange(v,0,ibin);

  FillRange(v,0);
  
  return;
}

void SB2x2D::InitSystematics()
{
  vNominal.clear();
  vSystPos.clear();
  vSystNeg.clear();

  vNominal.resize(E1I,0);
  vSystPos.resize(E1I,0);
  vSystNeg.resize(E1I,0);

  int nbin = GetNbinsX();

  vNominalByBin.clear();
  vRangeMaxByBin.clear();
  vRangeMinByBin.clear();
  vStatErrByBin.clear();
  vSystPosByBin.clear();
  vSystNegByBin.clear();

  vNominalByBin.resize(E1I);
  vRangeMaxByBin.resize(E1I);
  vRangeMinByBin.resize(E1I);
  vStatErrByBin.resize(E1I);
  vSystPosByBin.resize(E1I);
  vSystNegByBin.resize(E1I);
  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    vNominalByBin[i].resize(nbin+2,0.0);
    vRangeMaxByBin[i].resize(nbin+2,0.0);
    vRangeMinByBin[i].resize(nbin+2,0.0);
    vStatErrByBin[i].resize(nbin+2,0.0);
    vSystPosByBin[i].resize(nbin+2,0.0);
    vSystNegByBin[i].resize(nbin+2,0.0);
  }

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    TH1D *h = GetHist(i);
    for(int ibin=0;ibin<=nbin+1;ibin++) {
      vNominalByBin[i][ibin] = h->GetBinContent(ibin);
      vStatErrByBin[i][ibin] = h->GetBinError(ibin);
    }
  }

  Solve(-1);
  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    vNominal[i]   = pout.GetValue(i);
  }
}

void SB2x2D::AddSystematics(TString name, double xlow, double xup, double step)
{
 
  isyst = -1;
  int nbin = GetNbinsX();

  for(int ibin=0;ibin<=nbin+1;ibin++) {
    GetSystRangeByBin(ibin,name,xlow,xup,step);

    for(int i=WGGTITI;i<=PURITYTITI;i++) {
      vSystPosByBin[i][ibin] += SQUARE(vRangeMax[i]-vNominalByBin[i][ibin]);
      vSystNegByBin[i][ibin] += SQUARE(vRangeMin[i]-vNominalByBin[i][ibin]);
      vRangeMaxByBin[i][ibin] = vRangeMax[i];
      vRangeMinByBin[i][ibin] = vRangeMin[i];
    }
  }

  GetSystRange(name,xlow,xup,step);

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    vSystPos[i] += SQUARE(vRangeMax[i]-vNominal[i]);
    vSystNeg[i] += SQUARE(vRangeMin[i]-vNominal[i]);
  }
  isyst = -1;
}

void SB2x2D::AddSystematics(std::vector<double> v, int symm)
{
  isyst = -1;
  int nbin = GetNbinsX();

  for(int ibin=0;ibin<=nbin+1;ibin++) {
    GetSystRangeByBin(ibin,v);

    if(symm) { // Symmetrize systematics to max of left or right
      for(int iy=WGGTITI;iy<=PURITYTITI;iy++) {
	double left  = vRangeMax[iy]-vNominalByBin[iy][ibin];
	double right = vNominalByBin[iy][ibin]-vRangeMin[iy];
	if(right>left) vRangeMax[iy] = vNominalByBin[iy][ibin] + right;
	if(left>right) vRangeMin[iy] = vNominalByBin[iy][ibin] - left;
      }
    }

    for(int i=WGGTITI;i<=PURITYTITI;i++) {
      vSystPosByBin[i][ibin] += SQUARE(vRangeMax[i]-vNominalByBin[i][ibin]);
      vSystNegByBin[i][ibin] += SQUARE(vRangeMin[i]-vNominalByBin[i][ibin]);
      vRangeMaxByBin[i][ibin] = vRangeMax[i];
      vRangeMinByBin[i][ibin] = vRangeMin[i];
    }
  }

  GetSystRange(v);

  if(symm) { // Symmetrize systematics to max of left or right
    for(int iy=WGGTITI;iy<=PURITYTITI;iy++) {
      double left  = vRangeMax[iy]-vNominal[iy];
      double right = vNominal[iy]-vRangeMin[iy];
      if(right>left) vRangeMax[iy] = vNominal[iy] + right;
      if(left>right) vRangeMin[iy] = vNominal[iy] - left;
    }
  }

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    vSystPos[i] += SQUARE(vRangeMax[i]-vNominal[i]);
    vSystNeg[i] += SQUARE(vRangeMin[i]-vNominal[i]);
  }
  isyst = -1;
}

void SB2x2D::AddSystematics(std::vector<void *> v, int symm)
{
  std::vector<SB2x2D *> vtmp;
  int n= v.size();
  for(int i=0;i<n;i++) {
    SB2x2D * t = (SB2x2D *) v[i];
    if(t) vtmp.push_back(t);
  }
  AddSystematics(vtmp,symm);
}

void SB2x2D::AddSystematics(std::vector<SB2x2D *> v, int symm, bool border)
{
  
  isyst = -1;
  int nbin = GetNbinsX();
  
  for(int ibin=0;ibin<=nbin+1;ibin++) {

    GetSystRangeByBin(ibin,v);

    if(symm || (border && (ibin<=1 || ibin>=nbin)) ) { // Symmetrize systematics to max of left or right
      for(int iy=WGGTITI;iy<=PURITYTITI;iy++) {
	double left  = vRangeMax[iy]-vNominalByBin[iy][ibin];
	double right = vNominalByBin[iy][ibin]-vRangeMin[iy];
	if(right>left) vRangeMax[iy] = vNominalByBin[iy][ibin] + right;
	if(left>right) vRangeMin[iy] = vNominalByBin[iy][ibin] - left;
      }
    }
    for(int i=WGGTITI;i<=PURITYTITI;i++) {
      vSystPosByBin[i][ibin] += SQUARE(vRangeMax[i]-vNominalByBin[i][ibin]);
      vSystNegByBin[i][ibin] += SQUARE(vRangeMin[i]-vNominalByBin[i][ibin]);
      vRangeMaxByBin[i][ibin] = vRangeMax[i];
      vRangeMinByBin[i][ibin] = vRangeMin[i];
    }
  }
  
  GetSystRange(v);

  if(symm) { // Symmetrize systematics to max of left or right
    for(int iy=WGGTITI;iy<=PURITYTITI;iy++) {
      double left  = vRangeMax[iy]-vNominal[iy];
      double right = vNominal[iy]-vRangeMin[iy];
      if(right>left) vRangeMax[iy] = vNominal[iy] + right;
      if(left>right) vRangeMin[iy] = vNominal[iy] - left;
    }
  }

  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    vSystPos[i] += SQUARE(vRangeMax[i]-vNominal[i]);
    vSystNeg[i] += SQUARE(vRangeMin[i]-vNominal[i]);
  }
  isyst = -1;
}

void SB2x2D::EndSystematics()
{
  Solve(-1);
  for(int i=WGGTITI;i<=PURITYTITI;i++) {
    vSystPos[i] = TMath::Sqrt(vSystPos[i]);
    vSystNeg[i] = TMath::Sqrt(vSystNeg[i]);
  }

  int nbin = GetNbinsX();
  for(int ibin=0;ibin<=nbin+1;ibin++) {
    for(int i=WGGTITI;i<=PURITYTITI;i++) {
      vSystPosByBin[i][ibin] = TMath::Sqrt(vSystPosByBin[i][ibin]);
      vSystNegByBin[i][ibin] = TMath::Sqrt(vSystNegByBin[i][ibin]);
      vRangeMaxByBin[i][ibin] = vNominalByBin[i][ibin]+vSystPosByBin[i][ibin];
      vRangeMinByBin[i][ibin] = vNominalByBin[i][ibin]-vSystNegByBin[i][ibin];
    }
  }
}

TGraphAsymmErrors* SB2x2D::GetLastSystematics(int iYield)
{
  int nbin = GetNbinsX();
  const TArrayD* xbins = GetXbins(); // Arrays size should be nbin+1
  std::vector<double> vX;
  std::vector<double> vXE;
  std::vector<double> vYEpos;
  std::vector<double> vYEneg;
  vX.push_back(xbins->fArray[0]-1); // underflows x = xmin-1
  vXE.push_back(0);
  vYEpos.push_back(0);
  vYEneg.push_back(0);
  for(int ibin=0;ibin<nbin;ibin++) {
    vX.push_back((xbins->fArray[ibin]+xbins->fArray[ibin+1])/2.0);
    vXE.push_back((xbins->fArray[ibin+1]-xbins->fArray[ibin])/2.0);
    vYEneg.push_back(  LastNegSystematics(iYield,ibin+1) );
    vYEpos.push_back(  LastPosSystematics(iYield,ibin+1) );
  }
  vX.push_back(xbins->fArray[nbin]+1); // overflows x = xmax+1
  vXE.push_back(0);
  vYEpos.push_back(0);
  vYEneg.push_back(0);
  TGraphAsymmErrors *g = new TGraphAsymmErrors(nbin,&(vX[1]),&(vNominalByBin[iYield][1]),&(vXE[1]),&(vXE[1]),&(vYEneg[1]),&(vYEpos[1]));
  TString gname, hname;
  gname.Append("g");
  gname.Append(ParamName[iYield]);
  hname.Append(ParamName[iYield]);
  gname.Append("Syst_");
  gname.Append(GetName());
  g->SetName(gname);
  return g;
}


TGraphAsymmErrors* SB2x2D::GetSystematics(int iYield, int nAverage, bool addStat)
{
  int nbin = GetNbinsX();
  const TArrayD* xbins = GetXbins(); // Arrays size should be nbin+1
  std::vector<double> vX;
  std::vector<double> vXE;

  vX.push_back(xbins->fArray[0]-1); // underflows x = xmin-1
  vXE.push_back(0);
  for(int ibin=0;ibin<nbin;ibin++) {
    vX.push_back((xbins->fArray[ibin]+xbins->fArray[ibin+1])/2.0);
    vXE.push_back((xbins->fArray[ibin+1]-xbins->fArray[ibin])/2.0);
  }
  vX.push_back(xbins->fArray[nbin]+1); // overflows x = xmax+1
  vXE.push_back(0);

  TGraphAsymmErrors *g = new TGraphAsymmErrors(nbin,&(vX[1]),&(vNominalByBin[iYield][1]),&(vXE[1]),&(vXE[1]),&(vSystNegByBin[iYield][1]),&(vSystPosByBin[iYield][1]));

  TString gname, hname;
  gname.Append("g");
  gname.Append(ParamName[iYield]);
  hname.Append(ParamName[iYield]);
  if(addStat) gname.Append("StatSyst_");
  else gname.Append("Syst_");
  gname.Append(GetName());
  g->SetName(gname);

  if(nAverage>0) {
    int np = g->GetN();
    for(int i=0;i<np;i++) { // 0 to nbins-1
      double Y       = vNominalByBin[iYield][i+1];

      double Yaverage  = 0;
      double Ylaverage = 0;
      double Yhaverage = 0;
      int    Naverage  = 0;
      for(int iw=i-nAverage;iw<=i+nAverage;iw++) {
	if(iw<0)  continue;
	if(iw>=np) continue;
	Naverage++;
	Yaverage  += vNominalByBin[iYield][iw+1];
	Yhaverage += g->GetEYhigh()[iw];
	Ylaverage += g->GetEYlow()[iw];
      }
      if(Naverage>0) {
	Yaverage  /= Naverage;
	Yhaverage /= Naverage;
	Ylaverage /= Naverage;
      }
      
      double Yh,Yl;
      if(Yaverage>0) {
	double w  = Y/Yaverage;
	Yh = w*Yhaverage;
	Yl = w*Ylaverage;
      } else {
	Yh = g->GetEYhigh()[i];
	Yl = g->GetEYlow()[i];
      }
      
      g->SetPointEYhigh(i,Yh);
      g->SetPointEYlow(i,Yl);
    }
  }

  // Add stat error to graph
  if(addStat) {
    int np = g->GetN();
    for(int i=0;i<np;i++) {
      double Ys      = vStatErrByBin[iYield][i+1];
      double Yh      = g->GetEYhigh()[i];
      double Yl      = g->GetEYlow()[i];
      double h       = TMath::Sqrt(Yh*Yh+Ys*Ys);
      double l       = TMath::Sqrt(Yl*Yl+Ys*Ys);
      g->SetPointEYhigh(i,h);
      g->SetPointEYlow(i,l);
    }
  }

  return g;
}

void SB2x2D::SetParameter(int i, double value)
{
  if(i<0) return;
  if(i>=NPARAMS) return;
  pin.SetValue(i,value);
  pout.SetValue(i,value);
}

void SB2x2D::SetParameterAndError2(int i, double value, double error2)
{
  if(i<0) return;
  if(i>=NPARAMS) return;
  pin.SetValue(i,value);
  pout.SetValue(i,value);
  pin.SetError2(i,error2);
  pout.SetError2(i,error2);
}

void SB2x2D::SetParameterError2(int i, double error2)
{
  if(i<0) return;
  if(i>=NPARAMS) return;
  pin.SetError2(i,error2);
  pout.SetError2(i,error2);
}

void SB2x2D::PrintSystematics(TString opt, ofstream *out, TString tag )
{

  opt.ToLower();
  if(opt.Contains("ll")) {
    for(int i=0;i<=PURITY;i++) {
      std::cout << std::setw(11) << ParamName[i] << " = " << GetNomSystematics(i) << " + " << GetPosSystematics(i) << " - " << GetNegSystematics(i)   << std::endl;
      TString parName = tag+ParamName[i];
      if(out) (*out) << std::setw(25) << parName << "= " << GetNomSystematics(i) << " +" << GetPosSystematics(i) << " -" << GetNegSystematics(i) << std::endl;
    }
  }
  if(opt.Contains("ii")) {
    for(int i=WGGII;i<=PURITYII;i++) {
      std::cout << std::setw(11) << ParamName[i] << " = " << GetNomSystematics(i) << " + " << GetPosSystematics(i) << " - " << GetNegSystematics(i)   << std::endl;
      TString parName = tag+ParamName[i];
      if(out) (*out) << std::setw(25) << parName << "= " << GetNomSystematics(i) << " +" << GetPosSystematics(i) << " -" << GetNegSystematics(i) << std::endl;
    }
  }
  if(opt.Contains("titi")) {
    
    for(int i=WGGTITI;i<=PURITYTITI;i++) {
      std::cout << std::setw(11) << ParamName[i] << " = " << GetNomSystematics(i) << " + " << GetPosSystematics(i) << " - " << GetNegSystematics(i)   << std::endl;
      TString parName = tag+ParamName[i];
      if(out) (*out) << std::setw(25) << parName << "= " << GetNomSystematics(i) << " +/-" << sqrt( GetOutputError2(i) ) << " +" << GetPosSystematics(i) << " -" << GetNegSystematics(i) << std::endl;
        
    }
    
        
    if(opt.Contains("bin")) {
      int nbin = GetNbinsX();
      for(int ibin=0;ibin<=nbin+1;ibin++) {
	{ int i=WGGTITI;
	  std::cout << std::setw(11) << ParamName[i] << "[" << ibin << "] = " << GetNomSystematics(i,ibin) << " + " << LastPosSystematics(i,ibin) << " - " << LastNegSystematics(i,ibin) << std::endl;
	}
      }
    }
  }
}

void SB2x2D::PrintLastSystematics(TString opt, ofstream *out, TString tag )
{
  opt.ToLower();
  if(opt.Contains("ll")) {
    for(int i=0;i<=PURITY;i++) {
      std::cout << std::setw(11) << ParamName[i] << " = " << GetNomSystematics(i) << " + " << LastPosSystematics(i) << " - " << LastNegSystematics(i) << std::endl;
      TString parName = tag+ParamName[i];
      if(out) (*out) << std::setw(25) << parName << "= " << GetNomSystematics(i) << " +" << LastPosSystematics(i) << " -" << LastNegSystematics(i) << std::endl;
    }
  }
  if(opt.Contains("ii")) {
    for(int i=WGGII;i<=PURITYII;i++) {
      std::cout << std::setw(11) << ParamName[i] << " = " << GetNomSystematics(i) << " + " << LastPosSystematics(i) << " - " << LastNegSystematics(i) << std::endl;
      TString parName = tag+ParamName[i];
      if(out) (*out) << std::setw(25) << parName << "= " << GetNomSystematics(i) << " +" << LastPosSystematics(i) << " -" << LastNegSystematics(i) << std::endl;
    }
  }
  if(opt.Contains("titi")) {
    for(int i=WGGTITI;i<=PURITYTITI;i++) {  
      std::cout << std::setw(11) << ParamName[i] << " = " << GetNomSystematics(i) << " + " << LastPosSystematics(i) << " - " << LastNegSystematics(i) << std::endl;
      TString parName = tag+ParamName[i];
      if(out) (*out) << std::setw(25) << parName << "= " << GetNomSystematics(i) << " +/-" << sqrt( GetOutputError2(i) ) << " +" << LastPosSystematics(i) << " -" << LastNegSystematics(i) << std::endl;
    }
    
    if(opt.Contains("bin")) {
      int nbin = GetNbinsX();
      for(int ibin=0;ibin<=nbin+1;ibin++) {
	{int i=WGGTITI;
	  std::cout << std::setw(11) << ParamName[i] << "[" << ibin << "] = " << GetNomSystematics(i,ibin) << " + " << LastPosSystematics(i,ibin) << " - " << LastNegSystematics(i,ibin) << std::endl;
	}
      }
    }
  }
}

void SB2x2D::SetExtraConstraints()
{
  vConstraints.clear();

  // Set extra constraint according to inputs: Fit if error is set to not zero

  // Efficiencies and fake rates, except f' Tight
  for(int ip=E1I;ip<=FP2I;ip++) {
    if(pcur.GetError2(ip)>0) vConstraints.push_back(ip);
  }

  // Correlations
  for(int ip=X1G;ip<NPARAMS;ip++) {
    if(pcur.GetError2(ip)>0) vConstraints.push_back(ip);
  }
}

void SB2x2D::PrintExtraConstraints() const
{
  int n = vConstraints.size();
  for(int ic=0;ic<n;ic++) {
    int i = vConstraints.at(ic);
    printf( "%-14.14s = %g +/- %g (Prediction: %g dchi2=%g)\n",
	    ParamName[i],pout.GetValue(i),pcur.GetError(i),pcur.GetValue(i),GetPChi2(ic));
  }
}

SB2x2DSolver *solver1 = 0;
SB2x2DSolver *solver2 = 0;
SB2x2D       *sideband = 0;

double fchi2(const double *par)
{
  double * p = (double *) par;

  p[FP1T] = p[F1T];
  p[FP2T] = p[F2T];

  double chi2a, chi2b, chi2c;
  if(solver1) {
    solver1->SetParameters(par);
    chi2a = solver1->GetChi2();
  } else chi2a = 0;
  if(solver2) {
    double tmp[4];
    for(int i=0;i<4;i++) {
      tmp[i] = p[WGGTITI+i];
      p[WGGTITI+i] = p[WGG+i];
    }
    solver2->SetParameters(par);
    chi2b = solver2->GetChi2();
    for(int i=0;i<4;i++) {
      p[WGGTITI+i] = tmp[i];
    }
  } else chi2b = 0;
  sideband->SetOutputValues(par);
  chi2c = sideband->GetExtraChi2();
  return chi2a+chi2b+chi2c;
}

#define EFF_MIN 0.781
#define EFF_MAX 1.0
#define FAKI_MIN 0.05
#define FAKI_MAX 0.5
#define FAKT_MIN 0.1
#define FAKT_MAX 0.78
#define COR_MIN 0.95
#define COR_MAX 1.05
#define CORJJ_MIN 0.9
#define CORJJ_MAX 2.1

// amount to change by in iterations
#define dEFF 0.03
#define dFAKI 0.1
#define dFAKT 0.1
#define dCOR 0.1
#define dCORJJ 0.1

// boundaries we don't go past
#define MINEFF 0.01
#define MAXEFF 1.0
#define MINCOR 0.1
#define MAXCOR 3.0

void SB2x2D::SetStartingValues(int ibin, int ibin1, int ibin2)
{
  // Copy input values
  pcur = pin;
  if(do_me) pcur = pout;
  // If per bin, get input from histograms if they exists
  if(ibin>=0) {
    for(int ip=0;ip<NPARAMS;ip++) {
      if(hInput[ip] && !do_me) {
        TH1D *h = hInput[ip];
	double ysum = 0;
	double err2 = 0;
	for(int ib=ibin1;ib<=ibin2;ib++) {
	  double w2 = h->GetSumw2()->fArray[ib];//h->GetBinError(ib);
	  if(w2>0) {
	    double y = h->GetBinContent(ib);
	    err2 += 1/w2;
	    ysum += y/w2;
	  }
	}
	ysum /= err2;
	err2 = 1/err2;
        pcur.SetValue(ip,ysum);
        pcur.SetError2(ip,err2);
      } else {
	//std::cout << "No input histogram for parameter " << ParamName[ip] << " using global value" << std::endl;
      }
    }
  }
  
  // Fill a few starting values with simplest/isolation method
  SB2x2DSolver sol;
  if(pcur.GetError2(E1T)==0) mInput=SB2x2DSolver::ISOLATION;
  else                                            mInput=SB2x2DSolver::SIMPLEST;
  // Per range - considered bin for solver2 yields  
  sol.Fill(this,ibin1,ibin2,ibin);
  for(int ip=0;ip<WGJJG;ip++) {
    pcur.SetValue(ip,sol.GetValue(ip+WGGTITI));
  }
  
  // Per bin for solver1 yields
  sol.Fill(this,ibin,-1);
  for(int ip=WGJJG;ip<E1I;ip++) {
    pcur.SetValue(ip,sol.GetValue(ip));
  }
  
  //Global values for other parameters
  sol.Fill(this,-1,-1);
  for(int ip=E1I;ip<NPARAMS;ip++) {
    if(pcur.GetError2(ip)==0) pcur.SetValue(ip,sol.GetValue(ip));
  }
  
  mInput=SB2x2DSolver::FIT;
}

void SB2x2D::SetMinimizerParameters(ROOT::Math::Minimizer *min, double naa1, double naa2, int nd = 0)
{
  // Yields: Only fit TITI main yields
  // Trick if fitting on two regions use standard yields for the second region
  if(mDoGathering && naa2>0) {
    std::cout<<"Zihang mDoGathering && naa2>0, float WGG -> WJJ"<<std::endl;
    int ip = WGG;
    min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,0.0,naa2);
    for(int ip=WGJ;ip<=WJJ;ip++) {
      min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,0.0,naa2);
    }
  } else {
    for(int ip=0;ip<=WJJ;ip++) {
      //std::cout<<"Zihang fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
      min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
    }
  }
  for(int ip=WGJJG;ip<=ALPHAII;ip++) {
    //std::cout<<"Zihang fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
  }
  int ip = WGGTITI;
  //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
  min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,0.0,naa1);
  for(int ip=WGJTITI;ip<=WJJTITI;ip++) {
    //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,0.0,naa1);
  }
  for(int ip=WGJJGTITI;ip<=ALPHATITI;ip++) {
    //std::cout<<"Zihang fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
  }

  if(do_me) {
    for(int ip=E1I;ip<NPARAMS;ip++) {
      //std::cout<<"Zihang do_me, fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
      min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
    }
    return;
  }

  // Fit all efficiencies and fake rates except f' Tight
  for(int ip=E1I;ip<F1I;ip++) {
    //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(EFF_MIN-nd*dEFF,MINEFF),TMath::Min(EFF_MAX+nd*dEFF,MAXEFF));
  }
  for(int ip=F1I;ip<=F2I;ip++) {
    //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(FAKI_MIN-nd*dFAKI,MINEFF),TMath::Min(FAKI_MAX+nd*dFAKI,MAXEFF));
  }
  for(int ip=F1T;ip<=F2T;ip++) {
    //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(FAKT_MIN-nd*dFAKT,MINEFF),TMath::Min(FAKT_MAX+nd*dFAKT,MAXEFF));
  }
  for(int ip=FP1I;ip<=FP2I;ip++) {
    //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(FAKI_MIN-nd*dFAKI,MINEFF),TMath::Min(FAKI_MAX+nd*dFAKI,MAXEFF));
  }


  for(int ip=FP1T;ip<=FP2T;ip++) {
    //std::cout<<"Zihang fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
    min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
  }

  // Correlations: Fit if error is set to not zero, except for xIjj always fit
  for(int ip=X1G;ip<XIJJ;ip++) {
  // ========== float =================
//   std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
//   min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(COR_MIN-nd*dCOR,MINCOR),TMath::Min(COR_MAX+nd*dCOR,MAXCOR));
  // =========end of float ==============
   if(pcur.GetError2(ip)>0) {
     //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
     min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(COR_MIN-nd*dCOR,MINCOR),TMath::Min(COR_MAX+nd*dCOR,MAXCOR));
   }
   else {
     //std::cout<<"Zihang fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
     min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
   }
  }
  ip = XIJJ;
  //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
  min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(CORJJ_MIN-nd*dCORJJ,MINCOR),TMath::Min(CORJJ_MAX+nd*dCORJJ,MAXCOR));
  for(int ip=XIJJ+1;ip<NPARAMS;ip++) {
  // ========== float =================
  // std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
  // min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(COR_MIN-nd*dCOR,MINCOR),TMath::Min(COR_MAX+nd*dCOR,MAXCOR));
  // =========end of float ==============
   if(pcur.GetError2(ip)>0) {
     //std::cout<<"Zihang floating "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
     min->SetLimitedVariable(ip,ParamName[ip],pcur.GetValue(ip),0.001,TMath::Max(COR_MIN-nd*dCOR,MINCOR),TMath::Min(COR_MAX+nd*dCOR,MAXCOR));
   }
   else {
     //std::cout<<"Zihang fixing "<<ParamName[ip]<<" to "<<pcur.GetValue(ip)<<std::endl;
     min->SetFixedVariable(ip,ParamName[ip],pcur.GetValue(ip));
   }
  }

}

void SB2x2D::GetMinosErrors(ROOT::Math::Minimizer *min)
{
  for(int ip=0;ip<NPARAMS;ip++) {
    double low,up;
    bool status = min->GetMinosError(ip,low,up,0);
    if(status) {
      pout.SetErrorHigh(ip,up);
      pout.SetErrorLow(ip,low);
    } else {
      double err = min->Errors()[ip];
      pout.SetErrorHigh(ip,err);
      pout.SetErrorLow(ip,err);
    }
  }
}

void SB2x2D::Fit(int ibin)
{
  SetInputMode(SB2x2DSolver::FIT);

  double naa1;
  if(ibin==-1) naa1 = GetN(0,0);
  else naa1 = GetNHist(0,0)->GetBinContent(ibin);

  if(naa1==0) {
    pout.SetDefaults();
    return;
  }

  //mDoGathering = 1;
  
  // gather bins
  int nbin  = GetNHist(0,0)->GetNbinsX();
  int ibin1 = ibin;
  int ibin2 = ibin;
  double naa2 = 0;

  if(ibin>=0 && mDoGathering) {
    if(ibin==0 || ibin==nbin+1) { // Overflows and underflows
      ibin1 = 0;
      ibin2 = nbin+1;
    } else {
      GatherBins(GetNHist(0,0), ibin, ibin1, ibin2);
      if(ibin1<0) ibin1 = 0;
      if(ibin2>nbin+1) ibin2 = nbin+1;
      //std::cout << "Gather bins: " << ibin << " => " << ibin1 << " " << ibin2 << std::endl;
    }
    for(int ib=ibin1;ib<=ibin2;ib++) {
      if(ib==ibin) continue;
      double n = GetNHist(0,0)->GetBinContent(ib);
      naa2 += n;
    }
  }

  SetStartingValues(ibin,ibin1,ibin2);
  std::cout << "Starting minimization ibin = " << ibin << " naa = " << naa1  << " " << naa2 << std::endl;

  // Setup input constraints in the sideband object
  SetExtraConstraints();
  std::cout << " Zihang PrintExtraConstraints ====== "<<std::endl;
  PrintExtraConstraints();
   std::cout << " end PrintExtraConstraints ====== "<<std::endl;
  sideband = this;

  // Setup solver 1 for current bin
  SB2x2DSolver sol1;
  sol1.Fill(this,ibin,-1);
  solver1 = &sol1;
  // Setup solver 2 for current bin
  SB2x2DSolver sol2;
  if(ibin>=0 && ibin1!=ibin2) {
    sol2.Fill(this,ibin1,ibin2,ibin);
    solver2 = &sol2;
  } else solver2 = 0;


  // Setup the minimizer
  ROOT::Math::Functor f(&fchi2, NPARAMS);
  ROOT::Math::Minimizer *min=ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");
  //ROOT::Math::Minimizer *min=ROOT::Math::Factory::CreateMinimizer("Minuit2", "Simplex");
  //ROOT::Math::Minimizer *min=ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "");
  min->SetMaxFunctionCalls(1000000); 
  min->SetMaxIterations(10000); 
  min->SetTolerance(0.001); 
  min->SetFunction(f);
  min->SetPrintLevel(0);
  bool status = false;
  int nd = 0;
  bool redo = true;
  do {
    SetMinimizerParameters(min,naa1,naa2,nd);
    //    min->PrintResults();
    bool status = min->Minimize();
    std::cout<<"Zihang doing Minimization"<<std::endl;
    
    if(status) {
      std::cout << GetName() << ": Minimization successful, bin = " << ibin << "/" << nbin+1 << " nAA = " << naa1 << " " << naa2 << " and nd = " << nd << std::endl;
      solver1->SetParameters(min->X());
      SetOutputValues(min->X());
      SetOutputErrors(min->Errors());
      pout.SetValue(FP1T,pout.GetValue(F1T));
      pout.SetValue(FP2T,pout.GetValue(F2T));
      //if(ibin==58) {
      //min->PrintResults();
      //  Print("pred results ll titi");
      //}
      //GetMinosErrors(min);
    } 
    else {
      std::cout << GetName() << ": Minimization failed, bin = " << ibin << "/" << nbin+1 << " nAA = " << naa1 << " " << naa2 << " and nd = " << nd << std::endl;
      nd++;
      solver1->SetParameters(min->X());
      SetOutputValues(min->X());
      SetOutputErrors(min->Errors());
      pout.SetValue(FP1T,pout.GetValue(F1T));
      pout.SetValue(FP2T,pout.GetValue(F2T));
      //min->PrintResults();
      //Print("pred results ll titi");
      //exit(1);
      //if(ibin>=0) {
      //  std::cout << "First minimization failed, trying to minimize only the yields" << std::endl;
      //  // Using global output
      //  Fit(-1);
      //  int tmp = mInput; 
      //  mInput = SB2x2DSolver::NOTHING; 
      //  Solve(ibin); 
      //  mInput = tmp;
      //} else {
      //  std::cout << "Minimizer failed!" << std::endl;
      //  pout.SetDefaults();
      //  exit(1);
      //}
    }
    redo = (nd <= 10 && !status);
  } while(redo==true); // stopping after 10x extra no matter what!
    
  mNFree = min->NFree();
  SetExtraTITI();
  //Print("results pred titi ll");
  //exit(1);
}

void SB2x2D::GatherBins(TH1D *h, int ibin, int& ibin1, int& ibin2)
{
  //std::cout << "Gathering bins: " << fName << " mode = " << mDoGathering << " bin = " << ibin << std::endl;
  int n    = vGatherBins.size();
  int nbin = h->GetNbinsX();
  // ibin is in between 1 and nbin, we don't care about underflows and overflows
  if(ibin<=0 || ibin>nbin) {
    //std::cout << fName << " No gather n = " << n << " ibin = " << ibin << " nbin = " << nbin << std::endl;
    ibin1=ibin2=ibin;
    return;
  }
  if(n==0) { // No specified gathering, gather left or right bin for systematics studies
    ibin1=ibin2=ibin;
    if(ibin1>1 && mDoGathering==2) ibin1--;
    if(ibin2<nbin && mDoGathering==3) ibin2++;
    //std::cout << "Special gathering of two bins: mode = " << mDoGathering << " " << ibin1 << " " << ibin2 << std::endl;
    return;
  }
  // Find gathered binning index
  double x = h->GetBinCenter(ibin);
  //std::cout << ibin << " " << mDoGathering << " x = " << x << std::endl;
  int index = 0;
  for(int i=0;i<n;i++) {
    if(x<vGatherBins[i]) {
      index = i;
      break;
    }
  }
  if(index>0 && mDoGathering==2) index--;
  if(index<n-1 && mDoGathering==3) index++;
  if(index==0) index++;
  if(index==n) index--;
  //std::cout << ibin << " " << mDoGathering << " ibin_gathered = " << index << std::endl;
  double x1 = vGatherBins[index-1];
  double x2 = vGatherBins[index];
  //std::cout << ibin << " " << ibin << " " << mDoGathering << " x1 = " <<  x1 << " x2 = " << x2 << std::endl;
  ibin1 = h->GetXaxis()->FindBin(x1);
  ibin2 = h->GetXaxis()->FindBin(x2)-1;
  if(ibin2<ibin1) ibin1=ibin2=ibin;
}
  
}
