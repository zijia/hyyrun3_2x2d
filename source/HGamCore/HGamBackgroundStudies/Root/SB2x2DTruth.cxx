#include <stdio.h>
#include "HGamBackgroundStudies/SB2x2DTruth.h"

namespace SB {

SB2x2DTruth::SB2x2DTruth() :  TNamed()
{
  fSignal = 0;
}


SB2x2DTruth::SB2x2DTruth(TString name, TString title, std::vector<int*>& flags, Int_t nbinsx, double* xbins) :  TNamed(name,title)
{
  fSignal  = new SB2x2D(MakeName(name,"_truth"), MakeTitle(title,"_truth"),flags,nbinsx,xbins);
  fIsTruth = flags.at(IsTrueSignal);
}


SB2x2DTruth::~SB2x2DTruth()
{
  if(fSignal) delete fSignal;
}

double SB2x2DTruth::GetEntries() const
{
  if(fSignal==0) return 0;
  return fSignal->GetEntries();
}

void SB2x2DTruth::Print(Option_t* option) const
{
  printf( "%s.Print Name  = %s, Title = %s\n",IsA()->GetName(),fName.Data(),fTitle.Data());

  TString opt = option;
  opt.ToLower();
  //std::cout << "Options = " << option << std::endl;

  if(opt.Contains("truth") && fSignal) {
    fSignal->Print(opt);
  }

  printf("// Parameters:\n");
  for(int ip=E1I;ip<NPARAMS;ip++) {
    double x  = GetTruthValue(ip);
    double w2 = GetTruthError2(ip);
    if(w2<=0) continue;
    TString pname = ParamName[ip];
    if(opt.Contains("gj")) {
      if(pname.Contains("2")) {
	if(ip>=E1I && ip<X1G) pname = ParamName[ip+8];
	else if(ip>=X1G) pname = ParamName[ip+2];
      }
      if(pname.Contains("GG")) {
	pname = ParamName[ip+1];
      }
    }
    if(opt.Contains("jg")) {
      if(pname.Contains("1")) {
	if(ip>=E1I && ip<X1G) pname = ParamName[ip+8];
	else if(ip>=X1G) pname = ParamName[ip+2];
      }
      if(pname.Contains("GG")) {
	pname = ParamName[ip+2];
      }
    }
    printf( "%-14.14s = %g +/- %g\n",pname.Data(),x,TMath::Sqrt(w2));
  }
}

void SB2x2DTruth::Fill(double x, double weight)
{
  if(fSignal==0) {
    std::cout << IsA()->GetName() << " " << fName << ": your object has no associated histograms..." << std::endl;
    exit(1);
  }

  if(*fIsTruth) fSignal->Fill(x,weight);
}

double SB2x2DTruth::GetTruthValue(int i) const
{
  if(fSignal==0) return 1;

  SB2x2DSolver p;
  p.Fill(fSignal,-1,-1);
  
  return p.GetTruthValue(i);
}

double SB2x2DTruth::GetTruthError2(int i) const
{
  if(fSignal==0) return 0;

  SB2x2DSolver p;
  p.Fill(fSignal,-1,-1);

  return p.GetTruthError2(i);
}

TH1D* SB2x2DTruth::GetTruthHist(int i) const
{
  if(fSignal==0) {
    std::cout << IsA()->GetName() << " " << fName << ": your object was not properly initialized..." << std::endl;
    exit(1);
  }

  TH1D *fHisto = (TH1D*) fSignal->GetNHist(0,0)->Clone();
  TString hname,htitle;
  hname.Form("h%s_truth_%s",ParamName[i],fName.Data());
  htitle.Form("%s truth %s",ParamName[i],fTitle.Data());
  fHisto->SetName(hname);
  fHisto->SetTitle(htitle);

  Int_t    n = fHisto->GetNbinsX();
  for(Int_t ibin=0;ibin<=n+1;ibin++) {
    SB2x2DSolver p;
    p.Fill(fSignal,ibin,-1);
    
    double w    = p.GetTruthValue(i);
    double werr = TMath::Sqrt(p.GetTruthError2(i));
    fHisto->SetBinContent(ibin,w);
    fHisto->SetBinError(ibin,werr);
  }

  //printf( "%s.GetHistoHist Name  = %s: %s, %s\n",IsA()->GetName(),fName.Data(),hname.Data(),htitle.Data());
  return fHisto;
}

TString SB2x2DTruth::MakeName(TString gname, TString hname)
{
  TString name;
  name.Form("%s_%s",gname.Data(),hname.Data());
  return name;
}

TString SB2x2DTruth::MakeTitle(TString gtitle, TString htitle)
{
  TString title;
  title.Form("%s - %s",gtitle.Data(),htitle.Data());
  return title;
}

TH1D *SB2x2DTruth::GetHistogram(TString hname)
{
  // leakage parameters histograms
  // Parameters and yields
  TH1D *h = 0;
  for(int ip=0;ip<E1I;ip++) {
    if(hname.EqualTo(ParamName[ip])) {
      h = GetTruthHist(ip);
      if(h) return h;
      else break;
    }
  }

  return fSignal->GetHistogram(hname);
}

}
