#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <HGamBackgroundStudies/HGam2x2DInputMaker.h>
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include <typeinfo>
#include <TH1.h>


/// Heper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )     \
        do {              \
                if( ! EXP.isSuccess() ) {       \
                        Error( CONTEXT,                                     \
                                        XAOD_MESSAGE( "Failed to execute: %s" ),            \
#EXP );                                      \
                        return EL::StatusCode::FAILURE;     \
                }             \
        } while( false )

// this is needed to distribute the algorithm to the workers
ClassImp(HGam2x2DInputMaker)


HGam2x2DInputMaker :: HGam2x2DInputMaker ()
{
        // std::cout << "in HGam2x2DInputMaker() " << std::endl;
        // Here you put any code for the base initialization of variables,
        // e.g. initialize all pointers to 0.  Note that you should only put
        // the most basic initialization here, since this method will be
        // called on both the submission and the worker node.  Most of your
        // initialization code will go into histInitialize() and
        // initialize().
        // m_env("env")

}

void HGam2x2DInputMaker :: addCfgFile(Str conf) {
        // std::cout << "addCfgFile() " << std::endl;

        TEnv cfg;
        int file_ok = cfg.ReadFile(conf.Data(),EEnvLevel(0));
        if (file_ok!=0) { Fatal("","Cannot open %s",conf.Data()); abort(); }
        TIter cfg_itr((const TCollection *)cfg.GetTable());
        while (TEnvRec *entry = (TEnvRec*) cfg_itr()){
                if (!confHasVal(entry->GetName())){
                        setConfigVal(entry->GetName(),entry->GetValue());
                }else{
                        if(!confHasEqualVal(entry->GetName(),entry->GetValue()))
                                std::cout<<"<<<<<<<<<<<<<<<<<<<<<<<<< Warning Inconsistant values for config entry: "
                                        << entry->GetName() << " :: " << entry->GetValue() << " vs. "
                                        << m_config->GetValue(entry->GetName(),"") <<std::endl;
                }
        }
}

TString HGam2x2DInputMaker::getMCSampleName(int mcID)
{
        // if (mcID == -1) { mcID = eventInfo()->mcChannelNumber(); }

        if (m_mcNames.find(mcID) == m_mcNames.end())
        { m_mcNames[mcID] = m_config->GetValue(Form("SampleName.%d", mcID), "");
                m_mcNames[mcID].ReplaceAll(" ", ""); }

        return m_mcNames[mcID];
}


double HGam2x2DInputMaker::getlumiXsecWeight(double lumi, int mcID, bool printFirst)
{


        if (lumi < 0) { lumi = 140.0;}
        //config()->getNum("IntegratedLuminosity_fbInv" + HG::mcType(), 1.0); 

        if (m_weightXsec.find(mcID) == m_weightXsec.end()) {
                double sigma      = getCrossSection(mcID);
                double gen_eff    = getGeneratorEfficiency(mcID);
                double kFactor    = getKFactor(mcID);

                // Hard-coding to bin number 1,2
                double sumInitial = 0, NxAOD = 1.0, NDxAOD = 1.0;
                // sumInitial = getIntialSumOfWeights(mcID);

                if (ish027){//fullsim
                        // ==================================
                        // h027
                        if (isMCa) {sumInitial = 13657083.25;}  //hardcoded to sum of bin 3 since sherpa files are split
                        else if (isMCd){sumInitial = 17291509.8594;}
                        else if (isMCe){sumInitial = 357017312.0 / 257313248.0 * 257832240;}
                        // Zihang sum_of_weight_mc16e =(histo_mc16e->GetBinContent(1)/histo_mc16e->GetBinContent(2))*histo_mc16e->GetBinContent(3);
                        // ==================================
                        //else if (isMCe){sumInitial = 22950038.3203;}
                        //std::cout<<"sumInitial = "<<sumInitial<<std::endl;


                        // //fastsim
                        // if (isMCa) {sumInitial = 71525893.3125;} //hardcoded to sum of bin 3 since sherpa files are split
                        // else if (isMCd){sumInitial = 88419472.375;}
                        // else if (isMCe){sumInitial = 122988927.742;}
                }

                if (ish029) {
                        // h029
                        if (isMCa) {sumInitial = 9.5504623e+10;}  //hardcoded to sum of bin 3 since mg  files are split

                }

                if (ish024){//fullsim
                        if (isMCa) {sumInitial = 8831045.4375;}  //hardcoded to sum of bin 3 since sherpa files are split
                        else if (isMCd){sumInitial = 9014737.22266;}
                        else if (isMCe){sumInitial = 14494338.3438;}
                }
                //
                // // NxAOD      = getCutFlowHistogram(mcID, "_weighted")->GetBinContent(1);
                //     //fullsim
                //     if (isMCa) {NxAOD = 113656765.75;} //hardcoded to sum of bin 1 since sherpa files are split
                //     else if (isMCd){NxAOD = 17295405.9844;}
                //     else if (isMCe){NxAOD = 22949687.8828;}
                //
                //     // //fastsim
                //     // if (isMCa) {NxAOD = 104661957.367;} //hardcoded to sum of bin 1 since sherpa files are split
                //     // else if (isMCd){NxAOD = 131042856.344;}
                //     // else if (isMCe){NxAOD = 172528768.074;}
                //
                // // NDxAOD     = getCutFlowHistogram(mcID, "_weighted")->GetBinContent(2);
                //     //fullsim
                //     if (isMCa) {NDxAOD = 13656765.75;} //hardcoded to sum of bin 2 since sherpa files are split
                //     else if (isMCd){NDxAOD = 17295405.9844;}
                //     else if (isMCe){NDxAOD = 22949687.8828;}

                // //fastsim
                // if (isMCa) {NDxAOD = 71493076.7344;} //hardcoded to sum of bin 2 since sherpa files are split
                // else if (isMCd){NDxAOD = 88257951.1875;}
                // else if (isMCe){NDxAOD = 122725517.996;}

                double skim_eff = 1.; // fullsim is not filtered.....   NDxAOD / NxAOD;

                m_weightXsec[mcID] = lumi * 1e3 * sigma * gen_eff * skim_eff * kFactor / sumInitial;

                if (printFirst) {
                        printf("\nMC sample %d: %s\n", mcID, getMCSampleName(mcID).Data());
                        printf("  Cross section:                %10.4e pb\n", sigma);

                        if (gen_eff != 1.0) { printf("  Generator efficiency:         %10.4e\n", gen_eff); }

                        if (kFactor != 1.0) { printf("  k-factor:                     %10.2f\n", kFactor); }

                        printf("  sum w in xAOD:                %10.2f\n", NxAOD);
                        printf("  sum w in DxAOD:               %10.2f\n", NDxAOD);

                        if (skim_eff != 1.0) { printf("  DxAOD efficiency:             %10.2f%%\n", skim_eff * 100); }

                        printf("  Sum of inital event weights:  %10.2f\n\n", sumInitial);

                        // L * sigma * eff * kFactor / Nevts
                        printf("  Integrated lumi.:             %10.4f fb-1\n", lumi);
                        printf("  N exp. events for analysis:   %10.2e\n", lumi * 1e3 * sigma * gen_eff * skim_eff * kFactor);
                        printf("  Cross section event weight:   %10.4e\n\n", m_weightXsec[mcID]);
                }
        }

        return m_weightXsec[mcID];
}


double HGam2x2DInputMaker::getIntialSumOfWeights(int mcID)
{

        if (m_NevtsInitial.find(mcID) == m_NevtsInitial.end()) {
                // Hard-coding to bin number 3 = ALLEVTS
                m_NevtsInitial[mcID] = getCutFlowHistogram(mcID, "_noDalitz_weighted")->GetBinContent(3);
        }

        return m_NevtsInitial[mcID];
}

TH1F *HGam2x2DInputMaker::getCutFlowHistogram(int mcID, TString suffix)
{
        // access the initial number of weighed events
        TString cutFlowName(Form("CutFlow_%s%d%s", isData ? "Run" : "MC", mcID, suffix.Data()));
        bool hasMCname = m_config->Defined(Form("SampleName.%d", mcID));

        if (hasMCname) cutFlowName = Form("CutFlow_%s%s",
                        getMCSampleName(mcID).Data(), suffix.Data());

        TH1F *cflowHist = (TH1F *)wk()->inputFile()->Get(cutFlowName);

        if (cflowHist == nullptr)
        { StatusCode::FAILURE;}

        return cflowHist;
}

double HGam2x2DInputMaker::getCrossSection(int mcID)
{

        if (m_crossSections.find(mcID) == m_crossSections.end())
        { m_crossSections[mcID] = m_config->GetValue(Form("CrossSection.%d", mcID), -99.0); }

        return m_crossSections[mcID];
}

double HGam2x2DInputMaker::getGeneratorEfficiency(int mcID)
{

        if (m_genEffs.find(mcID) == m_genEffs.end())
        { m_genEffs[mcID] = m_config->GetValue(Form("GeneratorEfficiency.%d", mcID), 1.0); }

        return m_genEffs[mcID];
}

double HGam2x2DInputMaker::getKFactor(int mcID)
{

        if (m_kFactors.find(mcID) == m_kFactors.end())
        { m_kFactors[mcID] = m_config->GetValue(Form("kFactor.%d", mcID), 1.0); }

        return m_kFactors[mcID];
}

EL::StatusCode HGam2x2DInputMaker :: setupJob (EL::Job& job)
{
        // std::cout << "setupJob() " << std::endl;
        // Here you put code that sets up the job on the submission object
        // so that it is ready to work with your algorithm, e.g. you can
        // request the D3PDReader service or add output files.  Any code you
        // put here could instead also go into the submission script.  The
        // sole advantage of putting it here is that it gets automatically
        // activated/deactivated when you add/remove the algorithm from your
        // job, which may or may not be of value to you.

        job.useXAOD ();
        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: histInitialize ()
{
        // std::cout << "histInitialize() " << std::endl;
        // Here you do everything that needs to be done at the very
        // beginning on each worker node, e.g. create histograms and output
        // trees.  This method gets called before any input files are
        // connected.

        //integrated luminosity per period, fb-1
        intLumi["MC16a"] = 36.24;
        intLumi["MC21a"] = 31.40; // h029 so far
        intLumi["MC16d"] = 44.31;
        //intLumi["MC16e"] = 35; // commented by Zihang
        intLumi["MC16e"] = 58.45;

        tree = new TTree("SidebandInputs", "tree with SidebandInputs");
        // tree->SetAutoFlush(100000);
        
        tree->Branch("y1_Rhad1", &y1_Rhad1);
        tree->Branch("y1_Rhad", &y1_Rhad);
        tree->Branch("y1_Reta", &y1_Reta);
        tree->Branch("y1_weta2", &y1_weta2);
        tree->Branch("y1_Rphi", &y1_Rphi);
        tree->Branch("y1_Eratio", &y1_Eratio);
        tree->Branch("y1_f1", &y1_f1);

        tree->Branch("y2_Rhad1", &y2_Rhad1);
        tree->Branch("y2_Rhad", &y2_Rhad);
        tree->Branch("y2_Reta", &y2_Reta);
        tree->Branch("y2_weta2", &y2_weta2);
        tree->Branch("y2_Rphi", &y2_Rphi);
        tree->Branch("y2_Eratio", &y2_Eratio);
        tree->Branch("y2_f1", &y2_f1);

        tree->Branch("y1_pt",   &y1_pt);
        tree->Branch("y1_eta",   &y1_eta);
        tree->Branch("y1_phi",   &y1_phi);
        tree->Branch("y1_m",   &y1_m);
        tree->Branch("y2_pt",   &y2_pt);
        tree->Branch("y2_eta",   &y2_eta);
        tree->Branch("y2_phi",   &y2_phi);
        tree->Branch("y2_m",   &y2_m);
        tree->Branch("y1_eta_s2",   &y1_eta_s2);
        tree->Branch("y2_eta_s2",   &y2_eta_s2);
        tree->Branch("mu",   &mu);
        // tree->Branch("VBF_abs_Zepp",   &VBF_abs_Zepp);
        tree->Branch("y1_topoetcone20",   &y1_topoetcone20);
        tree->Branch("y2_topoetcone20",   &y2_topoetcone20);
        tree->Branch("y1_ptcone20",   &y1_ptcone20);
        tree->Branch("y2_ptcone20",   &y2_ptcone20);
        tree->Branch("weight",   &weight);
        // tree->Branch("weightSF",   &weightSF);
        // tree->Branch("pileupWeight",   &pileupWeight);
        // tree->Branch("vertexWeight",   &vertexWeight);
        // tree->Branch("weightInitial",   &weightInitial);
        tree->Branch("weight_lumiXsec",   &weight_lumiXsec);
        //  tree->Branch("weight_tot",   &weight_tot);
        //  tree->Branch("yybb_lowMass_weight",   &yybb_lowMass_weight);
        // tree->Branch("weightCatCoup_Moriond2017",   &weightCatCoup_Moriond2017);
        // tree->Branch("weightCatCoup_Moriond2017BDT",   &weightCatCoup_Moriond2017BDT);
        // tree->Branch("weightCatXS_ttH",   &weightCatXS_ttH);
        // tree->Branch("weightCatXS_VBF",   &weightCatXS_VBF);
        // tree->Branch("weightCatXS_lepton",   &weightCatXS_lepton);
        // tree->Branch("weightN_lep_15",   &weightN_lep_15);
        // tree->Branch("weightCatXS_nbjet",   &weightCatXS_nbjet);
        // tree->Branch("met_weight",   &met_weight);
        // tree->Branch("weightJvt_30",   &weightJvt_30);
        // tree->Branch("weight_final",   &weight_final);
        // tree->Branch("HiggsHF_weight",   &HiggsHF_weight);
        tree->Branch("m_yy",   &m_yy);
        // tree->Branch("pT_yy",   &pT_yy);
        // tree->Branch("yAbs_yy",   &yAbs_yy);
        // tree->Branch("cosTS_yy",   &cosTS_yy);
        // tree->Branch("pTt_yy",   &pTt_yy);
        // tree->Branch("Dy_y_y",   &Dy_y_y);
        // tree->Branch("pT_y1",   &pT_y1);
        // tree->Branch("pT_y2",   &pT_y2);
        // tree->Branch("eta_j1_30",   &eta_j1_30);
        // tree->Branch("abs_eta_j1_30",   &abs_eta_j1_30);
        // tree->Branch("HT_30",   &HT_30);
        // tree->Branch("pT_j1_30",   &pT_j1_30);
        // tree->Branch("VBF_pT_j1_30",   &VBF_pT_j1_30);
        // tree->Branch("pT_j2_30",   &pT_j2_30);
        // tree->Branch("pT_j3_30",   &pT_j3_30);
        // tree->Branch("yAbs_j1_30",   &yAbs_j1_30);
        // tree->Branch("yAbs_j2_30",   &yAbs_j2_30);
        // tree->Branch("Dphi_j_j_30_signed",   &Dphi_j_j_30_signed);
        // tree->Branch("VBF_Dphi_j_j_30_signed",   &VBF_Dphi_j_j_30_signed);
        // tree->Branch("Dy_j_j_30",   &Dy_j_j_30);
        // tree->Branch("m_jj_30",   &m_jj_30);
        // tree->Branch("pT_yyjj_30",   &pT_yyjj_30);
        // tree->Branch("VBF_pT_yyjj_30",   &VBF_pT_yyjj_30);
        // tree->Branch("Dphi_yy_jj_30",   &Dphi_yy_jj_30);
        // tree->Branch("maxTau_yyj_30",   &maxTau_yyj_30);
        // tree->Branch("sumTau_yyj_30",   &sumTau_yyj_30);
        // tree->Branch("met_TST",   &met_TST);
        tree->Branch("isPassedIsolation",   &isPassedIsolation);
        tree->Branch("isPassedRelPtCuts",   &isPassedRelPtCuts);
        tree->Branch("isPassedMassCut",   &isPassedMassCut);
        //  tree->Branch("cat_ntag",   &cat_ntag);
        //tree->Branch("pass_trigger_HLT_g35_loose_g25_loose",   &pass_trigger_HLT_g35_loose_g25_loose);
        //tree->Branch("HLT_2g20_tight",   &HLT_2g20_tight);
        //tree->Branch("HLT_2g22_tight",   &HLT_2g22_tight);
        //tree->Branch("HLT_2g20_loose",   &HLT_2g20_loose);
        //tree->Branch("HLT_2g20_tight_icalotight_L12EM15VHI",   &HLT_2g20_tight_icalotight_L12EM15VHI);
        //tree->Branch("HLT_2g20_tight_icalovloose_L12EM15VHI",   &HLT_2g20_tight_icalovloose_L12EM15VHI);
        //tree->Branch("HLT_2g22_tight_L12EM15VHI",   &HLT_2g22_tight_L12EM15VHI);
        //tree->Branch("pass_trigger_HLT_g35_medium_g25_medium_L12EM20VH",   &pass_trigger_HLT_g35_medium_g25_medium_L12EM20VH);
        tree->Branch("isPassed",   &isPassed);
        tree->Branch("isPassedPID",   &isPassedPID);
        tree->Branch("isPassedBasic",   &isPassedBasic);
        tree->Branch("isPassedJetEventClean",   &isPassedJetEventClean);
        tree->Branch("isPassedTriggerMatch",   &isPassedTriggerMatch);
        tree->Branch("cutFlow",   &cutFlow);
        // tree->Branch("yybb_lowMass_cutFlow",   &yybb_lowMass_cutFlow);
        // tree->Branch("yybb_bTagCat",   &yybb_bTagCat);
        tree->Branch("y1_convType",   &y1_convType);
        tree->Branch("y2_convType",   &y2_convType);
        tree->Branch("y1_isEMTight",   &y1_isEMTight);
        tree->Branch("y2_isEMTight",   &y2_isEMTight);
        tree->Branch("y1_isEMTight_nofudge",   &y1_isEMTight_nofudge);
        tree->Branch("y2_isEMTight_nofudge",   &y2_isEMTight_nofudge);
        tree->Branch("y1_isTight",   &y1_isTight);
        tree->Branch("y2_isTight",   &y2_isTight);
        tree->Branch("y1_isTight_nofudge",   &y1_isTight_nofudge);
        tree->Branch("y2_isTight_nofudge",   &y2_isTight_nofudge);
        //  tree->Branch("catCoup_Moriond2017BDT",   &catCoup_Moriond2017BDT);
        // tree->Branch("catCoup_Moriond2017",   &catCoup_Moriond2017);
        // tree->Branch("catXS_HiggsHF",   &catXS_HiggsHF);
        // tree->Branch("catXS_VBF",   &catXS_VBF);
        // tree->Branch("catXS_lepton", &catXS_lepton);
        // tree->Branch("catXS_MET80pTyy80",   &catXS_MET80pTyy80);
        // tree->Branch("catXS_MET",   &catXS_MET);
        // tree->Branch("catXS_ttH",   &catXS_ttH);
        // tree->Branch("N_lep_15",   &N_lep_15);
        // tree->Branch("catXS_Nlep",   &catXS_Nlep);
        // tree->Branch("catXS_nbjet",   &catXS_nbjet);
        // tree->Branch("N_j",   &N_j);
        // tree->Branch("N_j_btag30",   &N_j_btag30);
        // tree->Branch("N_j_30",   &N_j_30);
        // tree->Branch("N_j_50",   &N_j_50);
        // tree->Branch("N_j_central30",   &N_j_central30);
        // tree->Branch("N_j_central50",   &N_j_central50);
        tree->Branch("runNumber",   &runNumber);

        // tree->Branch("pT_yy_JV_30",   &pT_yy_JV_30);
        // tree->Branch("pT_yy_JV_40",   &pT_yy_JV_40);
        // tree->Branch("pT_yy_JV_50",   &pT_yy_JV_50);
        // tree->Branch("pT_yy_JV_60",   &pT_yy_JV_60);
        //
        // tree->Branch("m_yyj_30", &m_yyj_30);
        // tree->Branch("pT_yyj_30", &pT_yyj_30);
        // tree->Branch("rel_pT_y1", &rel_pT_y1);
        // tree->Branch("rel_pT_y2", &rel_pT_y2);

        //tree->Branch("VBF_pT_j1_30_vs_Dphi_j_j_30_0t1_04",   &VBF_pT_j1_30_vs_Dphi_j_j_30_0t1_04);
        //tree->Branch("VBF_pT_j1_30_vs_Dphi_j_j_30_1_04t2_08",   &VBF_pT_j1_30_vs_Dphi_j_j_30_1_04t2_08);
        //tree->Branch("VBF_pT_j1_30_vs_Dphi_j_j_30_2_08t3_15",   &VBF_pT_j1_30_vs_Dphi_j_j_30_2_08t3_15);
        //tree->Branch("VBF_pT_j1_30_30t120_vs_Dphi_j_j_30",   &VBF_pT_j1_30_30t120_vs_Dphi_j_j_30);
        //tree->Branch("VBF_pT_j1_30_120t500_vs_Dphi_j_j_30",   &VBF_pT_j1_30_120t500_vs_Dphi_j_j_30);
        //t
        //tree->Branch("VBF_pT_j1_30_vs_Dphi_j_j_30_signed_3_15t0",   &VBF_pT_j1_30_vs_Dphi_j_j_30_signed_3_15t0);
        //tree->Branch("VBF_pT_j1_30_vs_Dphi_j_j_30_signed_0t3_15",   &VBF_pT_j1_30_vs_Dphi_j_j_30_signed_0t3_15);
        //t
        //tree->Branch("pT_yy_0t45_vs_yAbs_yy",   &pT_yy_0t45_vs_yAbs_yy);
        //tree->Branch("pT_yy_45t120_vs_yAbs_yy",   &pT_yy_45t120_vs_yAbs_yy);
        //tree->Branch("pT_yy_120t350_vs_yAbs_yy",   &pT_yy_120t350_vs_yAbs_yy);
        //t
        //tree->Branch("pT_yy_vs_yAbs_yy_0t0_5",   &pT_yy_vs_yAbs_yy_0t0_5);
        //tree->Branch("pT_yy_vs_yAbs_yy_0_5t1_0",   &pT_yy_vs_yAbs_yy_0_5t1_0);
        //tree->Branch("pT_yy_vs_yAbs_yy_1_0t1_5",   &pT_yy_vs_yAbs_yy_1_0t1_5);
        //tree->Branch("pT_yy_vs_yAbs_yy_1_5t2_5",   &pT_yy_vs_yAbs_yy_1_5t2_5);
        //t
        //tree->Branch("maxTau_yyj_30_100t0_vs_pT_yy",   &maxTau_yyj_30_100t0_vs_pT_yy);
        //tree->Branch("maxTau_yyj_30_0t15_vs_pT_yy",   &maxTau_yyj_30_0t15_vs_pT_yy);
        //tree->Branch("maxTau_yyj_30_15t25_vs_pT_yy",   &maxTau_yyj_30_15t25_vs_pT_yy);
        //tree->Branch("maxTau_yyj_30_25t40_vs_pT_yy",   &maxTau_yyj_30_25t40_vs_pT_yy);
        //tree->Branch("maxTau_yyj_30_40t400_vs_pT_yy",   &maxTau_yyj_30_40t400_vs_pT_yy);
        //t
        //t
        //tree->Branch("pT_yyj_30_100t0_vs_pT_yy",   &pT_yyj_30_100t0_vs_pT_yy);
        //tree->Branch("pT_yyj_30_0t30_vs_pT_yy",   &pT_yyj_30_0t30_vs_pT_yy);
        //tree->Branch("pT_yyj_30_30t60_vs_pT_yy",   &pT_yyj_30_30t60_vs_pT_yy);
        //tree->Branch("pT_yyj_30_60t350_vs_pT_yy",   &pT_yyj_30_60t350_vs_pT_yy);
        //t
        //t
        //tree->Branch("pT_yy_0t120_vs_maxTau_yyj_30", &pT_yy_0t120_vs_maxTau_yyj_30);
        //tree->Branch("pT_yy_0t45_vs_maxTau_yyj_30", &pT_yy_0t45_vs_maxTau_yyj_30);
        //tree->Branch("pT_yy_45t120_vs_maxTau_yyj_30", &pT_yy_45t120_vs_maxTau_yyj_30);
        //tree->Branch("pT_yy_120t350_vs_maxTau_yyj_30", &pT_yy_120t350_vs_maxTau_yyj_30);
        //tree->Branch("pT_yy_0t200_vs_maxTau_yyj_30", &pT_yy_0t200_vs_maxTau_yyj_30);
        //tree->Branch("pT_yy_200t350_vs_maxTau_yyj_30", &pT_yy_200t350_vs_maxTau_yyj_30);
        //t
        //tree->Branch("pT_yy_0t45_vs_pT_yyj_30", &pT_yy_0t45_vs_pT_yyj_30);
        //tree->Branch("pT_yy_45t120_vs_pT_yyj_30", &pT_yy_45t120_vs_pT_yyj_30);
        //tree->Branch("pT_yy_120t350_vs_pT_yyj_30", &pT_yy_120t350_vs_pT_yyj_30);
        //t
        //tree->Branch("rel_DpT_y_y_0t0_3_vs_rel_sumpT_y_y", &rel_DpT_y_y_0t0_3_vs_rel_sumpT_y_y);
        //tree->Branch("rel_DpT_y_y_0_3t0_6_vs_rel_sumpT_y_y", &rel_DpT_y_y_0_3t0_6_vs_rel_sumpT_y_y);
        //tree->Branch("rel_DpT_y_y_0_6t4_vs_rel_sumpT_y_y", &rel_DpT_y_y_0_6t4_vs_rel_sumpT_y_y);
        //tree->Branch("rel_DpT_y_y_0t0_05_vs_rel_sumpT_y_y", &rel_DpT_y_y_0t0_05_vs_rel_sumpT_y_y);
        //tree->Branch("rel_DpT_y_y_0_05t0_1_vs_rel_sumpT_y_y", &rel_DpT_y_y_0_05t0_1_vs_rel_sumpT_y_y);
        //tree->Branch("rel_DpT_y_y_0_1t0_2_vs_rel_sumpT_y_y", &rel_DpT_y_y_0_1t0_2_vs_rel_sumpT_y_y);
        //tree->Branch("rel_DpT_y_y_0_2t0_8_vs_rel_sumpT_y_y", &rel_DpT_y_y_0_2t0_8_vs_rel_sumpT_y_y);
        //t
        //tree->Branch("rel_DpT_y_y_vs_rel_sumpT_y_y_0_6t0_8", &rel_DpT_y_y_vs_rel_sumpT_y_y_0_6t0_8);
        //tree->Branch("rel_DpT_y_y_vs_rel_sumpT_y_y_0_8t1_1", &rel_DpT_y_y_vs_rel_sumpT_y_y_0_8t1_1);
        //tree->Branch("rel_DpT_y_y_vs_rel_sumpT_y_y_1_1t4", &rel_DpT_y_y_vs_rel_sumpT_y_y_1_1t4);


        wk()->addOutput(tree);

        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: fileExecute ()
{
        // Here you do everything that needs to be done exactly once for every
        // single file, e.g. collect a list of all lumi-blocks processed
        // m_fout = new TFile("sideband2x2d_input.root", "recreate");
        // m_fout->cd();



        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: changeInput (bool /*firstFile*/)
{
        // Here you do everything you need to do when we change input files,
        // e.g. resetting branch addresses on trees.  If you are using
        // D3PDReader or a similar service this method is not needed.
        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: initialize ()
{
        // Here you do everything that you need to do after the first input
        // file has been connected and before the first event is processed,
        // e.g. create additional histograms based on which variables are
        // available in the input files.  You can also create all of your
        // histograms and trees in here, but be aware that this method
        // doesn't get called if no events are processed.  So any objects
        // you create here won't be available in the output if you have no
        // input events.

        xAOD::TEvent* event = wk()->xaodEvent();
        m_eventCounter = 0;
        // GeV = 0.001;

        // as a check, let's see the number of events in our xAOD
        Warning("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

        TTree *MetaData = dynamic_cast<TTree *>(wk()->inputFile()->Get("MetaData"));

        TIter keyList(wk()->inputFile()->GetListOfKeys());
        TKey *key;
        while ((key = (TKey*)keyList())) {
                TH1 *h = (TH1*)key->ReadObj();
                TString histname(h->GetName());
                // std::cout << "hist name is " << histname << std::endl;
                if (histname.Contains("noDalitz_weighted")){
                        wk()->addOutput(h);
                }
                if (histname.Contains("CutFlow_Run")){
                        wk()->addOutput(h);
                }
        }

        m_config = new TEnv();
        std::string TestArea = std::string(getenv("TestArea"));
        Str confBaseDir = TestArea+"/HGamCore/HGamAnalysisFramework/data/";
        addCfgFile( confBaseDir+"MCSamples.config" );

        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: execute ()
{
        // Here you do everything that needs to be done on every single
        // events, e.g. read input variables, apply cuts, and fill
        // histograms and trees.  This is where most of your actual analysis
        // code will go.
        // Here you do everything that needs to be done on every single
        // events, e.g. read input variables, apply cuts, and fill
        // histograms and trees.  This is where most of your actual analysis
        // code will go.

        xAOD::TEvent* event = wk()->xaodEvent();
        m_eventCounter++;
        ResetVariables();

        m_eventInfoCont =0;
        EL_RETURN_CHECK("execute()",event->retrieve( m_eventInfoCont, "EventInfo"));


        const xAOD::EventInfo *HGamEventInfo = NULL;
        if (! event->retrieve(HGamEventInfo, "HGamEventInfo").isSuccess()) {
                Error("execute()", "Failed to retrieve HGamEventInfo. Exiting.");
                return EL::StatusCode::FAILURE;
        }

        const xAOD::EventInfo *EventInfo = NULL;
        if (! event->retrieve(EventInfo, "EventInfo").isSuccess()) {
                Error("execute()", "Failed to retrieve EventInfo. Exiting.");
                return EL::StatusCode::FAILURE;
        }

        const xAOD::MuonContainer *HGamMuons = NULL;
        if (! event->retrieve(HGamMuons, "HGamMuons").isSuccess()) {
                Error("execute()", "Failed to retrieve HGamMuons. Exiting.");
                return EL::StatusCode::FAILURE;
        }

        const xAOD::ElectronContainer *HGamElectrons = NULL;
        if (! event->retrieve(HGamElectrons, "HGamElectrons").isSuccess()) {
                Error("execute()", "Failed to retrieve HGamElectrons. Exiting.");
                return EL::StatusCode::FAILURE;
        }

        const xAOD::PhotonContainer *photons = NULL;
        if (! event->retrieve(photons, "HGamPhotons").isSuccess()) {
                Error("execute()", "Failed to retrieve HgamPhotons. Exiting.");
                return EL::StatusCode::FAILURE;
        }

        const xAOD::JetContainer *CustomJets = NULL;
        if (ish029){
                // ========== for h029 data ======
                if (! event->retrieve(CustomJets, "HGamAntiKt4EMPFlowCustomVtxJets").isSuccess()) {
                        Error("execute()", "Failed to retrieve HGamAntiKt4EMPFlowCustomVtxJets. Exiting.");
                        return EL::StatusCode::FAILURE;
                }
        }
        if (ish027){
                // ======== for h027 MC ======
                if (! event->retrieve(CustomJets, "HGamAntiKt4PFlowCustomVtxHggJets").isSuccess()) {
                        Error("execute()", "Failed to retrieve HGamAntiKt4PFlowCustomVtxHggJets. Exiting.");
                        return EL::StatusCode::FAILURE;
                }
        }
        if (ish024){
                if (! event->retrieve(CustomJets, "HGamAntiKt4EMTopoJets").isSuccess()) {
                        Error("execute()", "Failed to retrieve HGamAntiKt4EMTopoJets. Exiting.");
                        return EL::StatusCode::FAILURE;
                }
        }

        //pass_trigger_HLT_g35_loose_g25_loose = EventInfo->auxdataConst<char>("passTrig_HLT_g35_loose_g25_loose");
        //HLT_2g20_tight = EventInfo->auxdataConst<char>("passTrig_HLT_2g20_tight");
        //HLT_2g20_loose = EventInfo->auxdataConst<char>("passTrig_HLT_2g20_loose");
        //HLT_2g22_tight = EventInfo->auxdataConst<char>("passTrig_HLT_2g22_tight");
        //HLT_2g20_tight_icalotight_L12EM15VHI = EventInfo->auxdataConst<char>("passTrig_HLT_2g20_tight_icalotight_L12EM15VHI");
        //HLT_2g20_tight_icalovloose_L12EM15VHI = EventInfo->auxdataConst<char>("passTrig_HLT_2g20_tight_icalovloose_L12EM15VHI");
        //HLT_2g22_tight_L12EM15VHI = EventInfo->auxdataConst<char>("passTrig_HLT_2g22_tight_L12EM15VHI");


        pass_trigger_HLT_g35_medium_g25_medium_L12EM20VH = EventInfo->auxdataConst<char>("passTrig_HLT_g35_medium_g25_medium_L12EM20VH");
        isPassedTriggerMatch = HGamEventInfo->auxdataConst<char>("isPassedTriggerMatch");
        // passTrig_HLT_g35_loose_g25_loose = HGamEventInfo->auxdataConst<bool>("passTrig_HLT_g35_loose_g25_loose");
        // ** TODO BLS FIX ** passTrig_HLT_g35_medium_g25_medium_L12EM20VH = HGamEventInfo->auxdataConst<bool>("passTrig_HLT_g35_medium_g25_medium_L12EM20VH");

        runNumber = EventInfo->auxdataConst<unsigned int>("runNumber");


        isPassed = HGamEventInfo->auxdataConst<char>("isPassed")?1:5; // 5????
        isPassedPID = HGamEventInfo->auxdataConst<char>("isPassedPID");
        isPassedBasic = HGamEventInfo->auxdataConst<char>("isPassedBasic");
        isPassedJetEventClean = HGamEventInfo->auxdataConst<char>("isPassedJetEventClean");
        isPassedIsolation = HGamEventInfo->auxdataConst<char>("isPassedIsolation");
        isPassedRelPtCuts = HGamEventInfo->auxdataConst<char>("isPassedRelPtCuts");
        isPassedMassCut = HGamEventInfo->auxdataConst<char>("isPassedMassCut");

        cutFlow = HGamEventInfo->auxdataConst<int>("cutFlow");
        //std::cout<<"cutFlow = "<<cutFlow<<std::endl;
        if (cutFlow<11) return EL::StatusCode::SUCCESS;
        if (!(isPassedRelPtCuts)) return EL::StatusCode::SUCCESS;
        if (!(isPassedMassCut)) return EL::StatusCode::SUCCESS;


        // // catCoup_Moriond2017 = HGamEventInfo->auxdataConst<int>("catCoup_Moriond2017");
        // // weightCatCoup_Moriond2017 = HGamEventInfo->auxdataConst<float>("weightCatCoup_Moriond2017");
        // catCoup_Moriond2017BDT = HGamEventInfo->auxdataConst<int>("catCoup_Moriond2017BDT");
        // weightCatCoup_Moriond2017BDT = HGamEventInfo->auxdataConst<float>("weightCatCoup_Moriond2017BDT");
        // // catXS_VBF = HGamEventInfo->auxdataConst<char>("catXS_VBF");
        // catXS_ttH = HGamEventInfo->auxdataConst<char>("catXS_ttH");
        // N_lep_15 = HGamEventInfo->auxdataConst<int>("N_lep_15");
        // catXS_Nlep = N_lep_15>0?1:0;
        // // catXS_nbjet = HGamEventInfo->auxdataConst<char>("catXS_nbjet");
        // int nbjet = HGamEventInfo->auxdataConst<char>("catXS_nbjet");
        //
        // if (nbjet == 3 ){catXS_nbjet = 2;}
        // else {catXS_nbjet = nbjet;}
        //
        // catXS_MET80pTyy80 = (HGamEventInfo->auxdataConst<float>("pT_yy")/1000.>80.&&HGamEventInfo->auxdataConst<float>("met_TST")/1000.>80.)?1:0;
        // HiggsHF_weight = HGamEventInfo->auxdataConst<float>("HiggsHF_weight") * weightJvt_30;
        // weightJvt_30 = HGamEventInfo->auxdataConst<float>("weightJvt_30");
        // weightCatXS_nbjet = HGamEventInfo->auxdataConst<float>("weightCatXS_nbjet");
        // weightCatXS_ttH =  HGamEventInfo->auxdataConst<float>("weightCatXS_ttH") * weightJvt_30;
        //
        // // if (!isData){
        // //   if (HGamEventInfo->auxdataConst<float>("weightN_lep_15") == 0) {HiggsHF_weight = weightCatXS_ttH * weightJvt_30;}
        // //   else {HiggsHF_weight = weightCatXS_ttH * weightJvt_30 / HGamEventInfo->auxdataConst<float>("weightN_lep_15");}
        // // }
        //
        // // catHiggsHF = 0;
        //
        //   int HFTool_LeadDL1Jet_btagbin = HGamEventInfo->auxdataConst<int>("HiggsHF_leadBtagJet_btagbin");
        //
        //   if (HFTool_LeadDL1Jet_btagbin == 0){catXS_HiggsHF = 0;}
        //   else if (HFTool_LeadDL1Jet_btagbin == 1){catXS_HiggsHF = 1;}
        //   else if (HFTool_LeadDL1Jet_btagbin == 2){catXS_HiggsHF = 1; }
        //   else if (HFTool_LeadDL1Jet_btagbin == 3){catXS_HiggsHF = 2;}
        //   else if (HFTool_LeadDL1Jet_btagbin == 4){catXS_HiggsHF = 2;}
        //   else {catXS_HiggsHF=-999;}
        //
        //
        // // int N_j_btag30 = HGamEventInfo->auxdataConst<int>("N_j_btag30");
        //
        // // if (HGamEventInfo->auxdataConst<int>("N_lep") > 0 || HGamEventInfo->auxdataConst<int>("N_j_central30") == 0) {catHiggsHF = 0;} // Fail preselection
        // // else if (N_j_btag30 == 0) {catHiggsHF = 1;} //Pass preselection, but no b-tags
        // // else if (N_j_btag30 == 1) {catHiggsHF = 2;} //Pass preselection, have b-tag
        // // else {catHiggsHF = 3;} //Pass preselection, two or more b-tags
        //
        //
        mu = HGamEventInfo->auxdataConst<float>("mu");

        weight_lumiXsec = 1.0;
        if (!isData) weight_lumiXsec = getlumiXsecWeight( intLumi[ getMCType() ], EventInfo->auxdataConst<unsigned int>("mcChannelNumber"), true);
        //std::cout<<"crossSectionBRfilterEff = "<<HGamEventInfo->auxdataConst<float>("crossSectionBRfilterEff")<<" * "<<"35 ="<<HGamEventInfo->auxdataConst<float>("crossSectionBRfilterEff")*35<<" , weight_lumiXsec = "<<weight_lumiXsec<<std::endl;

        double extraWeight = 1.; //@@@
        // if ((!isData) && runNumber < 290000) extraWeight = 36.1/79.9; // 2015-2016, for MC16a
        // else if ((!isData)) extraWeight *= 43.8/79.9; // 2017, MC16d

        weight = HGamEventInfo->auxdataConst<float>("weight") * extraWeight;
        //weight_tot = (!isData) ? HGamEventInfo->auxdataConst<float>("weight") * weight_lumiXsec * extraWeight : HGamEventInfo->auxdataConst<float>("weight") * extraWeight;

        // weightSF = HGamEventInfo->auxdataConst<float>("weightSF");
        // lumiXsecWeight = !isData? weight_lumiXsec : -999;
        // pileupWeight = (!isData) ? HGamEventInfo->auxdataConst<float>("pileupWeight"): 1.0;
        // vertexWeight =  (!isData) ? HGamEventInfo->auxdataConst<float>("vertexWeight") : 1.0;
        // weightInitial = HGamEventInfo->auxdataConst<float>("weightInitial");
        // weightN_lep_15 = HGamEventInfo->auxdataConst<float>("weightN_lep_15");
        // weightCatXS_ttH = HGamEventInfo->auxdataConst<float>("weightCatXS_ttH");
        // met_weight = HGamEventInfo->auxdataConst<float>("met_weight");

        auto leadphoton = (*photons)[0];
        auto subleadphoton = (*photons)[1];

        y1_convType = leadphoton->auxdata<int>("conversionType");
        y1_m = leadphoton->m();
        y1_pt = leadphoton->pt();
        y1_eta = leadphoton->eta();
        y1_eta_s2 = leadphoton->auxdata<float>("eta_s2");
        y1_phi = leadphoton->phi();
        y1_topoetcone20 = leadphoton->auxdata<float>("topoetcone20");
        y1_ptcone20 = leadphoton->auxdata<float>("ptcone20");
        y1_isEMTight = leadphoton->auxdata<unsigned int>("isEMTight");
        y1_isEMTight_nofudge = (!isData) ? leadphoton->auxdata<unsigned int>("isEMTight_nofudge"):0;
        y1_isTight = leadphoton->auxdata<char>("isTight");
        y1_isTight_nofudge = (!isData) ? leadphoton->auxdata<char>("isTight_nofudge"):0;

        y2_convType = subleadphoton->auxdata<int>("conversionType");
        y2_m = subleadphoton->m();
        y2_pt = subleadphoton->pt();
        y2_eta = subleadphoton->eta();
        y2_eta_s2 = subleadphoton->auxdata<float>("eta_s2");
        y2_phi = subleadphoton->phi();
        y2_topoetcone20 = subleadphoton->auxdata<float>("topoetcone20");
        y2_ptcone20 = subleadphoton->auxdata<float>("ptcone20");
        y2_isEMTight = subleadphoton->auxdata<unsigned int>("isEMTight");
        y2_isEMTight_nofudge = (!isData) ? subleadphoton->auxdata<unsigned int>("isEMTight_nofudge"):0;
        y2_isTight = subleadphoton->auxdata<char>("isTight");
        y2_isTight_nofudge = (!isData) ? subleadphoton->auxdata<char>("isTight_nofudge"):0;

        y1_Rhad1 = leadphoton->auxdata<float>("Rhad1");
        y1_Rhad = leadphoton->auxdata<float>("Rhad");
        y1_Reta = leadphoton->auxdata<float>("Reta");
        y1_weta2 = leadphoton->auxdata<float>("weta2");
        y1_Rphi = leadphoton->auxdata<float>("Rphi");
        y1_Eratio = leadphoton->auxdata<float>("Eratio");
        y1_f1 = leadphoton->auxdata<float>("f1");

        y2_Rhad1 = subleadphoton->auxdata<float>("Rhad1");
        y2_Rhad = subleadphoton->auxdata<float>("Rhad");
        y2_Reta = subleadphoton->auxdata<float>("Reta");
        y2_weta2 = subleadphoton->auxdata<float>("weta2");
        y2_Rphi = subleadphoton->auxdata<float>("Rphi");
        y2_Eratio = subleadphoton->auxdata<float>("Eratio");
        y2_f1 = subleadphoton->auxdata<float>("f1");
        // // if (!ish024){
        // m_yyj_30 = HGamEventInfo->auxdataConst<float>("m_yyj_30");
        // pT_yyj_30 = HGamEventInfo->auxdataConst<float>("pT_yyj_30");
        // // }

        m_yy = HGamEventInfo->auxdataConst<float>("m_yy");
        // pT_y1 = HGamEventInfo->auxdataConst<float>("pT_y1");
        // pT_y2 = HGamEventInfo->auxdataConst<float>("pT_y2");
        // pT_yy = HGamEventInfo->auxdataConst<float>("pT_yy");
        // // if (pT_yy > 350e3) {pT_yy = 400e3;}
        //
        // rel_pT_y1 = pT_y1 / m_yy;
        // rel_pT_y2 = pT_y2 / m_yy;
        //
        //
        // yAbs_yy = HGamEventInfo->auxdataConst<float>("yAbs_yy");
        // pTt_yy = HGamEventInfo->auxdataConst<float>("pTt_yy");
        // cosTS_yy = HGamEventInfo->auxdataConst<float>("cosTS_yy");
        // Dy_y_y = HGamEventInfo->auxdataConst<float>("Dy_y_y");
        //
        // N_j = HGamEventInfo->auxdataConst<int>("N_j");
        // N_j_30 = HGamEventInfo->auxdataConst<int>("N_j_30");
        // N_j_50 = HGamEventInfo->auxdataConst<int>("N_j_50");
        // // N_j_central = HGamEventInfo->auxdataConst<int>("N_j_central");
        // N_j_central30 = HGamEventInfo->auxdataConst<int>("N_j_central30");
        // // N_j_central50 = eventHandler()->getVar< int >("N_j_central50");
        // // N_j_btag = HGamEventInfo->auxdataConst<int>("N_j_btag");
        // N_j_btag30 = HGamEventInfo->auxdataConst<int>("N_j_btag30");
        //
        // int n_jets_central = 0;
        // for (auto jet : *CustomJets) {
        //   if (fabs(jet->eta()) > 2.5 && jet->pt() * 0.001 < 25){continue;}
        //   ++n_jets_central;
        // }
        //
        // bool isOK=true;
        // for (auto muon : *HGamMuons) {
        //   if (muon->pt() * 0.001 > 10) isOK=false;
        // }
        // for (auto electron : *HGamElectrons) {
        //   if (electron->pt() * 0.001 > 10) isOK = false;
        // }
        //
        // if (n_jets_central > 3 || n_jets_central < 1) isOK = false;
        // if (isOK == false) {cat_ntag = 0;} // doesn't pass our cuts
        // else {
        //   cat_ntag = 1;
        //   for (auto jet: *CustomJets) {
        //     if (fabs(jet->eta()) > 2.5 && jet->pt() * 0.001 < 25){continue;}
        //     if (ish024){
        //       if (jet->auxdata<char>("MV2c10_FixedCutBEff_60")) {
        //         cat_ntag = 5;
        //       }
        //       else if (jet->auxdata<char>("MV2c10_FixedCutBEff_70") && cat_ntag <= 4)  {
        //         cat_ntag = 4;
        //       }
        //       else if (jet->auxdata<char>("MV2c10_FixedCutBEff_77") && cat_ntag <= 3)  {
        //         cat_ntag = 3;
        //       }
        //       else if (jet->auxdata<char>("MV2c10_FixedCutBEff_85") && cat_ntag <= 2)  {
        //         cat_ntag = 2;
        //       }
        //     }
        //     else if (!ish024){
        //       if (jet->auxdata<char>("DL1r_FixedCutBEff_60")) {
        //         cat_ntag = 5;
        //       }
        //       else if (jet->auxdata<char>("DL1r_FixedCutBEff_70") && cat_ntag <= 4)  {
        //         cat_ntag = 4;
        //       }
        //       else if (jet->auxdata<char>("DL1r_FixedCutBEff_77") && cat_ntag <= 3)  {
        //         cat_ntag = 3;
        //       }
        //       else if (jet->auxdata<char>("DL1r_FixedCutBEff_85") && cat_ntag <= 2)  {
        //         cat_ntag = 2;
        //       }
        //     }
        //   }
        // }
        // float m_jj = HGamEventInfo->auxdataConst<float>("m_jj");
        // float Dphi_yy_jj = HGamEventInfo->auxdataConst<float>("Dphi_yy_jj");
        // float Dy_j_j = HGamEventInfo->auxdataConst<float>("Dy_j_j");
        //
        // met_TST = HGamEventInfo->auxdataConst<float>("met_TST");
        // pT_j1_30 = HGamEventInfo->auxdataConst<float>("pT_j1_30");
        //
        // catXS_MET = met_TST > 80.0 * 1000 && pT_yy > 80.0 * 1000;
        // catXS_VBF = N_j_30 >= 2 && m_jj >= 600 * 1000 && Dy_j_j >= 3.5;
        // weightCatXS_VBF = weightJvt_30;
        // weightCatXS_lepton = HGamEventInfo->auxdataConst<float>("weightN_lep_15");
        // catXS_lepton = N_lep_15 > 0;
        // HiggsHF_weight = weightJvt_30;
        //
        // pT_yy_JV_30 = (pT_j1_30 == -99) ? pT_yy : -99; // pT_yy with a veto for pT_j1 < 30
        // pT_yy_JV_40 = (pT_j1_30 * 0.001 >= 40) ? -99 : pT_yy; // pT_yy with a veto for pT_j1 < 40
        // pT_yy_JV_50 = (pT_j1_30 * 0.001 >= 50) ? -99 : pT_yy; // pT_yy with a veto for pT_j1 < 50
        // pT_yy_JV_60 = (pT_j1_30 * 0.001 >= 60) ? -99 : pT_yy; // pT_yy with a veto for pT_j1 < 60
        //
        //
        // // kludge to get positive values
        // // if (pT_j1_30 < 0) pT_j1_30 = 0.01;
        //
        // // kludge to save things in last bin (ie overflow)
        // // if (pT_j1_30 > 350e3) pT_j1_30 = 400e3;
        //
        // pT_j2_30 = HGamEventInfo->auxdataConst<float>("pT_j2_30");
        // yAbs_j1_30 = HGamEventInfo->auxdataConst<float>("yAbs_j1_30");
        // yAbs_j2_30 = HGamEventInfo->auxdataConst<float>("yAbs_j2_30");
        // m_jj_30 = HGamEventInfo->auxdataConst<float>("m_jj_30");
        // Dy_j_j_30 = HGamEventInfo->auxdataConst<float>("Dy_j_j_30");
        // Dphi_j_j_30_signed = HGamEventInfo->auxdataConst<float>("Dphi_j_j_30_signed");
        // // Dy_yy_jj_30 = HGamEventInfo->auxdataConst<float>("Dy_yy_jj_30");
        // pT_yyjj_30 = HGamEventInfo->auxdataConst<float>("pT_yyjj_30");
        // Dphi_yy_jj_30 = HGamEventInfo->auxdataConst<float>("Dphi_yy_jj_30");
        // maxTau_yyj_30 = HGamEventInfo->auxdataConst<float>("maxTau_yyj_30");
        // sumTau_yyj_30 = HGamEventInfo->auxdataConst<float>("sumTau_yyj_30");
        // pT_j3_30 = HGamEventInfo->auxdataConst<float>("pT_j3_30");
        // HT_30 = HGamEventInfo->auxdataConst<float>("HT_30");
        //
        // rel_sumpT_y_y = rel_pT_y1 + rel_pT_y2;
        // rel_DpT_y_y = rel_pT_y1 - rel_pT_y2;
        //
        // if (catXS_VBF){
        //   VBF_pT_j1_30 = pT_j1_30;
        //   VBF_Dphi_j_j_30_signed = HGamEventInfo->auxdataConst<float>("Dphi_j_j_30_signed");
        //   VBF_pT_yyjj_30 = HGamEventInfo->auxdataConst<float>("pT_yyjj_30");
        //   if (HGamEventInfo->auxdataConst<float>("Zepp") <= -99) {VBF_abs_Zepp = -99;}
        //   else {VBF_abs_Zepp = fabs(HGamEventInfo->auxdataConst<float>("Zepp"));}
        // 
        //   //2D variables:
        //
        //   if (HGamEventInfo->auxdataConst<float>("Dphi_j_j_30") <= 1.04){VBF_pT_j1_30_vs_Dphi_j_j_30_0t1_04 = pT_j1_30;}
        //   else if (HGamEventInfo->auxdataConst<float>("Dphi_j_j_30") <= 2.08){VBF_pT_j1_30_vs_Dphi_j_j_30_1_04t2_08 = pT_j1_30;}
        //   else if (HGamEventInfo->auxdataConst<float>("Dphi_j_j_30") <= 3.15){VBF_pT_j1_30_vs_Dphi_j_j_30_2_08t3_15 = pT_j1_30;}
        //
        //   if (VBF_Dphi_j_j_30_signed >= -3.15 && VBF_Dphi_j_j_30_signed < 0){VBF_pT_j1_30_vs_Dphi_j_j_30_signed_3_15t0 = pT_j1_30;}
        //   else if (VBF_Dphi_j_j_30_signed > 0 && VBF_Dphi_j_j_30_signed <= 3.15){VBF_pT_j1_30_vs_Dphi_j_j_30_signed_0t3_15 = pT_j1_30;}
        //
        //   if (pT_j1_30 * 0.001 <= 120){VBF_pT_j1_30_30t120_vs_Dphi_j_j_30 = HGamEventInfo->auxdataConst<float>("Dphi_j_j_30");}
        //   else if (pT_j1_30 * 0.001 <= 500){VBF_pT_j1_30_120t500_vs_Dphi_j_j_30 = HGamEventInfo->auxdataConst<float>("Dphi_j_j_30");}
        // }//end of VBF checks
        // // else{//set all VBF variables to -99 if doesn't pass the category
        // //   VBF_pT_j1_30 = -99;
        // //   VBF_Dphi_j_j_30_signed = -99;
        // //   VBF_pT_yyjj_30 = -99;
        // //   VBF_pT_yyjj_30 = -99;
        // //   VBF_abs_Zepp = -99;
        // //   VBF_pT_j1_30_vs_Dphi_j_j_30_0t1_04 = -99;
        // //   VBF_pT_j1_30_vs_Dphi_j_j_30_1_04t2_08 = -99;
        // //   VBF_pT_j1_30_vs_Dphi_j_j_30_2_08t3_15 = -99;
        // //   VBF_pT_j1_30_30t120_vs_Dphi_j_j_30 = -99;
        // //   VBF_pT_j1_30_120t500_vs_Dphi_j_j_30 = -99;
        // // }//end nopass VBF cat
        //
        //   //2D variables:
        // if (pT_yy > 0){
        //   if (pT_yy * 0.001 <= 45){pT_yy_0t45_vs_yAbs_yy = yAbs_yy;}
        //   else if (pT_yy * 0.001 <= 120){pT_yy_45t120_vs_yAbs_yy = yAbs_yy;}
        //   else if (pT_yy * 0.001 <= 350){pT_yy_120t350_vs_yAbs_yy = yAbs_yy;}
        // }
        //
        //
        // if (yAbs_yy <= 0.5){pT_yy_vs_yAbs_yy_0t0_5 = pT_yy;}
        // else if (yAbs_yy <= 1.0){pT_yy_vs_yAbs_yy_0_5t1_0 = pT_yy;}
        // else if (yAbs_yy <= 1.5){pT_yy_vs_yAbs_yy_1_0t1_5 = pT_yy;}
        // else if (yAbs_yy <= 2.5){pT_yy_vs_yAbs_yy_1_5t2_5 = pT_yy;}
        //
        //
        // if (maxTau_yyj_30 * 0.001 < 0) {maxTau_yyj_30_100t0_vs_pT_yy = pT_yy;}
        // if (maxTau_yyj_30 * 0.001 > 0 && pT_yy * 0.001 > 0){
        //   if (maxTau_yyj_30 * 0.001 <= 15){maxTau_yyj_30_0t15_vs_pT_yy = pT_yy;}
        //   else if (maxTau_yyj_30 * 0.001 <= 25){maxTau_yyj_30_15t25_vs_pT_yy = pT_yy;}
        //   else if (maxTau_yyj_30 * 0.001 <= 40){maxTau_yyj_30_25t40_vs_pT_yy = pT_yy;}
        //   else if (maxTau_yyj_30 * 0.001 <= 400){maxTau_yyj_30_40t400_vs_pT_yy = pT_yy;}
        // }//end of maxTau_yyj_30_vs_pT_yy
        //
        //
        // // if (maxTau_yyj_30 > 0 && pT_yy * 0.001 > 0){
        // //   if (pT_yy * 0.001 <= 120){pT_yy_0t120_vs_maxTau_yyj_30 = maxTau_yyj_30;}
        //
        // //   if (pT_yy * 0.001 <= 45){pT_yy_0t45_vs_maxTau_yyj_30 = maxTau_yyj_30;}
        // //   if (pT_yy * 0.001 > 45 && pT_yy * 0.001 <= 120){pT_yy_45t120_vs_maxTau_yyj_30 = maxTau_yyj_30;}
        // //   if (pT_yy * 0.001 > 120 && pT_yy * 0.001 <= 350){pT_yy_120t350_vs_maxTau_yyj_30 = maxTau_yyj_30;}
        //
        // //   if (pT_yy * 0.001 <= 200){pT_yy_0t200_vs_maxTau_yyj_30 = maxTau_yyj_30;}
        // //   if (pT_yy * 0.001 > 200 && pT_yy * 0.001 <= 350){pT_yy_200t350_vs_maxTau_yyj_30 = maxTau_yyj_30;}
        // // }//end of pT_yy_vs_maxTau_yyj_30
        //
        //
        // if (pT_yyj_30 > 0 && pT_yy * 0.001 > 0){
        //   if (pT_yy * 0.001 <= 45){pT_yy_0t45_vs_pT_yyj_30 = pT_yyj_30;}
        //   else if (pT_yy * 0.001 <= 120){pT_yy_45t120_vs_pT_yyj_30 = pT_yyj_30;}
        //   else if (pT_yy * 0.001 <= 350){pT_yy_120t350_vs_pT_yyj_30 = pT_yyj_30;}
        // }//end of pT_yy_vs_pT_yyj_30
        //
        //
        // if (pT_yyj_30 < 0){pT_yyj_30_100t0_vs_pT_yy =  pT_yy;}
        // if (pT_yyj_30 > 0 && pT_yy * 0.001 > 0){
        //   if (pT_yyj_30 * 0.001 <= 30){pT_yyj_30_0t30_vs_pT_yy = pT_yy;}
        //   else if (pT_yyj_30 * 0.001 <= 60){pT_yyj_30_30t60_vs_pT_yy = pT_yy;}
        //   else if (pT_yyj_30 * 0.001 <= 350){pT_yyj_30_60t350_vs_pT_yy = pT_yy;}
        // }//end of pT_yyj_30_vs_pT_yy
        //
        //
        //
        // // if (rel_DpT_y_y > 0){
        // //   if (rel_DpT_y_y <= 0.3){rel_DpT_y_y_0t0_3_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        // //   if (rel_DpT_y_y > 0.3 && rel_DpT_y_y <= 0.6){rel_DpT_y_y_0_3t0_6_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        // //   if (rel_DpT_y_y > 0.6 && rel_DpT_y_y <= 4){rel_DpT_y_y_0_6t4_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        //
        // //   if (rel_DpT_y_y <= 0.05){rel_DpT_y_y_0t0_05_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        // //   if (rel_DpT_y_y > 0.05 && rel_DpT_y_y <= 0.1){rel_DpT_y_y_0_05t0_1_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        // //   if (rel_DpT_y_y > 0.1 && rel_DpT_y_y <= 0.2){rel_DpT_y_y_0_1t0_2_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        // //   if (rel_DpT_y_y > 0.2 && rel_DpT_y_y <= 0.8){rel_DpT_y_y_0_2t0_8_vs_rel_sumpT_y_y = rel_sumpT_y_y;}
        // // }//end of rel_sumpT_y_y_vs_rel_DpT_y_y
        //
        //
        // if (rel_sumpT_y_y >= 0.6 && rel_DpT_y_y > 0){
        //   if (rel_sumpT_y_y <= 0.8){rel_DpT_y_y_vs_rel_sumpT_y_y_0_6t0_8 = rel_DpT_y_y;}
        //   else if (rel_sumpT_y_y <= 1.1){rel_DpT_y_y_vs_rel_sumpT_y_y_0_8t1_1 = rel_DpT_y_y;}
        //   else if (rel_sumpT_y_y <= 4){rel_DpT_y_y_vs_rel_sumpT_y_y_1_1t4 = rel_DpT_y_y;}
        // }//end of rel_DpT_y_y_vs_rel_sumpT_y_y


        tree->Fill();

        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: postExecute ()
{
        // Here you do everything that needs to be done after the main event
        // processing.  This is typically very rare, particularly in user
        // code.  It is mainly used in implementing the NTupleSvc.
        return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGam2x2DInputMaker :: finalize ()
{
        // This method is the mirror image of initialize(), meaning it gets
        // called after the last event has been processed on the worker node
        // and allows you to finish up any objects you created in
        // initialize() before they are written to disk.  This is actually
        // fairly rare, since this happens separately for each worker node.
        // Most of the time you want to do your post-processing on the
        // submission node after all your histogram outputs have been
        // merged.  This is different from histFinalize() in that it only
        // gets called on worker nodes that processed input events.
        return EL::StatusCode::SUCCESS;
}


void HGam2x2DInputMaker::ResetVariables()
{
        //resetting variables that may not be filled every time
        VBF_pT_j1_30 = -99;
        VBF_Dphi_j_j_30_signed = -99;
        VBF_pT_yyjj_30 = -99;
        VBF_pT_yyjj_30 = -99;
        VBF_abs_Zepp = -99;

        VBF_pT_j1_30_vs_Dphi_j_j_30_0t1_04 = -99;
        VBF_pT_j1_30_vs_Dphi_j_j_30_1_04t2_08 = -99;
        VBF_pT_j1_30_vs_Dphi_j_j_30_2_08t3_15 = -99;
        VBF_pT_j1_30_30t120_vs_Dphi_j_j_30 = -99;
        VBF_pT_j1_30_120t500_vs_Dphi_j_j_30 = -99;

        VBF_pT_j1_30_vs_Dphi_j_j_30_signed_3_15t0 = -99;
        VBF_pT_j1_30_vs_Dphi_j_j_30_signed_0t3_15 = -99;

        pT_yy_0t45_vs_yAbs_yy = -99;
        pT_yy_45t120_vs_yAbs_yy = -99;
        pT_yy_120t350_vs_yAbs_yy = -99;

        pT_yy_vs_yAbs_yy_0t0_5 = -99;
        pT_yy_vs_yAbs_yy_0_5t1_0 = -99;
        pT_yy_vs_yAbs_yy_1_0t1_5 = -99;
        pT_yy_vs_yAbs_yy_1_5t2_5 = -99;

        maxTau_yyj_30_0t15_vs_pT_yy = -99;
        maxTau_yyj_30_15t25_vs_pT_yy = -99;
        maxTau_yyj_30_25t40_vs_pT_yy = -99;
        maxTau_yyj_30_40t400_vs_pT_yy = -99;

        pT_yyj_30_0t30_vs_pT_yy = -99;
        pT_yyj_30_30t60_vs_pT_yy = -99;
        pT_yyj_30_60t350_vs_pT_yy = -99;

        pT_yy_0t120_vs_maxTau_yyj_30 = -99;
        pT_yy_0t45_vs_maxTau_yyj_30 = -99;
        pT_yy_45t120_vs_maxTau_yyj_30 = -99;
        pT_yy_120t350_vs_maxTau_yyj_30 = -99;
        pT_yy_0t200_vs_maxTau_yyj_30 = -99;
        pT_yy_200t350_vs_maxTau_yyj_30 = -99;

        pT_yy_0t45_vs_pT_yyj_30 = -99;
        pT_yy_45t120_vs_pT_yyj_30 = -99;
        pT_yy_120t350_vs_pT_yyj_30 = -99;

        rel_DpT_y_y_0t0_3_vs_rel_sumpT_y_y = -99;
        rel_DpT_y_y_0_3t0_6_vs_rel_sumpT_y_y = -99;
        rel_DpT_y_y_0_6t4_vs_rel_sumpT_y_y = -99;
        rel_DpT_y_y_0t0_05_vs_rel_sumpT_y_y = -99;
        rel_DpT_y_y_0_05t0_1_vs_rel_sumpT_y_y = -99;
        rel_DpT_y_y_0_1t0_2_vs_rel_sumpT_y_y = -99;
        rel_DpT_y_y_0_2t0_8_vs_rel_sumpT_y_y = -99;

        rel_DpT_y_y_vs_rel_sumpT_y_y_0_6t0_8 = -99;
        rel_DpT_y_y_vs_rel_sumpT_y_y_0_8t1_1 = -99;
        rel_DpT_y_y_vs_rel_sumpT_y_y_1_1t4 = -99;



}

EL::StatusCode HGam2x2DInputMaker :: histFinalize ()
{
        // This method is the mirror image of histInitialize(), meaning it
        // gets called after the last event has been processed on the worker
        // node and allows you to finish up any objects you created in
        // histInitialize() before they are written to disk.  This is
        // actually fairly rare, since this happens separately for each
        // worker node.  Most of the time you want to do your
        // post-processing on the submission node after all your histogram
        // outputs have been merged.  This is different from finalize() in
        // that it gets called on all worker nodes regardless of whether
        // they processed input events.
        return EL::StatusCode::SUCCESS;
}
