#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

// #include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

#include "HGamBackgroundStudies/HGam2x2DInputMaker.h"
// #include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/ScanDir.h"

#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"

int main( int argc, char* argv[] ) {

        // // Set the input file directory:
        std::string inputFilePath = "";

        // // Set the input file Txt:
        std::string inputFileTxt = "";

        // // Set the input file name:
        std::string sampleName = "";

        // // Take the mc type (mc16a, mc16d, mc16e):
        std::string mc_type = "Data";

        //  // Set the output directory name:
        std::string submitDir = "submitDir";
        // if( argc > 4 ) submitDir = argv[ 4 ];

        std::string prod_version = "h027";

        for (int argi = 1; argi < argc; ++argi) {
                TString arg = argv[argi];

                if (arg.Contains("MonteCarloType")) {
                        mc_type = argv[++argi];
                }

                if (arg.Contains("InputFilePath")) {
                        inputFilePath = argv[++argi];
                        continue;
                }
                else if (arg.Contains("InputFileTxt")) {
                        inputFileTxt = argv[++argi];
                        continue;
                }
                if (arg.Contains("OutputDir")) {
                        submitDir = argv[++argi];
                        continue;
                }
                if (arg.Contains("Production")) {
                        prod_version = argv[++argi];
                        continue;
                }
                if (arg.Contains("SampleName")) {
                        sampleName = argv[++argi];
                }
        }

        std::cout << "Running with type: " << mc_type << std::endl;
        if (inputFilePath!="") {
                std::cout << "Running over file Path: " << inputFilePath << std::endl;
        }
        else if (inputFileTxt!="") {
                std::cout << "Running over file: " <<inputFileTxt<< std::endl;
        }
        std::cout << "Using Production: " << prod_version << std::endl;

        xAOD::Init().ignore();
        SH::SampleHandler sh;

        if (inputFilePath!="") {
                TString myinputFilePath = (TString)inputFilePath;
                if (myinputFilePath.Contains("Sherpa2_diphoton_myy_90_175_AF2") || myinputFilePath.Contains("data") || myinputFilePath.Contains("aMCPy8EG_aa_FxFx_2j_myy_90_175")) {
                        const char* inputFile = inputFilePath.c_str();
                        SH::DiskListLocal list (inputFile);
                        if (mc_type != "Data") SH::ScanDir().filePattern("*root*").scan (sh, inputFile);
                        else SH::ScanDir().filePattern("*.MxAODDetailed*.root*").scan (sh, inputFile);  // for h029 data
                        //else SH::ScanDir().filePattern("*.DS*.root*").scan (sh, inputFile);  // for h027 data
                        //else SH::ScanDir().filePattern("*.MxAOD.root*").scan (sh, inputFile);  // for h029 data_stage
                }
                else {
                        const char* inputFile = inputFilePath.c_str();
                        SH::DiskListLocal list (inputFile);
                        if (mc_type != "Data") SH::ScanDir().filePattern("*Sh_2210_yybb_90Myy175*root*").scan (sh, inputFile);
                }
        }
        else if (inputFileTxt!="") {
                SH::readFileList (sh, "sample", inputFileTxt);
        }

        sh.setMetaString( "nc_tree", "CollectionTree" );
        sh.print();

        EL::Job job;
        job.sampleHandler( sh );
        job.options()->setDouble (EL::Job::optMaxEvents, -1);
        // job.options()->setDouble (EL::Job::optSkipEvents, 1500);
        // job.options()->setDouble (EL::Job::optMaxEvents, 5);


        HGam2x2DInputMaker* alg1 = new HGam2x2DInputMaker();

        alg1->SetIsData(false);
        alg1->SetIsMCa(false);
        alg1->SetIsMCd(false);
        alg1->SetIsMCe(false);
        alg1->SetIsh024(false);
        alg1->SetIsh027(false);
        alg1->SetIsh029(false);

        if (prod_version == "h024"){alg1->SetIsh024(true);}
        if (prod_version == "h027"){alg1->SetIsh027(true);}
        if (prod_version == "h029"){alg1->SetIsh029(true);}

        if(mc_type == "mc21a" || mc_type == "MC21a") {alg1->SetIsMCa(true);}
        else if(mc_type == "mc16d" || mc_type == "MC16d") {alg1->SetIsMCd(true);}
        else if(mc_type == "mc16e" || mc_type == "MC16e") {alg1->SetIsMCe(true);}
        else if (mc_type == "Data"){alg1->SetIsData(true);}
        else{std::cerr << "FATAL: Please enter the MC type (i.e. MonteCarloType: mc21a) else sample is assumed to be data" << std::endl; _exit(0);}

        job.algsAdd( alg1 );

        EL::DirectDriver driver;
        driver.submit( job, submitDir );

        return 0;
}

