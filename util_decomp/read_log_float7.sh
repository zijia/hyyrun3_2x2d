echo "
double mc_e1I= $(cat log_float7_mc | grep E1I | cut -d '=' -f 2);
double mc_e2I= $(cat log_float7_mc | grep E2I | cut -d '=' -f 2);
double mc_e1T= $(cat log_float7_mc | grep E1T | cut -d '=' -f 2);
double mc_e2T= $(cat log_float7_mc | grep E2T | cut -d '=' -f 2);
double mc_ep1I= $(cat log_float7_mc | grep EP1I | cut -d '=' -f 2);
double mc_ep2I= $(cat log_float7_mc | grep EP2I | cut -d '=' -f 2);
double mc_ep1T= $(cat log_float7_mc | grep EP1T | cut -d '=' -f 2);
double mc_ep2T= $(cat log_float7_mc | grep EP2T | cut -d '=' -f 2);
double mc_f1I= 1; // unknown
double mc_f2I= 1; // unknown
double mc_f1T= 1; // unknown
double mc_f2T= 1; // unknown
double mc_fp1I= 1; // unknown
double mc_fp2I= 1; // unknown
double mc_fp1T= mc_f1T;
double mc_fp2T= mc_f2T;
double mc_x1g= $(cat log_float7_mc | grep X1G | cut -d '=' -f 2);
double mc_x2g= $(cat log_float7_mc | grep X2G | cut -d '=' -f 2);
double mc_x1j= 1; // fixed
double mc_x2j= 1; // fixed
double mc_xIgg= $(cat log_float7_mc | grep XIGG | cut -d '=' -f 2);
double mc_xTgg= $(cat log_float7_mc | grep XTGG | cut -d '=' -f 2);
double mc_xIgj= 1; // fixed
double mc_xTgj= 1; // fixed
double mc_xIjg= 1; // fixed
double mc_xTjg= 1; // fixed
double mc_xIjj= 1; // fixed
double mc_xTjj= 1;

double data_e1I= $(cat log_float7 | grep E1I | cut -d '=' -f 2);
double data_e2I= $(cat log_float7 | grep E2I | cut -d '=' -f 2);
double data_e1T= $(cat log_float7 | grep E1T | cut -d '=' -f 2);
double data_e2T= $(cat log_float7 | grep E2T | cut -d '=' -f 2);
double data_ep1I= $(cat log_float7 | grep EP1I | cut -d '=' -f 2);
double data_ep2I= $(cat log_float7 | grep EP2I | cut -d '=' -f 2);
double data_ep1T= $(cat log_float7 | grep EP1T | cut -d '=' -f 2);
double data_ep2T= $(cat log_float7 | grep EP2T | cut -d '=' -f 2);
double data_f1I= $(cat log_float7 | grep F1I | cut -d '=' -f 2);
double data_f2I= $(cat log_float7 | grep F2I | cut -d '=' -f 2);
double data_f1T= $(cat log_float7 | grep F1T | cut -d '=' -f 2);
double data_f2T= $(cat log_float7 | grep F2T | cut -d '=' -f 2);
double data_fp1I= $(cat log_float7 | grep FP1I | cut -d '=' -f 2);
double data_fp2I= $(cat log_float7 | grep FP2I | cut -d '=' -f 2);
double data_fp1T= data_f1T;
double data_fp2T= data_f2T;
double data_x1g= $(cat log_float7 | grep X1G | cut -d '=' -f 2);
double data_x2g= $(cat log_float7 | grep X2G | cut -d '=' -f 2);
double data_x1j= $(cat log_float7 | grep X1J | cut -d '=' -f 2);
double data_x2j= $(cat log_float7 | grep X2J | cut -d '=' -f 2);
double data_xIgg= $(cat log_float7 | grep XIGG | cut -d '=' -f 2);
double data_xTgg= $(cat log_float7 | grep XTGG | cut -d '=' -f 2);
double data_xIgj= $(cat log_float7 | grep XIGJ | cut -d '=' -f 2);
double data_xTgj= $(cat log_float7 | grep XTGJ | cut -d '=' -f 2);
double data_xIjg= $(cat log_float7 | grep XIJG | cut -d '=' -f 2);
double data_xTjg= $(cat log_float7 | grep XTJG | cut -d '=' -f 2);
double data_xIjj= $(cat log_float7 | grep XIJJ | cut -d '=' -f 2);
double data_xTjj= $(cat log_float7 | grep XTJJ | cut -d '=' -f 2);

vector<double> v_WTITI = {$(cat log_float7 | grep WGGTITI | cut -d '=' -f 2), $(cat log_float7 | grep WGJTITI | cut -d '=' -f 2), $(cat log_float7 | grep WJGTITI | cut -d '=' -f 2), $(cat log_float7 | grep WJJTITI | cut -d '=' -f 2)};
vector<double> v_NAA = {$(cat log_float7 | grep NAA | cut -d '=' -f 2), $(cat log_float7 | grep NAB | cut -d '=' -f 2), $(cat log_float7 | grep NAC | cut -d '=' -f 2), $(cat log_float7 | grep NAD | cut -d '=' -f 2), $(cat log_float7 | grep NBA | cut -d '=' -f 2), $(cat log_float7 | grep NBB | cut -d '=' -f 2), $(cat log_float7 | grep NBC | cut -d '=' -f 2), $(cat log_float7 | grep NBD | cut -d '=' -f 2), $(cat log_float7 | grep NCA | cut -d '=' -f 2), $(cat log_float7 | grep NCB | cut -d '=' -f 2), $(cat log_float7 | grep NCC | cut -d '=' -f 2), $(cat log_float7 | grep NCD | cut -d '=' -f 2), $(cat log_float7 | grep NDA | cut -d '=' -f 2), $(cat log_float7 | grep NDB | cut -d '=' -f 2), $(cat log_float7 | grep NDC | cut -d '=' -f 2), $(cat log_float7 | grep NDD | cut -d '=' -f 2)};
"
