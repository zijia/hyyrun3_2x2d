#!/bin/bash
if [[ $# -ne 4 ]]; then
    echo "USAGE: LCie.sh <jobname> <executable> <systematics> <inputconfig>  <luminosity>"
    exit
fi
version=$1
shift
jobname=$1
shift
samplename=$1
shift
njob=$1

jobnameall=${version}/${jobname}/${samplename}

inputname="list/${jobname}/${samplename}.list"
#inputname="list/h027/${samplename}.list"
#inputname="list/${samplename}.list"
#option=$3
#njob=$4
nfile=`wc -l < "$inputname"`

echo "samplename" $samplename

echo "inputname" $inputname

echo "nfile" $nfile

nfile_perjob=`expr $nfile / $njob`
nfile_lastjob=`expr $nfile % $njob`

echo "nfileperjob" $nfile_perjob 

if [ "$nfile_lastjob" -gt 0 ]; then   ### be divided with remainder
nfile_perjob=`expr $nfile_perjob + 1`
fi
#########################################

#base_dir="/nfs/dust/atlas/user/hans/datarun/condor/SM_H_Fidu/${jobnameall}/"
#output_dir="/nfs/dust/atlas/user/hans/datarun/resultfs/SM_H_Fidu/${jobnameall}"
#source_dir="/nfs/dust/atlas/user/hans/HZG_h023_MxAOD2Ntuple/source"

#base_dir="/afs/cern.ch/user/f/falves/work/MyProject/run/condor_submit_CR/datarun/condor/SM_H_Fidu/${jobnameall}/"
#output_dir="/afs/cern.ch/user/f/falves/work/MyProject/run/condor_submit_CR/datarun/results/SM_H_Fidu/${jobnameall}/"
#source_dir="/afs/cern.ch/user/f/falves/work/MyProject/source"

base_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/datarun/condor/${jobnameall}/"
output_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/datarun/results/${jobnameall}/"
source_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/source"

echo "output directory---------------------------------" ${output_dir}

mkdir -p ${base_dir}/job
mkdir -p ${base_dir}/err
mkdir -p ${base_dir}/log
mkdir -p ${base_dir}/out
mkdir -p ${base_dir}/exe
mkdir -p ${base_dir}/input

mkdir -p ${output_dir}/log
mkdir -p ${output_dir}/err
mkdir -p ${output_dir}/out
mkdir -p ${output_dir}/root

#cp -fr ../../sourcedir.tar ${base_dir}/${jobname}/exe/
cp -fr LCie.sh  ${base_dir}/exe/
#cp -fr setup_rootcore.sh  ${base_dir}/${jobname}/exe/
#########################################
ifile=0
ijob=0
while read line
do
ifile=`expr $ifile + 1`
tempfile=`expr $ifile % $nfile_perjob`   ## which job

echo "tempfile" $tempfile

if [ "$nfile_perjob" -eq 1 ]; then
ijob=`expr $ijob + 1`
rm -fr "$jobname"_"$ijob".txt
echo "$line" > "$jobname"_"$ijob".txt
else
if [ "$tempfile" -eq 1 ]; then
ijob=`expr $ijob + 1`
rm -fr "$jobname"_"$ijob".txt
echo "$line" > "$jobname"_"$ijob".txt
else
echo "$line" >> "$jobname"_"$ijob".txt
fi
fi

###############################################
if [ "$tempfile" -eq 0 ]; then
mv  "$jobname"_"$ijob".txt  ${base_dir}/input/
################# script of qsub job  ################
submitfilename="${base_dir}/job/${jobname}_"$ijob".sub"
jobfilename="${base_dir}/job/${jobname}_"$ijob".job"
rm -fr  ${jobfilename}
echo "#!/bin/bash"   > ${jobfilename}
echo " sh ${base_dir}/exe/LCie.sh  ${version} ${jobname}  ${samplename}  ${ijob}  > ${base_dir}/log/${jobname}_${ijob}.log 2>${base_dir}/err/${jobname}_${ijob}.err  "  >>  ${jobfilename}
chmod a+x ${jobfilename}

submitfilename="${base_dir}/job/${jobname}_"$ijob".sub"
rm -fr  ${submitfilename}
echo "executable     = ${jobfilename}" > ${submitfilename}
echo "should_transfer_files   = Yes"  >> ${submitfilename}
echo "when_to_transfer_output = ON_EXIT"   >> ${submitfilename}
echo "input          = ${base_dir}/input/${jobname}_"$ijob".txt"   >> ${submitfilename}
echo "output         = ${base_dir}/out/${jobname}_"$ijob".out2" >> ${submitfilename}
echo "error          = ${base_dir}/err/${jobname}_"$ijob".err2" >> ${submitfilename}
echo "log            = ${base_dir}/log/${jobname}_"$ijob".log2" >> ${submitfilename}
#echo "+RequestRuntime = 259200"  >> ${submitfilename}
#echo "+MaxRuntime = 18000" >> ${submitfilename} #MC files
echo "+MaxRuntime = 108000" >> ${submitfilename}  #Data files
#echo "transfer_input_files  = ${source_dir}/../run/condor_submit_CR/x509up_u25913" >> ${submitfilename}
echo "transfer_input_files  = /afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/x509up_u114134" >> ${submitfilename}
echo "universe       = vanilla"  >> ${submitfilename}
#echo "RequestMemory   = 4096" >> ${submitfilename}
#echo "RequestDisk = 4096000" >> ${submitfilename}
echo "queue" >> ${submitfilename}

condor_submit ${submitfilename}  
echo "hello : condor submitted"
#source ${jobfilename}

###############
else          #### with remainder
   if [ "$ifile" -eq "$nfile" ]; then
   mv  "$jobname"_"$ijob".txt  ${base_dir}/input/
##################### script of qsub job  ################
jobfilename="${base_dir}/job/${jobname}_"$ijob".job"
rm -fr  ${jobfilename}
echo "#!/bin/bash"   > ${jobfilename}
echo " sh ${base_dir}/exe/LCie.sh  ${version} ${jobname} ${samplename}  ${ijob}  > ${base_dir}/log/${jobname}_${ijob}.log 2>${base_dir}/err/${jobname}_${ijob}.err  "  >>  ${jobfilename}
chmod a+x ${jobfilename}


submitfilename="${base_dir}/job/${jobname}_"$ijob".sub"
rm -fr  ${submitfilename}
echo "executable     = ${jobfilename}" > ${submitfilename}
echo "should_transfer_files   = Yes"  >> ${submitfilename}
echo "when_to_transfer_output = ON_EXIT"   >> ${submitfilename}
echo "input          = ${base_dir}/input/${jobname}_"$ijob".txt"   >> ${submitfilename}
echo "output         = ${base_dir}/out/${jobname}_"$ijob".out2" >> ${submitfilename}
echo "error          = ${base_dir}/err/${jobname}_"$ijob".err2" >> ${submitfilename}
echo "log            = ${base_dir}/log/${jobname}_"$ijob".log2" >> ${submitfilename}
#echo "+RequestRuntime = 259200"  >> ${submitfilename}
echo "+MaxRuntime = 18000" >> ${submitfilename} #MC Files
#echo "+MaxRuntime = 108000" >> ${submitfilename}  #Data files
#comentei aqui -> echo "transfer_input_files  = ${source_dir}/../run/condor_submit_CR/x509up_u25913" >> ${submitfilename}
echo "transfer_input_files  = /afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/x509up_u114134" >> ${submitfilename}
echo "universe       = vanilla"  >> ${submitfilename}
#echo "RequestMemory   = 4096" >> ${submitfilename}
#echo "RequestDisk = 4096000" >> ${submitfilename}
echo "queue" >> ${submitfilename}

condor_submit ${submitfilename}  

#source ${jobfilename}

#####################
   fi

fi


done <${inputname}
############################################
#iline=1
#while [ "$iline" -le "$nline" ]
#do
#
#
#
#
#
#workspacename=`sed -n "$iline","$iline"p  "$inputname" | awk '{print $1}'`
#sh submit_limit.sh  makespace_"$workspacename"  makespace_"$workspacename" 
#
#let "iline+=1"
#done
