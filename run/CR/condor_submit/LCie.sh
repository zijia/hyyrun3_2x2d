#!/bin/bash

if [[ $# -ne 4 ]]; then
    echo "USAGE: LCie.sh <jobname> <executable> <systematics>   <luminosity>"
    
else
version=$1
shift
jobname=$1
shift
samplename=$1
shift
ijob=$1
shift

jobnameall=${version}/${jobname}/${samplename}
#base_dir="/nfs/dust/atlas/user/hans/datarun/condor/SM_H_Fidu/${jobnameall}/"
#output_dir="/nfs/dust/atlas/user/hans/datarun/resultfs/SM_H_Fidu/${jobnameall}"
#run_dir="/nfs/dust/atlas/user/hans/datarun/run_temp/SM_H_Fidu/${jobnameall}"
#source_dir="/nfs/dust/atlas/user/hans/HZG_h023_MxAOD2Ntuple/source"
#echo "base_dir: " ${base_dir}


mcTypea="mc21a"
mcTyped="mc16d"
mcTypee="mc16e"
if [[ ${samplename} == ${mcTypea}* ]]; then
    mcType="MC21a"
elif [[ ${samplename} == ${mcTyped}* ]]; then
    mcType="MC16d"
elif [[ ${samplename} == ${mcTypee}* ]]; then
    mcType="MC16e"
fi

echo ${mcType} 



#change for your path
base_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/datarun/condor/${jobnameall}/"
output_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/datarun/results/${jobnameall}/"
run_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/datarun/run_tmp/${jobnameall}/"
source_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/source"
store_dir="/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/OUTPUT_yybb/"
echo "base_dir: " ${base_dir}
echo "output_dir: " ${output_dir}
echo "store_dir: " ${store_dir}


#    mkdir -vp ${output_dir}/fig
#    mkdir -vp ${output_dir}/log
#    mkdir -vp ${output_dir}/err
#    mkdir -vp ${output_dir}/input

    if [ ! -e ${output_dir} ];then
    echo "Can not make working directory ${output_dir}. Exit"
    exit
    fi

    work_dir=${run_dir}/"${jobname}_${ijob}"

    rm -fr ${work_dir}
    mkdir -vp ${work_dir}
	
    rm -fr ${store_dir}/${samplename}
    mkdir -vp ${store_dir}/${samplename}

    echo "hello : making work_dir"
    if [ ! -e ${work_dir} ];then
	echo "Can not make working directory ${work_dir}. Exit"
	exit
    fi

    echo "LCie file3"

    cd ${source_dir}
    echo "hello : source environment"
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
    #lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
    
    echo "hello : proxy"
    export X509_USER_PROXY=/afs/cern.ch/work/z/zijia/private/HGam/hgam-Run3-2x2D/run/CR/condor_submit/x509up_u114134 #change for your proxy
    echo "$X509_USER_PROXY"
    #rm -f voms.sh
    echo "#!/usr/bin/expect -f" >> ${work_dir}/voms.sh
    echo 'set password "1234"' >> ${work_dir}/voms.sh
    echo "spawn voms-proxy-init -voms atlas" >> ${work_dir}/voms.sh
    echo 'expect "Enter GRID pass phrase for this identity:"'  >> ${work_dir}/voms.sh
    echo 'send "$password\r"' >> ${work_dir}/voms.sh
    echo "interact" >> ${work_dir}/voms.sh
    expect ${work_dir}/voms.sh

    echo "hello : lsetup xroot rucio"
    lsetup xrootd rucio
    
    asetup AnalysisBase,21.2.222,here
    #asetup AnalysisBase,21.2.131,here
    #asetup AnalysisBase,21.2.56,here
    cd -
    cd ${source_dir}/../build/
    source x86_64-centos7-gcc8-opt/setup.sh
    cd -

    echo "hello : AnalysisBase set"
    pushd ${work_dir}

    cp ${base_dir}/input/"$jobname"_"$ijob".txt  ./toberun.txt    
 
filename=$(cat toberun.txt)
echo $filename

isMC="mc21"
isDT="data"

if [[ ${samplename} == ${isDT}* ]]; then
  rm -r /eos/home-z/zijia/HGamRun3/2x2D/${samplename}
elif [[ ${samplename} == ${isMC}* ]]; then
  rm -r /eos/home-z/zijia/HGamRun3/2x2D/${samplename}_${ijob}
#mkdir /eos/home-z/zijia/HGamRun3/2x2D
fi

if [[ ${samplename} == ${isMC}* ]]; then
#====== MC =======
	runHGam2x2DInputMaker BaseConfig: $TestArea/HGamCore/HGamAnalysisFramework/data/HGamRel21.config $TestArea/HGamCore/HGamAnalysisFramework/data/MCSamples.config OutputDir: /eos/home-z/zijia/HGamRun3/2x2D/${samplename}_${ijob} InputFileTxt: ${base_dir}/input/"$jobname"_"$ijob".txt MonteCarloType: ${mcType} Production: ${jobname}
	#runHGam2x2DInputMaker BaseConfig: $TestArea/HGamCore/HGamAnalysisFramework/data/HGamRel21.config $TestArea/HGamCore/HGamAnalysisFramework/data/MCSamples.config OutputDir: /eos/home-z/zijia/HGamRun3/2x2D/${samplename} InputFilePath: ${filename} MonteCarloType: ${mcType} Production: ${jobname}
	echo "ZIHANG MC" 
elif [[ ${samplename} == ${isDT}* ]]; then
#====== Data =======
	runHGam2x2DInputMaker BaseConfig: $TestArea/HGamCore/HGamAnalysisFramework/data/HGamRel21.config OutputDir: /eos/home-z/zijia/HGamRun3/2x2D/${samplename} InputFilePath: ${filename} MonteCarloType: Data Production: ${jobname}
  echo "runHGam2x2DInputMaker BaseConfig: $TestArea/HGamCore/HGamAnalysisFramework/data/HGamRel21.config OutputDir: /eos/home-z/zijia/HGamRun3/2x2D/${samplename} InputFilePath: ${filename} MonteCarloType: Data Production: ${jobname}"
	echo "ZIHANG DATA"
fi

   #change this line
   #cp /eos/home-z/zijia/2x2D_h027/${samplename}/data-MxAOD/sideband2x2d_input.root ${store_dir}/${samplename}/sideband2x2d_${ijob}.root
 

#########################################################################
    popd

#    rm -r ${work_dir}

    echo "----------All done------------"
    date
#    sleep 1200			# it is always bad to be too fast for a man.

fi
