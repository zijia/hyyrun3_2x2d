MC="a"
DT="22"

for dt in $DT;do
ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/data${dt}/runs/*root > data${dt}.list
done

for mC in $MC;do

# ggH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_ggH_MINLO_gammagamma*/*root > mc21${mC}_PhPy8EG_PDF4LHC21_ggH_MINLO_gammagamma.list
#hadd -T mc21${mC}_PhPy8EG_PDF4LHC21_ggH_MINLO_gammagamma.root /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_ggH_MINLO_gammagamma*/*root
# VBFH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_VBFH125_gammagamma*/*root > mc21${mC}_PhPy8EG_PDF4LHC21_VBFH125_gammagamma.list
#hadd -T mc21${mC}_PhPy8EG_PDF4LHC21_VBFH125_gammagamma.root /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_VBFH125_gammagamma*/*root
# WmH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_WmH125J_Wincl_MINLO*root > mc21${mC}_PhPy8EG_PDF4LHC21_WmH125J_Wincl_MINLO.list
# WpH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_WpH125J_Wincl_MINLO*root > mc21${mC}_PhPy8EG_PDF4LHC21_WpH125J_Wincl_MINLO.list
# ZH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_ZH125J_Zincl_MINLO*root > mc21${mC}_PhPy8EG_PDF4LHC21_ZH125J_Zincl_MINLO.list
# ggZH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_ggZH125_Zincl*root > mc21${mC}_PhPy8EG_PDF4LHC21_ggZH125_Zincl.list
# ttH
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.PhPy8EG_PDF4LHC21_ttH125_tincl*root > mc21${mC}_PhPy8EG_PDF4LHC21_ttH125_tincl.list
# yy
#ls /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.aMCPy8EG_aa_FxFx_2j_myy_90_175.MxAODDetailed*/*root > mc21${mC}_aMCPy8EG_aa_FxFx_2j_myy_90_175.list
hadd -T mc21${mC}_aMCPy8EG_aa_FxFx_2j_myy_90_175.root /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21${mC}/Nominal/mc21${mC}.aMCPy8EG_aa_FxFx_2j_myy_90_175.MxAODDetailed*/*root
echo "skip"
done
