//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Mar  1 09:06:39 2023 by ROOT version 6.24/08
// from TTree CollectionTree/xAOD event tree
// found on file: /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21a/Nominal/mc21a.PhPy8EG_PDF4LHC21_WpH125J_Wincl_MINLO.MxAODDetailedNoSkim.e8472_s3873_r13829_p5441_h029.root
//////////////////////////////////////////////////////////

#ifndef h029_MxAOD_h
#define h029_MxAOD_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class h029_MxAOD {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxHGamPhotonsAux = 1;
   static constexpr Int_t kMaxHGamPhotonsAuxDyn_truthLink = 3;
   static constexpr Int_t kMaxHGamElectronsAux = 1;
   static constexpr Int_t kMaxBTagging_HGamAntiKt4EMPFlowCustomVtxAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4EMPFlowCustomVtxJetsAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink = 26;
   static constexpr Int_t kMaxBTagging_HGamAntiKt4EMPFlowAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4EMPFlowJetsAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink = 26;
   static constexpr Int_t kMaxHGamMuonsAux = 1;
   static constexpr Int_t kMaxHGamTauJetsAux = 1;
   static constexpr Int_t kMaxHGamMET_Reference_AntiKt4EMPFlowAux = 1;
   static constexpr Int_t kMaxHGamEventInfoAux = 1;
   static constexpr Int_t kMaxHGamTruthPhotonsAux = 1;
   static constexpr Int_t kMaxHGamTruthPhotonsAuxDyn_recoLink = 7;
   static constexpr Int_t kMaxHGamTruthElectronsAux = 1;
   static constexpr Int_t kMaxHGamTruthMuonsAux = 1;
   static constexpr Int_t kMaxHGamTruthTausAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4TruthWZJetsAux = 1;
   static constexpr Int_t kMaxHGamMET_TruthAux = 1;
   static constexpr Int_t kMaxHGamTruthHiggsBosonsAux = 1;
   static constexpr Int_t kMaxTruthEventsAux = 1;
   static constexpr Int_t kMaxHGamTruthEventInfoAux = 1;
   static constexpr Int_t kMaxEventInfoAux = 1;

   // Declaration of leaf types
   DataVector<xAOD::Photon_v1> *HGamPhotons;
   xAOD::AuxContainerBase *HGamPhotonsAux_;
   vector<float>   *HGamPhotonsAuxDyn_DeltaE;
   vector<float>   *HGamPhotonsAuxDyn_Eratio;
   vector<float>   *HGamPhotonsAuxDyn_Rconv;
   vector<float>   *HGamPhotonsAuxDyn_Reta;
   vector<float>   *HGamPhotonsAuxDyn_Rhad;
   vector<float>   *HGamPhotonsAuxDyn_Rhad1;
   vector<float>   *HGamPhotonsAuxDyn_Rphi;
   vector<unsigned short> *HGamPhotonsAuxDyn_author;
   vector<float>   *HGamPhotonsAuxDyn_cl_E;
   vector<float>   *HGamPhotonsAuxDyn_cl_E_TileGap3;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es0;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es1;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es2;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es3;
   vector<float>   *HGamPhotonsAuxDyn_cl_eta;
   vector<float>   *HGamPhotonsAuxDyn_cl_etaCalo;
   vector<float>   *HGamPhotonsAuxDyn_cl_phiCalo;
   vector<float>   *HGamPhotonsAuxDyn_cl_ratioEs1Es2;
   vector<int>     *HGamPhotonsAuxDyn_conversionType;
   vector<float>   *HGamPhotonsAuxDyn_convtrk1nPixHits;
   vector<float>   *HGamPhotonsAuxDyn_convtrk1nSCTHits;
   vector<float>   *HGamPhotonsAuxDyn_convtrk2nPixHits;
   vector<float>   *HGamPhotonsAuxDyn_convtrk2nSCTHits;
   vector<float>   *HGamPhotonsAuxDyn_e277;
   vector<float>   *HGamPhotonsAuxDyn_eta;
   vector<float>   *HGamPhotonsAuxDyn_eta_s1;
   vector<float>   *HGamPhotonsAuxDyn_eta_s2;
   vector<float>   *HGamPhotonsAuxDyn_f1;
   vector<float>   *HGamPhotonsAuxDyn_fracs1;
   vector<unsigned int> *HGamPhotonsAuxDyn_isEMTight;
   vector<char>    *HGamPhotonsAuxDyn_isIsoFixedCutLoose;
   vector<char>    *HGamPhotonsAuxDyn_isIsoFixedCutTight;
   vector<char>    *HGamPhotonsAuxDyn_isIsoTightCaloOnly;
   vector<char>    *HGamPhotonsAuxDyn_isTight;
   vector<char>    *HGamPhotonsAuxDyn_isTight_nofudge;
   vector<float>   *HGamPhotonsAuxDyn_m;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_energy;
   vector<int>     *HGamPhotonsAuxDyn_maxEcell_gain;
   vector<unsigned long> *HGamPhotonsAuxDyn_maxEcell_onlId;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_time;
   vector<int>     *HGamPhotonsAuxDyn_parentPdgId;
   vector<int>     *HGamPhotonsAuxDyn_pdgId;
   vector<float>   *HGamPhotonsAuxDyn_phi;
   vector<float>   *HGamPhotonsAuxDyn_pt;
   vector<float>   *HGamPhotonsAuxDyn_pt1conv;
   vector<float>   *HGamPhotonsAuxDyn_pt2conv;
   vector<float>   *HGamPhotonsAuxDyn_pt_s2;
   vector<float>   *HGamPhotonsAuxDyn_ptcone20;
   vector<float>   *HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000;
   vector<float>   *HGamPhotonsAuxDyn_ptconv;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es0;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es1;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es2;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es3;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_ratioEs1Es2;
   vector<float>   *HGamPhotonsAuxDyn_relEreso;
   vector<float>   *HGamPhotonsAuxDyn_scaleFactor;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone20;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone20_DDcorr;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone40;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone40_DDcorr;
   Int_t           HGamPhotonsAuxDyn_truthLink_;
   UInt_t          HGamPhotonsAuxDyn_truthLink_m_persKey[kMaxHGamPhotonsAuxDyn_truthLink];   //[HGamPhotonsAuxDyn.truthLink_]
   UInt_t          HGamPhotonsAuxDyn_truthLink_m_persIndex[kMaxHGamPhotonsAuxDyn_truthLink];   //[HGamPhotonsAuxDyn.truthLink_]
   vector<int>     *HGamPhotonsAuxDyn_truthOrigin;
   vector<float>   *HGamPhotonsAuxDyn_truthRconv;
   vector<int>     *HGamPhotonsAuxDyn_truthType;
   vector<float>   *HGamPhotonsAuxDyn_weta1;
   vector<float>   *HGamPhotonsAuxDyn_weta2;
   vector<float>   *HGamPhotonsAuxDyn_wtots1;
   vector<float>   *HGamPhotonsAuxDyn_zconv;
   vector<float>   *HGamPhotonsAuxDyn_zvertex;
   DataVector<xAOD::Electron_v1> *HGamElectrons;
   xAOD::AuxContainerBase *HGamElectronsAux_;
   DataVector<xAOD::BTagging_v1> *BTagging_HGamAntiKt4EMPFlowCustomVtx;
   xAOD::AuxContainerBase *BTagging_HGamAntiKt4EMPFlowCustomVtxAux_;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pb;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pc;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pu;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pb;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pc;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pu;
   DataVector<xAOD::Jet_v1> *HGamAntiKt4EMPFlowCustomVtxJets;
   xAOD::AuxContainerBase *HGamAntiKt4EMPFlowCustomVtxJetsAux_;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_CorrJvf;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DFCommonJets_fJvt;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_bin;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pb;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pc;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pu;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DetectorEta;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_bin;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pb;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pc;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pu;
   vector<int>     *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_HadronConeExclTruthLabelID;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Jvt;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_NNRegPtSF;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_eta;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_m;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_phi;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_pt;
   vector<int>     *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PartonTruthLabelID;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PtRecoSF;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Rpt;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_fjvt;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_jvt;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Timing;
   Int_t           HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_;
   UInt_t          HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persKey[kMaxHGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink];   //[HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.btaggingLink_]
   UInt_t          HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persIndex[kMaxHGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink];   //[HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.btaggingLink_]
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_eta;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtHS;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtPU;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_m;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_overlapTau;
   vector<char>    *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_passFJVT;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_phi;
   vector<float>   *HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_pt;
   DataVector<xAOD::BTagging_v1> *BTagging_HGamAntiKt4EMPFlow;
   xAOD::AuxContainerBase *BTagging_HGamAntiKt4EMPFlowAux_;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pb;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pc;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pu;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pb;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pc;
   vector<float>   *BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pu;
   DataVector<xAOD::Jet_v1> *HGamAntiKt4EMPFlowJets;
   xAOD::AuxContainerBase *HGamAntiKt4EMPFlowJetsAux_;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_DFCommonJets_fJvt;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_bin;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pb;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pc;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pu;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_DetectorEta;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_bin;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pb;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pc;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pu;
   vector<int>     *HGamAntiKt4EMPFlowJetsAuxDyn_HadronConeExclTruthLabelID;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_Jvt;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_SF_jvt;
   Int_t           HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_;
   UInt_t          HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persKey[kMaxHGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink];   //[HGamAntiKt4EMPFlowJetsAuxDyn.btaggingLink_]
   UInt_t          HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persIndex[kMaxHGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink];   //[HGamAntiKt4EMPFlowJetsAuxDyn.btaggingLink_]
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_eta;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_isJvtHS;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_isJvtPU;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_m;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_overlapTau;
   vector<char>    *HGamAntiKt4EMPFlowJetsAuxDyn_passFJVT;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_phi;
   vector<float>   *HGamAntiKt4EMPFlowJetsAuxDyn_pt;
   DataVector<xAOD::Muon_v1> *HGamMuons;
   xAOD::AuxContainerBase *HGamMuonsAux_;
   DataVector<xAOD::TauJet_v3> *HGamTauJets;
   xAOD::AuxContainerBase *HGamTauJetsAux_;
   xAOD::MissingETContainer_v1 *HGamMET_Reference_AntiKt4EMPFlow;
   xAOD::AuxContainerBase *HGamMET_Reference_AntiKt4EMPFlowAux_;
   vector<float>   *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx;
   vector<float>   *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy;
   vector<string>  *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name;
   vector<ULong64_t> *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source;
   vector<float>   *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet;
   xAOD::EventInfo_v1 *HGamEventInfo;
   xAOD::AuxInfoBase *HGamEventInfoAux_;
   Float_t         HGamEventInfoAuxDyn_DR_y_y;
   Float_t         HGamEventInfoAuxDyn_DRmin_y_j;
   Float_t         HGamEventInfoAuxDyn_DRmin_y_j_2;
   Float_t         HGamEventInfoAuxDyn_Deta_j_j;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_30;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_30_signed;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_50;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_50_signed;
   Float_t         HGamEventInfoAuxDyn_Dphi_y_y;
   Float_t         HGamEventInfoAuxDyn_Dphi_yy_jj;
   Float_t         HGamEventInfoAuxDyn_Dphi_yy_jj_30;
   Float_t         HGamEventInfoAuxDyn_Dphi_yy_jj_50;
   Float_t         HGamEventInfoAuxDyn_Dy_j_j;
   Float_t         HGamEventInfoAuxDyn_Dy_j_j_30;
   Float_t         HGamEventInfoAuxDyn_Dy_y_y;
   Float_t         HGamEventInfoAuxDyn_Dy_yy_jj;
   Float_t         HGamEventInfoAuxDyn_Dy_yy_jj_30;
   Float_t         HGamEventInfoAuxDyn_E_y1;
   Float_t         HGamEventInfoAuxDyn_E_y2;
   Float_t         HGamEventInfoAuxDyn_HT_30;
   Float_t         HGamEventInfoAuxDyn_HTall_30;
   Int_t           HGamEventInfoAuxDyn_Hc_cutFlowAllJet;
   Int_t           HGamEventInfoAuxDyn_Hc_cutFlowLeadJet;
   vector<int>     *HGamEventInfoAuxDyn_Hc_jet_truthlabel;
   Int_t           HGamEventInfoAuxDyn_Hc_leadJet_truthLabel;
   Int_t           HGamEventInfoAuxDyn_Hc_m_yy_cat;
   vector<float>   *HGamEventInfoAuxDyn_Hc_y1_j_dr;
   vector<float>   *HGamEventInfoAuxDyn_Hc_y2_j_dr;
   vector<float>   *HGamEventInfoAuxDyn_HggPrimaryVerticesScore;
   vector<float>   *HGamEventInfoAuxDyn_HggPrimaryVerticesZ;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_BDT;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_BDT_cat;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_cutFlow;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dEta_yy;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_yy;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_weight;
   Int_t           HGamEventInfoAuxDyn_NLoosePhotons;
   Int_t           HGamEventInfoAuxDyn_N_e;
   Int_t           HGamEventInfoAuxDyn_N_j;
   Int_t           HGamEventInfoAuxDyn_N_j_30;
   Int_t           HGamEventInfoAuxDyn_N_j_50;
   Int_t           HGamEventInfoAuxDyn_N_j_btag;
   Int_t           HGamEventInfoAuxDyn_N_j_btag30;
   Int_t           HGamEventInfoAuxDyn_N_j_central;
   Int_t           HGamEventInfoAuxDyn_N_j_central30;
   Int_t           HGamEventInfoAuxDyn_N_lep;
   Int_t           HGamEventInfoAuxDyn_N_lep_15;
   Int_t           HGamEventInfoAuxDyn_N_mu;
   Float_t         HGamEventInfoAuxDyn_Zepp;
   Float_t         HGamEventInfoAuxDyn_beamSpotWeight;
   Int_t           HGamEventInfoAuxDyn_catCoup_GlobalICHEP;
   Int_t           HGamEventInfoAuxDyn_catCoup_HybridICHEP;
   Int_t           HGamEventInfoAuxDyn_catCoup_MonoH_2var;
   Int_t           HGamEventInfoAuxDyn_catCoup_Moriond2017BDT;
   Int_t           HGamEventInfoAuxDyn_catCoup_XGBoost_ttH;
   Int_t           HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP;
   Int_t           HGamEventInfoAuxDyn_catLowHighMyy_conv;
   Int_t           HGamEventInfoAuxDyn_catMass_Run1;
   Int_t           HGamEventInfoAuxDyn_catMass_conv;
   Int_t           HGamEventInfoAuxDyn_catMass_eta;
   Int_t           HGamEventInfoAuxDyn_catMass_mu;
   Int_t           HGamEventInfoAuxDyn_catMass_pT;
   Char_t          HGamEventInfoAuxDyn_catXS_HiggsHF;
   Char_t          HGamEventInfoAuxDyn_catXS_VBF;
   Char_t          HGamEventInfoAuxDyn_catXS_nbjet;
   Char_t          HGamEventInfoAuxDyn_catXS_ttH;
   Float_t         HGamEventInfoAuxDyn_cosTS_yy;
   Float_t         HGamEventInfoAuxDyn_cosTS_yyjj;
   Float_t         HGamEventInfoAuxDyn_crossSectionBRfilterEff;
   Int_t           HGamEventInfoAuxDyn_cutFlow;
   Float_t         HGamEventInfoAuxDyn_eta_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_eta_recotop1;
   vector<float>   *HGamEventInfoAuxDyn_fcnc_Score;
   Int_t           HGamEventInfoAuxDyn_fcnc_cat;
   Int_t           HGamEventInfoAuxDyn_fcnc_cutFlow;
   Float_t         HGamEventInfoAuxDyn_fcnc_fJvtweight;
   Float_t         HGamEventInfoAuxDyn_fcnc_weight;
   Float_t         HGamEventInfoAuxDyn_hardestVertexPhi;
   Float_t         HGamEventInfoAuxDyn_hardestVertexSumPt2;
   Float_t         HGamEventInfoAuxDyn_hardestVertexZ;
   vector<int>     *HGamEventInfoAuxDyn_idx_jets_recotop1;
   vector<int>     *HGamEventInfoAuxDyn_idx_jets_recotop2;
   Char_t          HGamEventInfoAuxDyn_isDalitz;
   Char_t          HGamEventInfoAuxDyn_isPassed;
   Char_t          HGamEventInfoAuxDyn_isPassedBasic;
   Char_t          HGamEventInfoAuxDyn_isPassedHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedIsolation;
   Char_t          HGamEventInfoAuxDyn_isPassedIsolationHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedIsolationLowMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedJetEventClean;
   Char_t          HGamEventInfoAuxDyn_isPassedLowMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedMassCut;
   Char_t          HGamEventInfoAuxDyn_isPassedPID;
   Char_t          HGamEventInfoAuxDyn_isPassedPreselection;
   Char_t          HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedRelPtCuts;
   Char_t          HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedTriggerMatch;
   Float_t         HGamEventInfoAuxDyn_m_alljet;
   Float_t         HGamEventInfoAuxDyn_m_alljet_30;
   Float_t         HGamEventInfoAuxDyn_m_ee;
   Float_t         HGamEventInfoAuxDyn_m_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_m_jj;
   Float_t         HGamEventInfoAuxDyn_m_jj_30;
   Float_t         HGamEventInfoAuxDyn_m_jj_50;
   Float_t         HGamEventInfoAuxDyn_m_mumu;
   Float_t         HGamEventInfoAuxDyn_m_recotop1;
   Float_t         HGamEventInfoAuxDyn_m_yy;
   Float_t         HGamEventInfoAuxDyn_m_yy_hardestVertex;
   Float_t         HGamEventInfoAuxDyn_m_yy_resolution;
   Float_t         HGamEventInfoAuxDyn_m_yy_truthVertex;
   Float_t         HGamEventInfoAuxDyn_m_yy_zCommon;
   Float_t         HGamEventInfoAuxDyn_m_yyj;
   Float_t         HGamEventInfoAuxDyn_m_yyj_30;
   Float_t         HGamEventInfoAuxDyn_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_massTrans;
   Float_t         HGamEventInfoAuxDyn_maxTau_yyj_30;
   Float_t         HGamEventInfoAuxDyn_met_TST;
   Int_t           HGamEventInfoAuxDyn_met_cat;
   Float_t         HGamEventInfoAuxDyn_met_hardVertexTST;
   Float_t         HGamEventInfoAuxDyn_met_weight;
   Float_t         HGamEventInfoAuxDyn_mu;
   Int_t           HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP;
   Int_t           HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP;
   Int_t           HGamEventInfoAuxDyn_numberOfPrimaryVertices;
   Float_t         HGamEventInfoAuxDyn_pT_hard;
   Float_t         HGamEventInfoAuxDyn_pT_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_pT_j1;
   Float_t         HGamEventInfoAuxDyn_pT_j1_30;
   Float_t         HGamEventInfoAuxDyn_pT_j2;
   Float_t         HGamEventInfoAuxDyn_pT_j2_30;
   Float_t         HGamEventInfoAuxDyn_pT_j3_30;
   Float_t         HGamEventInfoAuxDyn_pT_jj;
   Float_t         HGamEventInfoAuxDyn_pT_recotop1;
   Float_t         HGamEventInfoAuxDyn_pT_y1;
   Float_t         HGamEventInfoAuxDyn_pT_y2;
   Float_t         HGamEventInfoAuxDyn_pT_yy;
   Float_t         HGamEventInfoAuxDyn_pT_yyj;
   Float_t         HGamEventInfoAuxDyn_pT_yyj_30;
   Float_t         HGamEventInfoAuxDyn_pT_yyjj;
   Float_t         HGamEventInfoAuxDyn_pT_yyjj_30;
   Float_t         HGamEventInfoAuxDyn_pT_yyjj_50;
   Float_t         HGamEventInfoAuxDyn_pTlepMET;
   Float_t         HGamEventInfoAuxDyn_pTt_yy;
   Char_t          HGamEventInfoAuxDyn_passCrackVetoCleaning;
   Char_t          HGamEventInfoAuxDyn_passMeyCut;
   Float_t         HGamEventInfoAuxDyn_phiStar_yy;
   Float_t         HGamEventInfoAuxDyn_phi_TST;
   Float_t         HGamEventInfoAuxDyn_phi_hardVertexTST;
   Float_t         HGamEventInfoAuxDyn_phi_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_phi_recotop1;
   Float_t         HGamEventInfoAuxDyn_pileupVertexPhi;
   Float_t         HGamEventInfoAuxDyn_pileupVertexSumPt2;
   Float_t         HGamEventInfoAuxDyn_pileupVertexZ;
   Float_t         HGamEventInfoAuxDyn_pileupWeight;
   Float_t         HGamEventInfoAuxDyn_pt_llmax;
   Float_t         HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP;
   Float_t         HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP;
   Float_t         HGamEventInfoAuxDyn_score_MonoH_2var;
   Float_t         HGamEventInfoAuxDyn_score_WH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_ZH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_recotop1;
   Float_t         HGamEventInfoAuxDyn_score_recotop2;
   Float_t         HGamEventInfoAuxDyn_score_tHjb_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_tWH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_ttH;
   Float_t         HGamEventInfoAuxDyn_score_ttHCP;
   Float_t         HGamEventInfoAuxDyn_score_ttH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_selectedVertexPhi;
   Float_t         HGamEventInfoAuxDyn_selectedVertexSumPt2;
   Float_t         HGamEventInfoAuxDyn_selectedVertexZ;
   Float_t         HGamEventInfoAuxDyn_sumTau_yyj_30;
   Float_t         HGamEventInfoAuxDyn_sumet_TST;
   Float_t         HGamEventInfoAuxDyn_sumet_hardVertexTST;
   Float_t         HGamEventInfoAuxDyn_weight;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP;
   Float_t         HGamEventInfoAuxDyn_weightCatXS_HiggsHF;
   Float_t         HGamEventInfoAuxDyn_weightCatXS_nbjet;
   Float_t         HGamEventInfoAuxDyn_weightCatXS_ttH;
   Float_t         HGamEventInfoAuxDyn_weightFJvt;
   Float_t         HGamEventInfoAuxDyn_weightFJvt_30;
   Float_t         HGamEventInfoAuxDyn_weightInitial;
   Float_t         HGamEventInfoAuxDyn_weightJvt;
   Float_t         HGamEventInfoAuxDyn_weightJvt_30;
   Float_t         HGamEventInfoAuxDyn_weightJvt_50;
   Float_t         HGamEventInfoAuxDyn_weightLowHighMyy;
   Float_t         HGamEventInfoAuxDyn_weightN_lep;
   Float_t         HGamEventInfoAuxDyn_weightN_lep_15;
   Float_t         HGamEventInfoAuxDyn_weightSF;
   Float_t         HGamEventInfoAuxDyn_weightTrigSF;
   Float_t         HGamEventInfoAuxDyn_yAbs_j1;
   Float_t         HGamEventInfoAuxDyn_yAbs_j1_30;
   Float_t         HGamEventInfoAuxDyn_yAbs_j2;
   Float_t         HGamEventInfoAuxDyn_yAbs_j2_30;
   Float_t         HGamEventInfoAuxDyn_yAbs_yy;
   Int_t           HGamEventInfoAuxDyn_yyb_cutFlow;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_planarFlow;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_sphericityT;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_eta_1;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_eta_2;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_1;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_2;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_phi_1;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_phi_2;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_planarFlow;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_pt_1;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_pt_2;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_sphericityT;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_candidate_jet1_fix;
   Int_t           HGamEventInfoAuxDyn_yybb_candidate_jet2_fix;
   Float_t         HGamEventInfoAuxDyn_yybb_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_m_yyjj_tilde;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Signal_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ggF_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Signal_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ggF_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Signal_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ggF_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_BCal_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_KF_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_planarFlow;
   Float_t         HGamEventInfoAuxDyn_yybb_sphericityT;
   Int_t           HGamEventInfoAuxDyn_yybb_vbf_jet1;
   Int_t           HGamEventInfoAuxDyn_yybb_vbf_jet2;
   Float_t         HGamEventInfoAuxDyn_yybb_weight;
   Float_t         HGamEventInfoAuxDyn_zCommon;
   DataVector<xAOD::TruthParticle_v1> *HGamTruthPhotons;
   xAOD::AuxContainerBase *HGamTruthPhotonsAux_;
   vector<float>   *HGamTruthPhotonsAuxDyn_e;
   vector<float>   *HGamTruthPhotonsAuxDyn_eta;
   vector<float>   *HGamTruthPhotonsAuxDyn_etcone20;
   vector<float>   *HGamTruthPhotonsAuxDyn_etcone40;
   vector<char>    *HGamTruthPhotonsAuxDyn_isIsolated;
   vector<float>   *HGamTruthPhotonsAuxDyn_m;
   vector<float>   *HGamTruthPhotonsAuxDyn_partonetcone20;
   vector<float>   *HGamTruthPhotonsAuxDyn_partonetcone40;
   vector<float>   *HGamTruthPhotonsAuxDyn_pt;
   vector<float>   *HGamTruthPhotonsAuxDyn_ptcone20;
   vector<float>   *HGamTruthPhotonsAuxDyn_ptcone40;
   vector<float>   *HGamTruthPhotonsAuxDyn_px;
   vector<float>   *HGamTruthPhotonsAuxDyn_py;
   vector<float>   *HGamTruthPhotonsAuxDyn_pz;
   Int_t           HGamTruthPhotonsAuxDyn_recoLink_;
   UInt_t          HGamTruthPhotonsAuxDyn_recoLink_m_persKey[kMaxHGamTruthPhotonsAuxDyn_recoLink];   //[HGamTruthPhotonsAuxDyn.recoLink_]
   UInt_t          HGamTruthPhotonsAuxDyn_recoLink_m_persIndex[kMaxHGamTruthPhotonsAuxDyn_recoLink];   //[HGamTruthPhotonsAuxDyn.recoLink_]
   vector<int>     *HGamTruthPhotonsAuxDyn_truthOrigin;
   vector<int>     *HGamTruthPhotonsAuxDyn_truthType;
   DataVector<xAOD::TruthParticle_v1> *HGamTruthElectrons;
   xAOD::AuxContainerBase *HGamTruthElectronsAux_;
   DataVector<xAOD::TruthParticle_v1> *HGamTruthMuons;
   xAOD::AuxContainerBase *HGamTruthMuonsAux_;
   DataVector<xAOD::TruthParticle_v1> *HGamTruthTaus;
   xAOD::AuxContainerBase *HGamTruthTausAux_;
   DataVector<xAOD::Jet_v1> *HGamAntiKt4TruthWZJets;
   xAOD::AuxContainerBase *HGamAntiKt4TruthWZJetsAux_;
   vector<int>     *HGamAntiKt4TruthWZJetsAuxDyn_HadronConeExclTruthLabelID;
   vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > *HGamAntiKt4TruthWZJetsAuxDyn_constituentLinks;
   vector<float>   *HGamAntiKt4TruthWZJetsAuxDyn_eta;
   vector<float>   *HGamAntiKt4TruthWZJetsAuxDyn_m;
   vector<float>   *HGamAntiKt4TruthWZJetsAuxDyn_phi;
   vector<float>   *HGamAntiKt4TruthWZJetsAuxDyn_pt;
   DataVector<xAOD::MissingET_v1> *HGamMET_Truth;
   xAOD::AuxContainerBase *HGamMET_TruthAux_;
   vector<float>   *HGamMET_TruthAuxDyn_mpx;
   vector<float>   *HGamMET_TruthAuxDyn_mpy;
   vector<string>  *HGamMET_TruthAuxDyn_name;
   vector<ULong64_t> *HGamMET_TruthAuxDyn_source;
   vector<float>   *HGamMET_TruthAuxDyn_sumet;
   DataVector<xAOD::TruthParticle_v1> *HGamTruthHiggsBosons;
   xAOD::AuxContainerBase *HGamTruthHiggsBosonsAux_;
   vector<float>   *HGamTruthHiggsBosonsAuxDyn_e;
   vector<float>   *HGamTruthHiggsBosonsAuxDyn_m;
   vector<float>   *HGamTruthHiggsBosonsAuxDyn_pt;
   vector<float>   *HGamTruthHiggsBosonsAuxDyn_px;
   vector<float>   *HGamTruthHiggsBosonsAuxDyn_py;
   vector<float>   *HGamTruthHiggsBosonsAuxDyn_pz;
   DataVector<xAOD::TruthEvent_v1> *TruthEvents;
   xAOD::AuxContainerBase *TruthEventsAux_;
   vector<int>     *TruthEventsAuxDyn_PDFID1;
   vector<int>     *TruthEventsAuxDyn_PDFID2;
   vector<int>     *TruthEventsAuxDyn_PDGID1;
   vector<int>     *TruthEventsAuxDyn_PDGID2;
   vector<float>   *TruthEventsAuxDyn_Q;
   vector<float>   *TruthEventsAuxDyn_X1;
   vector<float>   *TruthEventsAuxDyn_X2;
   vector<float>   *TruthEventsAuxDyn_XF1;
   vector<float>   *TruthEventsAuxDyn_XF2;
   vector<vector<float> > *TruthEventsAuxDyn_weights;
   xAOD::EventInfo_v1 *HGamTruthEventInfo;
   xAOD::AuxInfoBase *HGamTruthEventInfoAux_;
   Float_t         HGamTruthEventInfoAuxDyn_DR_y_y;
   Float_t         HGamTruthEventInfoAuxDyn_DRmin_y_j;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_j_j;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_j_j_30;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_j_j_30_signed;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_j_j_50;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_j_j_50_signed;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_y_y;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_yy_jj;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_yy_jj_30;
   Float_t         HGamTruthEventInfoAuxDyn_Dphi_yy_jj_50;
   Float_t         HGamTruthEventInfoAuxDyn_Dy_j_j;
   Float_t         HGamTruthEventInfoAuxDyn_Dy_j_j_30;
   Float_t         HGamTruthEventInfoAuxDyn_Dy_y_y;
   Float_t         HGamTruthEventInfoAuxDyn_Dy_yy_jj;
   Float_t         HGamTruthEventInfoAuxDyn_Dy_yy_jj_30;
   Float_t         HGamTruthEventInfoAuxDyn_E_y1;
   Float_t         HGamTruthEventInfoAuxDyn_E_y2;
   Float_t         HGamTruthEventInfoAuxDyn_HT_30;
   Float_t         HGamTruthEventInfoAuxDyn_HTall_30;
   Int_t           HGamTruthEventInfoAuxDyn_Hc_N_bjets25;
   Int_t           HGamTruthEventInfoAuxDyn_Hc_N_cjets25;
   Int_t           HGamTruthEventInfoAuxDyn_Hc_N_cjets30;
   Int_t           HGamTruthEventInfoAuxDyn_Hc_cutFlowAllJet;
   Int_t           HGamTruthEventInfoAuxDyn_HiggsHF_cutFlow;
   Int_t           HGamTruthEventInfoAuxDyn_N_e;
   Int_t           HGamTruthEventInfoAuxDyn_N_j;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_30;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_50;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_btag30;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_central;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_central30;
   Int_t           HGamTruthEventInfoAuxDyn_N_lep;
   Int_t           HGamTruthEventInfoAuxDyn_N_lep_15;
   Int_t           HGamTruthEventInfoAuxDyn_N_mu;
   Float_t         HGamTruthEventInfoAuxDyn_Zepp;
   Float_t         HGamTruthEventInfoAuxDyn_alldm_pt;
   Int_t           HGamTruthEventInfoAuxDyn_catCoup;
   Char_t          HGamTruthEventInfoAuxDyn_catXS_HiggsHF;
   Char_t          HGamTruthEventInfoAuxDyn_catXS_VBF;
   Char_t          HGamTruthEventInfoAuxDyn_catXS_nbjet;
   Char_t          HGamTruthEventInfoAuxDyn_catXS_ttH;
   Float_t         HGamTruthEventInfoAuxDyn_cosTS_yy;
   Float_t         HGamTruthEventInfoAuxDyn_cosTS_yyjj;
   Char_t          HGamTruthEventInfoAuxDyn_isFiducial;
   Char_t          HGamTruthEventInfoAuxDyn_isFiducialHighMyy;
   Char_t          HGamTruthEventInfoAuxDyn_isFiducialKinOnly;
   Char_t          HGamTruthEventInfoAuxDyn_isFiducialLowMyy;
   Char_t          HGamTruthEventInfoAuxDyn_isVyyOverlap;
   Float_t         HGamTruthEventInfoAuxDyn_m_ee;
   Float_t         HGamTruthEventInfoAuxDyn_m_h1;
   Float_t         HGamTruthEventInfoAuxDyn_m_h2;
   Float_t         HGamTruthEventInfoAuxDyn_m_hh;
   Float_t         HGamTruthEventInfoAuxDyn_m_jj;
   Float_t         HGamTruthEventInfoAuxDyn_m_jj_30;
   Float_t         HGamTruthEventInfoAuxDyn_m_jj_50;
   Float_t         HGamTruthEventInfoAuxDyn_m_mumu;
   Float_t         HGamTruthEventInfoAuxDyn_m_yy;
   Float_t         HGamTruthEventInfoAuxDyn_m_yyj;
   Float_t         HGamTruthEventInfoAuxDyn_m_yyj_30;
   Float_t         HGamTruthEventInfoAuxDyn_m_yyjj;
   Float_t         HGamTruthEventInfoAuxDyn_massTrans;
   Float_t         HGamTruthEventInfoAuxDyn_maxTau_yyj_30;
   Float_t         HGamTruthEventInfoAuxDyn_met_NonHad;
   Float_t         HGamTruthEventInfoAuxDyn_met_NonInt;
   Float_t         HGamTruthEventInfoAuxDyn_pT_h1;
   Float_t         HGamTruthEventInfoAuxDyn_pT_h2;
   Float_t         HGamTruthEventInfoAuxDyn_pT_hard;
   Float_t         HGamTruthEventInfoAuxDyn_pT_j1;
   Float_t         HGamTruthEventInfoAuxDyn_pT_j1_30;
   Float_t         HGamTruthEventInfoAuxDyn_pT_j2;
   Float_t         HGamTruthEventInfoAuxDyn_pT_j2_30;
   Float_t         HGamTruthEventInfoAuxDyn_pT_j3_30;
   Float_t         HGamTruthEventInfoAuxDyn_pT_jj;
   Float_t         HGamTruthEventInfoAuxDyn_pT_y1;
   Float_t         HGamTruthEventInfoAuxDyn_pT_y2;
   Float_t         HGamTruthEventInfoAuxDyn_pT_yy;
   Float_t         HGamTruthEventInfoAuxDyn_pT_yyj_30;
   Float_t         HGamTruthEventInfoAuxDyn_pT_yyjj_30;
   Float_t         HGamTruthEventInfoAuxDyn_pT_yyjj_50;
   Float_t         HGamTruthEventInfoAuxDyn_pTlepMET;
   Float_t         HGamTruthEventInfoAuxDyn_pTt_yy;
   Char_t          HGamTruthEventInfoAuxDyn_passMeyCut;
   Float_t         HGamTruthEventInfoAuxDyn_phiStar_yy;
   Int_t           HGamTruthEventInfoAuxDyn_procCoup;
   Float_t         HGamTruthEventInfoAuxDyn_pt_llmax;
   Float_t         HGamTruthEventInfoAuxDyn_sumTau_yyj_30;
   Float_t         HGamTruthEventInfoAuxDyn_sumet_Int;
   Float_t         HGamTruthEventInfoAuxDyn_vertexZ;
   Float_t         HGamTruthEventInfoAuxDyn_yAbs_j1;
   Float_t         HGamTruthEventInfoAuxDyn_yAbs_j1_30;
   Float_t         HGamTruthEventInfoAuxDyn_yAbs_j2;
   Float_t         HGamTruthEventInfoAuxDyn_yAbs_j2_30;
   Float_t         HGamTruthEventInfoAuxDyn_yAbs_yy;
   Float_t         HGamTruthEventInfoAuxDyn_y_h1;
   Float_t         HGamTruthEventInfoAuxDyn_y_h2;
   xAOD::EventInfo_v1 *EventInfo;
   xAOD::AuxInfoBase *EventInfoAux_;
   Char_t          EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad;
   Char_t          EventInfoAuxDyn_DFCommonJets_isBadBatman;
   Float_t         EventInfoAuxDyn_HTXS_Higgs_eta;
   Float_t         EventInfoAuxDyn_HTXS_Higgs_m;
   Float_t         EventInfoAuxDyn_HTXS_Higgs_phi;
   Float_t         EventInfoAuxDyn_HTXS_Higgs_pt;
   Int_t           EventInfoAuxDyn_HTXS_Njets_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Njets_pTjet30;
   Int_t           EventInfoAuxDyn_HTXS_Stage0_Category;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet30;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet30;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet30;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet30;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_Category_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_Category_pTjet30;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet25;
   Int_t           EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet30;
   vector<float>   *EventInfoAuxDyn_HTXS_V_jets30_eta;
   vector<float>   *EventInfoAuxDyn_HTXS_V_jets30_m;
   vector<float>   *EventInfoAuxDyn_HTXS_V_jets30_phi;
   vector<float>   *EventInfoAuxDyn_HTXS_V_jets30_pt;
   Float_t         EventInfoAuxDyn_HTXS_V_pt;
   Int_t           EventInfoAuxDyn_HTXS_errorCode;
   Int_t           EventInfoAuxDyn_HTXS_isZ2vvDecay;
   Int_t           EventInfoAuxDyn_HTXS_prodMode;
   Float_t         EventInfoAuxDyn_actualInteractionsPerCrossing;
   Float_t         EventInfoAuxDyn_averageInteractionsPerCrossing;
   Float_t         EventInfoAuxDyn_centralEventShapeDensity;
   UInt_t          EventInfoAuxDyn_coreFlags;
   ULong64_t       EventInfoAuxDyn_eventNumber;
   UInt_t          EventInfoAuxDyn_eventTypeBitmask;
   Float_t         EventInfoAuxDyn_forwardEventShapeDensity;
   UInt_t          EventInfoAuxDyn_lumiBlock;
   UInt_t          EventInfoAuxDyn_mcChannelNumber;
   vector<float>   *EventInfoAuxDyn_mcEventWeights;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g20_tight_icaloloose_L12EM15VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g120_loose_L1EM22VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g140_loose_L1EM22VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH;
   UInt_t          EventInfoAuxDyn_runNumber;
   vector<unsigned short> *HGamElectronsAuxDyn_author;
   vector<float>   *HGamElectronsAuxDyn_charge;
   vector<float>   *HGamElectronsAuxDyn_eta;
   vector<float>   *HGamElectronsAuxDyn_eta_s2;
   vector<char>    *HGamElectronsAuxDyn_isTight;
   vector<float>   *HGamElectronsAuxDyn_m;
   vector<float>   *HGamElectronsAuxDyn_phi;
   vector<float>   *HGamElectronsAuxDyn_pt;
   vector<float>   *HGamElectronsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;
   vector<float>   *HGamElectronsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;
   vector<float>   *HGamElectronsAuxDyn_scaleFactor;
   vector<float>   *HGamElectronsAuxDyn_topoetcone20;
   vector<int>     *HGamElectronsAuxDyn_truthOrigin;
   vector<int>     *HGamElectronsAuxDyn_truthType;
   vector<float>   *HGamTruthElectronsAuxDyn_e;
   vector<float>   *HGamTruthElectronsAuxDyn_eta;
   vector<float>   *HGamTruthElectronsAuxDyn_m;
   vector<float>   *HGamTruthElectronsAuxDyn_pt;
   vector<float>   *HGamTruthElectronsAuxDyn_px;
   vector<float>   *HGamTruthElectronsAuxDyn_py;
   vector<float>   *HGamTruthElectronsAuxDyn_pz;
   vector<int>     *HGamTruthElectronsAuxDyn_truthOrigin;
   vector<int>     *HGamTruthElectronsAuxDyn_truthType;
   vector<ElementLink<DataVector<xAOD::IParticle> > > *HGamElectronsAuxDyn_truthLink;
   vector<ElementLink<DataVector<xAOD::IParticle> > > *HGamTruthElectronsAuxDyn_recoLink;
   vector<float>   *HGamMuonsAuxDyn_charge;
   vector<float>   *HGamMuonsAuxDyn_eta;
   vector<char>    *HGamMuonsAuxDyn_isBad;
   vector<unsigned short> *HGamMuonsAuxDyn_muonType;
   vector<float>   *HGamMuonsAuxDyn_neflowisol20;
   vector<char>    *HGamMuonsAuxDyn_passIPCut;
   vector<float>   *HGamMuonsAuxDyn_phi;
   vector<float>   *HGamMuonsAuxDyn_pt;
   vector<float>   *HGamMuonsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500;
   vector<float>   *HGamMuonsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;
   vector<float>   *HGamMuonsAuxDyn_scaleFactor;
   vector<int>     *HGamMuonsAuxDyn_truthOrigin;
   vector<int>     *HGamMuonsAuxDyn_truthType;
   vector<float>   *HGamTruthMuonsAuxDyn_e;
   vector<float>   *HGamTruthMuonsAuxDyn_eta;
   vector<float>   *HGamTruthMuonsAuxDyn_m;
   vector<float>   *HGamTruthMuonsAuxDyn_pt;
   vector<float>   *HGamTruthMuonsAuxDyn_px;
   vector<float>   *HGamTruthMuonsAuxDyn_py;
   vector<float>   *HGamTruthMuonsAuxDyn_pz;
   vector<int>     *HGamTruthMuonsAuxDyn_truthOrigin;
   vector<int>     *HGamTruthMuonsAuxDyn_truthType;
   vector<float>   *HGamTauJetsAuxDyn_RNNEleScore;
   vector<float>   *HGamTauJetsAuxDyn_RNNEleScoreSigTrans;
   vector<float>   *HGamTauJetsAuxDyn_RNNJetScore;
   vector<float>   *HGamTauJetsAuxDyn_RNNJetScoreSigTrans;
   vector<float>   *HGamTauJetsAuxDyn_TauEffSF_EleRNNLoose;
   vector<float>   *HGamTauJetsAuxDyn_TauEffSF_EleRNNMedium;
   vector<float>   *HGamTauJetsAuxDyn_TauEffSF_JetIDRNNLoose;
   vector<float>   *HGamTauJetsAuxDyn_TauEffSF_JetIDRNNMedium;
   vector<float>   *HGamTauJetsAuxDyn_TauEffSF_JetIDRNNTight;
   vector<float>   *HGamTauJetsAuxDyn_TauEffSF_Reco;
   vector<float>   *HGamTauJetsAuxDyn_charge;
   vector<unsigned int> *HGamTauJetsAuxDyn_classifierType;
   vector<float>   *HGamTauJetsAuxDyn_eta;
   vector<char>    *HGamTauJetsAuxDyn_isHadronicTau;
   vector<unsigned int> *HGamTauJetsAuxDyn_isTauFlags;
   vector<char>    *HGamTauJetsAuxDyn_isTruthJet;
   vector<int>     *HGamTauJetsAuxDyn_nTracks;
   vector<char>    *HGamTauJetsAuxDyn_overlapJet;
   vector<float>   *HGamTauJetsAuxDyn_phi;
   vector<float>   *HGamTauJetsAuxDyn_pt;
   vector<int>     *HGamTauJetsAuxDyn_truthMatchID;
   Int_t           HGamTruthEventInfoAuxDyn_HiggsHF_N_bjets;
   Int_t           HGamTruthEventInfoAuxDyn_HiggsHF_N_cjets;
   vector<float>   *HGamTruthTausAuxDyn_e;
   vector<float>   *HGamTruthTausAuxDyn_eta;
   vector<float>   *HGamTruthTausAuxDyn_pt;
   vector<float>   *HGamTruthTausAuxDyn_px;
   vector<float>   *HGamTruthTausAuxDyn_py;
   vector<float>   *HGamTruthTausAuxDyn_pz;
   vector<int>     *HGamTruthTausAuxDyn_truthOrigin;
   vector<int>     *HGamTruthTausAuxDyn_truthType;
   vector<ElementLink<DataVector<xAOD::IParticle> > > *HGamTruthTausAuxDyn_recoLink;
   Int_t           HGamEventInfoAuxDyn_yyb_m_yy_cat;
   Int_t           HGamEventInfoAuxDyn_yyb_SelectedForwardJet;
   Int_t           HGamEventInfoAuxDyn_yyb_SelectedTaggedJet;
   Float_t         HGamEventInfoAuxDyn_yyb_VLQmass;
   Int_t           HGamEventInfoAuxDyn_yyb_truth_label_forwardjet;
   Int_t           HGamEventInfoAuxDyn_yyb_truth_label_taggedjet;
   Float_t         HGamEventInfoAuxDyn_yyb_weight;

   // List of branches
   TBranch        *b_HGamPhotons;   //!
   TBranch        *b_HGamPhotonsAux_;   //!
   TBranch        *b_HGamPhotonsAuxDyn_DeltaE;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Eratio;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Reta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rhad;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rhad1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rphi;   //!
   TBranch        *b_HGamPhotonsAuxDyn_author;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_E;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_E_TileGap3;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es0;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es3;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_eta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_etaCalo;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_phiCalo;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_ratioEs1Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_conversionType;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk1nPixHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk1nSCTHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk2nPixHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk2nSCTHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_e277;   //!
   TBranch        *b_HGamPhotonsAuxDyn_eta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_eta_s1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_eta_s2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_f1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_fracs1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isEMTight;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoFixedCutLoose;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoFixedCutTight;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoTightCaloOnly;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isTight;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isTight_nofudge;   //!
   TBranch        *b_HGamPhotonsAuxDyn_m;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_energy;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_gain;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_onlId;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_time;   //!
   TBranch        *b_HGamPhotonsAuxDyn_parentPdgId;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pdgId;   //!
   TBranch        *b_HGamPhotonsAuxDyn_phi;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt1conv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt2conv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt_s2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptcone20;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es0;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es3;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_ratioEs1Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_relEreso;   //!
   TBranch        *b_HGamPhotonsAuxDyn_scaleFactor;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone20;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone20_DDcorr;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone40;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone40_DDcorr;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthLink_;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthLink_m_persKey;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthLink_m_persIndex;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthRconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthType;   //!
   TBranch        *b_HGamPhotonsAuxDyn_weta1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_weta2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_wtots1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_zconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_zvertex;   //!
   TBranch        *b_HGamElectrons;   //!
   TBranch        *b_HGamElectronsAux_;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtx;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAux_;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pu;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJets;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAux_;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_CorrJvf;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DFCommonJets_fJvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_bin;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pb;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pc;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DetectorEta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_bin;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pb;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pc;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_HadronConeExclTruthLabelID;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Jvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_NNRegPtSF;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_eta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_m;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_phi;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_pt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PartonTruthLabelID;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PtRecoSF;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Rpt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_fjvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_jvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Timing;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persKey;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persIndex;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_eta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtHS;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtPU;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_m;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_overlapTau;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_passFJVT;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_phi;   //!
   TBranch        *b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_pt;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAux_;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pu;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAux_;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DFCommonJets_fJvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_bin;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pb;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pc;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_DetectorEta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_bin;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pb;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pc;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_HadronConeExclTruthLabelID;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_Jvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_jvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persKey;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persIndex;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_eta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_isJvtHS;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_isJvtPU;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_m;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_overlapTau;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_passFJVT;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_phi;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJetsAuxDyn_pt;   //!
   TBranch        *b_HGamMuons;   //!
   TBranch        *b_HGamMuonsAux_;   //!
   TBranch        *b_HGamTauJets;   //!
   TBranch        *b_HGamTauJetsAux_;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlow;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAux_;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet;   //!
   TBranch        *b_HGamEventInfo;   //!
   TBranch        *b_HGamEventInfoAux_;   //!
   TBranch        *b_HGamEventInfoAuxDyn_DR_y_y;   //!
   TBranch        *b_HGamEventInfoAuxDyn_DRmin_y_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_DRmin_y_j_2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Deta_j_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_30_signed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_50_signed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_y_y;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_yy_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_yy_jj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_yy_jj_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_j_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_j_j_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_y_y;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_yy_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_yy_jj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_E_y1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_E_y2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HT_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HTall_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_cutFlowAllJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_cutFlowLeadJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_jet_truthlabel;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_leadJet_truthLabel;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_m_yy_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_y1_j_dr;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_y2_j_dr;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HggPrimaryVerticesScore;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HggPrimaryVerticesZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_BDT_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dEta_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_NLoosePhotons;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_e;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_btag;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_btag30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_central;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_central30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_lep;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_lep_15;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_mu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Zepp;   //!
   TBranch        *b_HGamEventInfoAuxDyn_beamSpotWeight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_MonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_Moriond2017BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catLowHighMyy_conv;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_Run1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_conv;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_eta;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_mu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_pT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_HiggsHF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_VBF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_nbjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_cosTS_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_cosTS_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_crossSectionBRfilterEff;   //!
   TBranch        *b_HGamEventInfoAuxDyn_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_eta_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_eta_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_fJvtweight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_hardestVertexPhi;   //!
   TBranch        *b_HGamEventInfoAuxDyn_hardestVertexSumPt2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_hardestVertexZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_idx_jets_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_idx_jets_recotop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isDalitz;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedBasic;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedIsolation;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedIsolationHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedIsolationLowMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedJetEventClean;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedLowMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedMassCut;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedPID;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedPreselection;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedRelPtCuts;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedTriggerMatch;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_alljet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_alljet_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_ee;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_jj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_jj_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_mumu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_hardestVertex;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_resolution;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_truthVertex;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_zCommon;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yyj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_massTrans;   //!
   TBranch        *b_HGamEventInfoAuxDyn_maxTau_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_TST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_hardVertexTST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_mu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_numberOfPrimaryVertices;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_hard;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j1_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j2_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j3_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_y1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_y2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyjj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyjj_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pTlepMET;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pTt_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_passCrackVetoCleaning;   //!
   TBranch        *b_HGamEventInfoAuxDyn_passMeyCut;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phiStar_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_TST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_hardVertexTST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupVertexPhi;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupVertexSumPt2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupVertexZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupWeight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pt_llmax;   //!
   TBranch        *b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_MonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_WH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ZH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_recotop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_tHjb_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_tWH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ttH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_selectedVertexPhi;   //!
   TBranch        *b_HGamEventInfoAuxDyn_selectedVertexSumPt2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_selectedVertexZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_sumTau_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_sumet_TST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_sumet_hardVertexTST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatXS_HiggsHF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatXS_nbjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatXS_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightFJvt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightFJvt_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightInitial;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightJvt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightJvt_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightJvt_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightLowHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightN_lep;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightN_lep_15;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightSF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightTrigSF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j1_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j2_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_planarFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_sphericityT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_eta_1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_eta_2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_phi_1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_phi_2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_planarFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_pt_1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_pt_2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_sphericityT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_candidate_jet1_fix;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_candidate_jet2_fix;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Signal_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ggF_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Signal_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ggF_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Signal_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ggF_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_BCal_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_KF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_planarFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_sphericityT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_vbf_jet1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_vbf_jet2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_zCommon;   //!
   TBranch        *b_HGamTruthPhotons;   //!
   TBranch        *b_HGamTruthPhotonsAux_;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_e;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_eta;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_etcone20;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_etcone40;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_isIsolated;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_m;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_partonetcone20;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_partonetcone40;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_pt;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_ptcone20;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_ptcone40;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_px;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_py;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_pz;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_recoLink_;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_recoLink_m_persKey;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_recoLink_m_persIndex;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamTruthPhotonsAuxDyn_truthType;   //!
   TBranch        *b_HGamTruthElectrons;   //!
   TBranch        *b_HGamTruthElectronsAux_;   //!
   TBranch        *b_HGamTruthMuons;   //!
   TBranch        *b_HGamTruthMuonsAux_;   //!
   TBranch        *b_HGamTruthTaus;   //!
   TBranch        *b_HGamTruthTausAux_;   //!
   TBranch        *b_HGamAntiKt4TruthWZJets;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAux_;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAuxDyn_HadronConeExclTruthLabelID;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAuxDyn_constituentLinks;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAuxDyn_eta;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAuxDyn_m;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAuxDyn_phi;   //!
   TBranch        *b_HGamAntiKt4TruthWZJetsAuxDyn_pt;   //!
   TBranch        *b_HGamMET_Truth;   //!
   TBranch        *b_HGamMET_TruthAux_;   //!
   TBranch        *b_HGamMET_TruthAuxDyn_mpx;   //!
   TBranch        *b_HGamMET_TruthAuxDyn_mpy;   //!
   TBranch        *b_HGamMET_TruthAuxDyn_name;   //!
   TBranch        *b_HGamMET_TruthAuxDyn_source;   //!
   TBranch        *b_HGamMET_TruthAuxDyn_sumet;   //!
   TBranch        *b_HGamTruthHiggsBosons;   //!
   TBranch        *b_HGamTruthHiggsBosonsAux_;   //!
   TBranch        *b_HGamTruthHiggsBosonsAuxDyn_e;   //!
   TBranch        *b_HGamTruthHiggsBosonsAuxDyn_m;   //!
   TBranch        *b_HGamTruthHiggsBosonsAuxDyn_pt;   //!
   TBranch        *b_HGamTruthHiggsBosonsAuxDyn_px;   //!
   TBranch        *b_HGamTruthHiggsBosonsAuxDyn_py;   //!
   TBranch        *b_HGamTruthHiggsBosonsAuxDyn_pz;   //!
   TBranch        *b_TruthEvents;   //!
   TBranch        *b_TruthEventsAux_;   //!
   TBranch        *b_TruthEventsAuxDyn_PDFID1;   //!
   TBranch        *b_TruthEventsAuxDyn_PDFID2;   //!
   TBranch        *b_TruthEventsAuxDyn_PDGID1;   //!
   TBranch        *b_TruthEventsAuxDyn_PDGID2;   //!
   TBranch        *b_TruthEventsAuxDyn_Q;   //!
   TBranch        *b_TruthEventsAuxDyn_X1;   //!
   TBranch        *b_TruthEventsAuxDyn_X2;   //!
   TBranch        *b_TruthEventsAuxDyn_XF1;   //!
   TBranch        *b_TruthEventsAuxDyn_XF2;   //!
   TBranch        *b_TruthEventsAuxDyn_weights;   //!
   TBranch        *b_HGamTruthEventInfo;   //!
   TBranch        *b_HGamTruthEventInfoAux_;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_DR_y_y;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_DRmin_y_j;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_j_j;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_j_j_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_j_j_30_signed;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_j_j_50;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_j_j_50_signed;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_y_y;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_yy_jj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_yy_jj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dphi_yy_jj_50;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dy_j_j;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dy_j_j_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dy_y_y;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dy_yy_jj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Dy_yy_jj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_E_y1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_E_y2;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_HT_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_HTall_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Hc_N_bjets25;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Hc_N_cjets25;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Hc_N_cjets30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Hc_cutFlowAllJet;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_HiggsHF_cutFlow;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_e;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_50;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_btag30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_central;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_central30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_lep;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_lep_15;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_mu;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_Zepp;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_alldm_pt;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_catCoup;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_catXS_HiggsHF;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_catXS_VBF;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_catXS_nbjet;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_catXS_ttH;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_cosTS_yy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_cosTS_yyjj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_isFiducial;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_isFiducialHighMyy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_isFiducialKinOnly;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_isFiducialLowMyy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_isVyyOverlap;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_ee;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_h1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_h2;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_hh;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_jj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_jj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_jj_50;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_mumu;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_yy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_yyj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_yyj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_m_yyjj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_massTrans;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_maxTau_yyj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_met_NonHad;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_met_NonInt;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_h1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_h2;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_hard;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_j1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_j1_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_j2;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_j2_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_j3_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_jj;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_y1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_y2;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_yy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_yyj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_yyjj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pT_yyjj_50;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pTlepMET;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pTt_yy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_passMeyCut;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_phiStar_yy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_procCoup;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_pt_llmax;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_sumTau_yyj_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_sumet_Int;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_vertexZ;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_yAbs_j1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_yAbs_j1_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_yAbs_j2;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_yAbs_j2_30;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_yAbs_yy;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_y_h1;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_y_h2;   //!
   TBranch        *b_EventInfo;   //!
   TBranch        *b_EventInfoAux_;   //!
   TBranch        *b_EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad;   //!
   TBranch        *b_EventInfoAuxDyn_DFCommonJets_isBadBatman;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Higgs_eta;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Higgs_m;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Higgs_phi;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Higgs_pt;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Njets_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Njets_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage0_Category;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_Category_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_Category_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet25;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet30;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_V_jets30_eta;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_V_jets30_m;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_V_jets30_phi;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_V_jets30_pt;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_V_pt;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_errorCode;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_isZ2vvDecay;   //!
   TBranch        *b_EventInfoAuxDyn_HTXS_prodMode;   //!
   TBranch        *b_EventInfoAuxDyn_actualInteractionsPerCrossing;   //!
   TBranch        *b_EventInfoAuxDyn_averageInteractionsPerCrossing;   //!
   TBranch        *b_EventInfoAuxDyn_centralEventShapeDensity;   //!
   TBranch        *b_EventInfoAuxDyn_coreFlags;   //!
   TBranch        *b_EventInfoAuxDyn_eventNumber;   //!
   TBranch        *b_EventInfoAuxDyn_eventTypeBitmask;   //!
   TBranch        *b_EventInfoAuxDyn_forwardEventShapeDensity;   //!
   TBranch        *b_EventInfoAuxDyn_lumiBlock;   //!
   TBranch        *b_EventInfoAuxDyn_mcChannelNumber;   //!
   TBranch        *b_EventInfoAuxDyn_mcEventWeights;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g20_tight_icaloloose_L12EM15VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g120_loose_L1EM22VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g140_loose_L1EM22VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH;   //!
   TBranch        *b_EventInfoAuxDyn_runNumber;   //!
   TBranch        *b_HGamElectronsAuxDyn_author;   //!
   TBranch        *b_HGamElectronsAuxDyn_charge;   //!
   TBranch        *b_HGamElectronsAuxDyn_eta;   //!
   TBranch        *b_HGamElectronsAuxDyn_eta_s2;   //!
   TBranch        *b_HGamElectronsAuxDyn_isTight;   //!
   TBranch        *b_HGamElectronsAuxDyn_m;   //!
   TBranch        *b_HGamElectronsAuxDyn_phi;   //!
   TBranch        *b_HGamElectronsAuxDyn_pt;   //!
   TBranch        *b_HGamElectronsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;   //!
   TBranch        *b_HGamElectronsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;   //!
   TBranch        *b_HGamElectronsAuxDyn_scaleFactor;   //!
   TBranch        *b_HGamElectronsAuxDyn_topoetcone20;   //!
   TBranch        *b_HGamElectronsAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamElectronsAuxDyn_truthType;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_e;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_eta;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_m;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_pt;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_px;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_py;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_pz;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_truthType;   //!
   TBranch        *b_HGamElectronsAuxDyn_truthLink;   //!
   TBranch        *b_HGamTruthElectronsAuxDyn_recoLink;   //!
   TBranch        *b_HGamMuonsAuxDyn_charge;   //!
   TBranch        *b_HGamMuonsAuxDyn_eta;   //!
   TBranch        *b_HGamMuonsAuxDyn_isBad;   //!
   TBranch        *b_HGamMuonsAuxDyn_muonType;   //!
   TBranch        *b_HGamMuonsAuxDyn_neflowisol20;   //!
   TBranch        *b_HGamMuonsAuxDyn_passIPCut;   //!
   TBranch        *b_HGamMuonsAuxDyn_phi;   //!
   TBranch        *b_HGamMuonsAuxDyn_pt;   //!
   TBranch        *b_HGamMuonsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500;   //!
   TBranch        *b_HGamMuonsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;   //!
   TBranch        *b_HGamMuonsAuxDyn_scaleFactor;   //!
   TBranch        *b_HGamMuonsAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamMuonsAuxDyn_truthType;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_e;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_eta;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_m;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_pt;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_px;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_py;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_pz;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamTruthMuonsAuxDyn_truthType;   //!
   TBranch        *b_HGamTauJetsAuxDyn_RNNEleScore;   //!
   TBranch        *b_HGamTauJetsAuxDyn_RNNEleScoreSigTrans;   //!
   TBranch        *b_HGamTauJetsAuxDyn_RNNJetScore;   //!
   TBranch        *b_HGamTauJetsAuxDyn_RNNJetScoreSigTrans;   //!
   TBranch        *b_HGamTauJetsAuxDyn_TauEffSF_EleRNNLoose;   //!
   TBranch        *b_HGamTauJetsAuxDyn_TauEffSF_EleRNNMedium;   //!
   TBranch        *b_HGamTauJetsAuxDyn_TauEffSF_JetIDRNNLoose;   //!
   TBranch        *b_HGamTauJetsAuxDyn_TauEffSF_JetIDRNNMedium;   //!
   TBranch        *b_HGamTauJetsAuxDyn_TauEffSF_JetIDRNNTight;   //!
   TBranch        *b_HGamTauJetsAuxDyn_TauEffSF_Reco;   //!
   TBranch        *b_HGamTauJetsAuxDyn_charge;   //!
   TBranch        *b_HGamTauJetsAuxDyn_classifierType;   //!
   TBranch        *b_HGamTauJetsAuxDyn_eta;   //!
   TBranch        *b_HGamTauJetsAuxDyn_isHadronicTau;   //!
   TBranch        *b_HGamTauJetsAuxDyn_isTauFlags;   //!
   TBranch        *b_HGamTauJetsAuxDyn_isTruthJet;   //!
   TBranch        *b_HGamTauJetsAuxDyn_nTracks;   //!
   TBranch        *b_HGamTauJetsAuxDyn_overlapJet;   //!
   TBranch        *b_HGamTauJetsAuxDyn_phi;   //!
   TBranch        *b_HGamTauJetsAuxDyn_pt;   //!
   TBranch        *b_HGamTauJetsAuxDyn_truthMatchID;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_HiggsHF_N_bjets;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_HiggsHF_N_cjets;   //!
   TBranch        *b_HGamTruthTausAuxDyn_e;   //!
   TBranch        *b_HGamTruthTausAuxDyn_eta;   //!
   TBranch        *b_HGamTruthTausAuxDyn_pt;   //!
   TBranch        *b_HGamTruthTausAuxDyn_px;   //!
   TBranch        *b_HGamTruthTausAuxDyn_py;   //!
   TBranch        *b_HGamTruthTausAuxDyn_pz;   //!
   TBranch        *b_HGamTruthTausAuxDyn_truthOrigin;   //!
   TBranch        *b_HGamTruthTausAuxDyn_truthType;   //!
   TBranch        *b_HGamTruthTausAuxDyn_recoLink;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_m_yy_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_SelectedForwardJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_SelectedTaggedJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_VLQmass;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_truth_label_forwardjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_truth_label_taggedjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_weight;   //!

   h029_MxAOD(TTree *tree=0);
   virtual ~h029_MxAOD();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef h029_MxAOD_cxx
h029_MxAOD::h029_MxAOD(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21a/Nominal/mc21a.PhPy8EG_PDF4LHC21_WpH125J_Wincl_MINLO.MxAODDetailedNoSkim.e8472_s3873_r13829_p5441_h029.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h029/mc21a/Nominal/mc21a.PhPy8EG_PDF4LHC21_WpH125J_Wincl_MINLO.MxAODDetailedNoSkim.e8472_s3873_r13829_p5441_h029.root");
      }
      f->GetObject("CollectionTree",tree);

   }
   Init(tree);
}

h029_MxAOD::~h029_MxAOD()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t h029_MxAOD::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t h029_MxAOD::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void h029_MxAOD::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   HGamPhotons = 0;
   HGamPhotonsAux_ = 0;
   HGamPhotonsAuxDyn_DeltaE = 0;
   HGamPhotonsAuxDyn_Eratio = 0;
   HGamPhotonsAuxDyn_Rconv = 0;
   HGamPhotonsAuxDyn_Reta = 0;
   HGamPhotonsAuxDyn_Rhad = 0;
   HGamPhotonsAuxDyn_Rhad1 = 0;
   HGamPhotonsAuxDyn_Rphi = 0;
   HGamPhotonsAuxDyn_author = 0;
   HGamPhotonsAuxDyn_cl_E = 0;
   HGamPhotonsAuxDyn_cl_E_TileGap3 = 0;
   HGamPhotonsAuxDyn_cl_Es0 = 0;
   HGamPhotonsAuxDyn_cl_Es1 = 0;
   HGamPhotonsAuxDyn_cl_Es2 = 0;
   HGamPhotonsAuxDyn_cl_Es3 = 0;
   HGamPhotonsAuxDyn_cl_eta = 0;
   HGamPhotonsAuxDyn_cl_etaCalo = 0;
   HGamPhotonsAuxDyn_cl_phiCalo = 0;
   HGamPhotonsAuxDyn_cl_ratioEs1Es2 = 0;
   HGamPhotonsAuxDyn_conversionType = 0;
   HGamPhotonsAuxDyn_convtrk1nPixHits = 0;
   HGamPhotonsAuxDyn_convtrk1nSCTHits = 0;
   HGamPhotonsAuxDyn_convtrk2nPixHits = 0;
   HGamPhotonsAuxDyn_convtrk2nSCTHits = 0;
   HGamPhotonsAuxDyn_e277 = 0;
   HGamPhotonsAuxDyn_eta = 0;
   HGamPhotonsAuxDyn_eta_s1 = 0;
   HGamPhotonsAuxDyn_eta_s2 = 0;
   HGamPhotonsAuxDyn_f1 = 0;
   HGamPhotonsAuxDyn_fracs1 = 0;
   HGamPhotonsAuxDyn_isEMTight = 0;
   HGamPhotonsAuxDyn_isIsoFixedCutLoose = 0;
   HGamPhotonsAuxDyn_isIsoFixedCutTight = 0;
   HGamPhotonsAuxDyn_isIsoTightCaloOnly = 0;
   HGamPhotonsAuxDyn_isTight = 0;
   HGamPhotonsAuxDyn_isTight_nofudge = 0;
   HGamPhotonsAuxDyn_m = 0;
   HGamPhotonsAuxDyn_maxEcell_energy = 0;
   HGamPhotonsAuxDyn_maxEcell_gain = 0;
   HGamPhotonsAuxDyn_maxEcell_onlId = 0;
   HGamPhotonsAuxDyn_maxEcell_time = 0;
   HGamPhotonsAuxDyn_parentPdgId = 0;
   HGamPhotonsAuxDyn_pdgId = 0;
   HGamPhotonsAuxDyn_phi = 0;
   HGamPhotonsAuxDyn_pt = 0;
   HGamPhotonsAuxDyn_pt1conv = 0;
   HGamPhotonsAuxDyn_pt2conv = 0;
   HGamPhotonsAuxDyn_pt_s2 = 0;
   HGamPhotonsAuxDyn_ptcone20 = 0;
   HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000 = 0;
   HGamPhotonsAuxDyn_ptconv = 0;
   HGamPhotonsAuxDyn_rawcl_Es0 = 0;
   HGamPhotonsAuxDyn_rawcl_Es1 = 0;
   HGamPhotonsAuxDyn_rawcl_Es2 = 0;
   HGamPhotonsAuxDyn_rawcl_Es3 = 0;
   HGamPhotonsAuxDyn_rawcl_ratioEs1Es2 = 0;
   HGamPhotonsAuxDyn_relEreso = 0;
   HGamPhotonsAuxDyn_scaleFactor = 0;
   HGamPhotonsAuxDyn_topoetcone20 = 0;
   HGamPhotonsAuxDyn_topoetcone20_DDcorr = 0;
   HGamPhotonsAuxDyn_topoetcone40 = 0;
   HGamPhotonsAuxDyn_topoetcone40_DDcorr = 0;
   HGamPhotonsAuxDyn_truthOrigin = 0;
   HGamPhotonsAuxDyn_truthRconv = 0;
   HGamPhotonsAuxDyn_truthType = 0;
   HGamPhotonsAuxDyn_weta1 = 0;
   HGamPhotonsAuxDyn_weta2 = 0;
   HGamPhotonsAuxDyn_wtots1 = 0;
   HGamPhotonsAuxDyn_zconv = 0;
   HGamPhotonsAuxDyn_zvertex = 0;
   HGamElectrons = 0;
   HGamElectronsAux_ = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtx = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAux_ = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pb = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pc = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pu = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pb = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pc = 0;
   BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pu = 0;
   HGamAntiKt4EMPFlowCustomVtxJets = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAux_ = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_CorrJvf = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DFCommonJets_fJvt = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_bin = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pb = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pc = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pu = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DetectorEta = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_bin = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pb = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pc = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pu = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_HadronConeExclTruthLabelID = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Jvt = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_NNRegPtSF = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_eta = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_m = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_phi = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_pt = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PartonTruthLabelID = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PtRecoSF = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Rpt = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_fjvt = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_jvt = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Timing = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_eta = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtHS = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtPU = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_m = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_overlapTau = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_passFJVT = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_phi = 0;
   HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_pt = 0;
   BTagging_HGamAntiKt4EMPFlow = 0;
   BTagging_HGamAntiKt4EMPFlowAux_ = 0;
   BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pb = 0;
   BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pc = 0;
   BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pu = 0;
   BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pb = 0;
   BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pc = 0;
   BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pu = 0;
   HGamAntiKt4EMPFlowJets = 0;
   HGamAntiKt4EMPFlowJetsAux_ = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DFCommonJets_fJvt = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_bin = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pb = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pc = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pu = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_DetectorEta = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_bin = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pb = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pc = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pu = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_HadronConeExclTruthLabelID = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_Jvt = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_SF_jvt = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_eta = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_isJvtHS = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_isJvtPU = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_m = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_overlapTau = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_passFJVT = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_phi = 0;
   HGamAntiKt4EMPFlowJetsAuxDyn_pt = 0;
   HGamMuons = 0;
   HGamMuonsAux_ = 0;
   HGamTauJets = 0;
   HGamTauJetsAux_ = 0;
   HGamMET_Reference_AntiKt4EMPFlow = 0;
   HGamMET_Reference_AntiKt4EMPFlowAux_ = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet = 0;
   HGamEventInfo = 0;
   HGamEventInfoAux_ = 0;
   HGamEventInfoAuxDyn_Hc_jet_truthlabel = 0;
   HGamEventInfoAuxDyn_Hc_y1_j_dr = 0;
   HGamEventInfoAuxDyn_Hc_y2_j_dr = 0;
   HGamEventInfoAuxDyn_HggPrimaryVerticesScore = 0;
   HGamEventInfoAuxDyn_HggPrimaryVerticesZ = 0;
   HGamEventInfoAuxDyn_fcnc_Score = 0;
   HGamEventInfoAuxDyn_idx_jets_recotop1 = 0;
   HGamEventInfoAuxDyn_idx_jets_recotop2 = 0;
   HGamTruthPhotons = 0;
   HGamTruthPhotonsAux_ = 0;
   HGamTruthPhotonsAuxDyn_e = 0;
   HGamTruthPhotonsAuxDyn_eta = 0;
   HGamTruthPhotonsAuxDyn_etcone20 = 0;
   HGamTruthPhotonsAuxDyn_etcone40 = 0;
   HGamTruthPhotonsAuxDyn_isIsolated = 0;
   HGamTruthPhotonsAuxDyn_m = 0;
   HGamTruthPhotonsAuxDyn_partonetcone20 = 0;
   HGamTruthPhotonsAuxDyn_partonetcone40 = 0;
   HGamTruthPhotonsAuxDyn_pt = 0;
   HGamTruthPhotonsAuxDyn_ptcone20 = 0;
   HGamTruthPhotonsAuxDyn_ptcone40 = 0;
   HGamTruthPhotonsAuxDyn_px = 0;
   HGamTruthPhotonsAuxDyn_py = 0;
   HGamTruthPhotonsAuxDyn_pz = 0;
   HGamTruthPhotonsAuxDyn_truthOrigin = 0;
   HGamTruthPhotonsAuxDyn_truthType = 0;
   HGamTruthElectrons = 0;
   HGamTruthElectronsAux_ = 0;
   HGamTruthMuons = 0;
   HGamTruthMuonsAux_ = 0;
   HGamTruthTaus = 0;
   HGamTruthTausAux_ = 0;
   HGamAntiKt4TruthWZJets = 0;
   HGamAntiKt4TruthWZJetsAux_ = 0;
   HGamAntiKt4TruthWZJetsAuxDyn_HadronConeExclTruthLabelID = 0;
   HGamAntiKt4TruthWZJetsAuxDyn_constituentLinks = 0;
   HGamAntiKt4TruthWZJetsAuxDyn_eta = 0;
   HGamAntiKt4TruthWZJetsAuxDyn_m = 0;
   HGamAntiKt4TruthWZJetsAuxDyn_phi = 0;
   HGamAntiKt4TruthWZJetsAuxDyn_pt = 0;
   HGamMET_Truth = 0;
   HGamMET_TruthAux_ = 0;
   HGamMET_TruthAuxDyn_mpx = 0;
   HGamMET_TruthAuxDyn_mpy = 0;
   HGamMET_TruthAuxDyn_name = 0;
   HGamMET_TruthAuxDyn_source = 0;
   HGamMET_TruthAuxDyn_sumet = 0;
   HGamTruthHiggsBosons = 0;
   HGamTruthHiggsBosonsAux_ = 0;
   HGamTruthHiggsBosonsAuxDyn_e = 0;
   HGamTruthHiggsBosonsAuxDyn_m = 0;
   HGamTruthHiggsBosonsAuxDyn_pt = 0;
   HGamTruthHiggsBosonsAuxDyn_px = 0;
   HGamTruthHiggsBosonsAuxDyn_py = 0;
   HGamTruthHiggsBosonsAuxDyn_pz = 0;
   TruthEvents = 0;
   TruthEventsAux_ = 0;
   TruthEventsAuxDyn_PDFID1 = 0;
   TruthEventsAuxDyn_PDFID2 = 0;
   TruthEventsAuxDyn_PDGID1 = 0;
   TruthEventsAuxDyn_PDGID2 = 0;
   TruthEventsAuxDyn_Q = 0;
   TruthEventsAuxDyn_X1 = 0;
   TruthEventsAuxDyn_X2 = 0;
   TruthEventsAuxDyn_XF1 = 0;
   TruthEventsAuxDyn_XF2 = 0;
   TruthEventsAuxDyn_weights = 0;
   HGamTruthEventInfo = 0;
   HGamTruthEventInfoAux_ = 0;
   EventInfo = 0;
   EventInfoAux_ = 0;
   EventInfoAuxDyn_HTXS_V_jets30_eta = 0;
   EventInfoAuxDyn_HTXS_V_jets30_m = 0;
   EventInfoAuxDyn_HTXS_V_jets30_phi = 0;
   EventInfoAuxDyn_HTXS_V_jets30_pt = 0;
   EventInfoAuxDyn_mcEventWeights = 0;
   HGamElectronsAuxDyn_author = 0;
   HGamElectronsAuxDyn_charge = 0;
   HGamElectronsAuxDyn_eta = 0;
   HGamElectronsAuxDyn_eta_s2 = 0;
   HGamElectronsAuxDyn_isTight = 0;
   HGamElectronsAuxDyn_m = 0;
   HGamElectronsAuxDyn_phi = 0;
   HGamElectronsAuxDyn_pt = 0;
   HGamElectronsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000 = 0;
   HGamElectronsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000 = 0;
   HGamElectronsAuxDyn_scaleFactor = 0;
   HGamElectronsAuxDyn_topoetcone20 = 0;
   HGamElectronsAuxDyn_truthOrigin = 0;
   HGamElectronsAuxDyn_truthType = 0;
   HGamTruthElectronsAuxDyn_e = 0;
   HGamTruthElectronsAuxDyn_eta = 0;
   HGamTruthElectronsAuxDyn_m = 0;
   HGamTruthElectronsAuxDyn_pt = 0;
   HGamTruthElectronsAuxDyn_px = 0;
   HGamTruthElectronsAuxDyn_py = 0;
   HGamTruthElectronsAuxDyn_pz = 0;
   HGamTruthElectronsAuxDyn_truthOrigin = 0;
   HGamTruthElectronsAuxDyn_truthType = 0;
   HGamElectronsAuxDyn_truthLink = 0;
   HGamTruthElectronsAuxDyn_recoLink = 0;
   HGamMuonsAuxDyn_charge = 0;
   HGamMuonsAuxDyn_eta = 0;
   HGamMuonsAuxDyn_isBad = 0;
   HGamMuonsAuxDyn_muonType = 0;
   HGamMuonsAuxDyn_neflowisol20 = 0;
   HGamMuonsAuxDyn_passIPCut = 0;
   HGamMuonsAuxDyn_phi = 0;
   HGamMuonsAuxDyn_pt = 0;
   HGamMuonsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500 = 0;
   HGamMuonsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500 = 0;
   HGamMuonsAuxDyn_scaleFactor = 0;
   HGamMuonsAuxDyn_truthOrigin = 0;
   HGamMuonsAuxDyn_truthType = 0;
   HGamTruthMuonsAuxDyn_e = 0;
   HGamTruthMuonsAuxDyn_eta = 0;
   HGamTruthMuonsAuxDyn_m = 0;
   HGamTruthMuonsAuxDyn_pt = 0;
   HGamTruthMuonsAuxDyn_px = 0;
   HGamTruthMuonsAuxDyn_py = 0;
   HGamTruthMuonsAuxDyn_pz = 0;
   HGamTruthMuonsAuxDyn_truthOrigin = 0;
   HGamTruthMuonsAuxDyn_truthType = 0;
   HGamTauJetsAuxDyn_RNNEleScore = 0;
   HGamTauJetsAuxDyn_RNNEleScoreSigTrans = 0;
   HGamTauJetsAuxDyn_RNNJetScore = 0;
   HGamTauJetsAuxDyn_RNNJetScoreSigTrans = 0;
   HGamTauJetsAuxDyn_TauEffSF_EleRNNLoose = 0;
   HGamTauJetsAuxDyn_TauEffSF_EleRNNMedium = 0;
   HGamTauJetsAuxDyn_TauEffSF_JetIDRNNLoose = 0;
   HGamTauJetsAuxDyn_TauEffSF_JetIDRNNMedium = 0;
   HGamTauJetsAuxDyn_TauEffSF_JetIDRNNTight = 0;
   HGamTauJetsAuxDyn_TauEffSF_Reco = 0;
   HGamTauJetsAuxDyn_charge = 0;
   HGamTauJetsAuxDyn_classifierType = 0;
   HGamTauJetsAuxDyn_eta = 0;
   HGamTauJetsAuxDyn_isHadronicTau = 0;
   HGamTauJetsAuxDyn_isTauFlags = 0;
   HGamTauJetsAuxDyn_isTruthJet = 0;
   HGamTauJetsAuxDyn_nTracks = 0;
   HGamTauJetsAuxDyn_overlapJet = 0;
   HGamTauJetsAuxDyn_phi = 0;
   HGamTauJetsAuxDyn_pt = 0;
   HGamTauJetsAuxDyn_truthMatchID = 0;
   HGamTruthTausAuxDyn_e = 0;
   HGamTruthTausAuxDyn_eta = 0;
   HGamTruthTausAuxDyn_pt = 0;
   HGamTruthTausAuxDyn_px = 0;
   HGamTruthTausAuxDyn_py = 0;
   HGamTruthTausAuxDyn_pz = 0;
   HGamTruthTausAuxDyn_truthOrigin = 0;
   HGamTruthTausAuxDyn_truthType = 0;
   HGamTruthTausAuxDyn_recoLink = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("HGamPhotons", &HGamPhotons, &b_HGamPhotons);
   fChain->SetBranchAddress("HGamPhotonsAux.", &HGamPhotonsAux_, &b_HGamPhotonsAux_);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.DeltaE", &HGamPhotonsAuxDyn_DeltaE, &b_HGamPhotonsAuxDyn_DeltaE);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Eratio", &HGamPhotonsAuxDyn_Eratio, &b_HGamPhotonsAuxDyn_Eratio);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rconv", &HGamPhotonsAuxDyn_Rconv, &b_HGamPhotonsAuxDyn_Rconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Reta", &HGamPhotonsAuxDyn_Reta, &b_HGamPhotonsAuxDyn_Reta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rhad", &HGamPhotonsAuxDyn_Rhad, &b_HGamPhotonsAuxDyn_Rhad);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rhad1", &HGamPhotonsAuxDyn_Rhad1, &b_HGamPhotonsAuxDyn_Rhad1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rphi", &HGamPhotonsAuxDyn_Rphi, &b_HGamPhotonsAuxDyn_Rphi);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.author", &HGamPhotonsAuxDyn_author, &b_HGamPhotonsAuxDyn_author);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_E", &HGamPhotonsAuxDyn_cl_E, &b_HGamPhotonsAuxDyn_cl_E);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_E_TileGap3", &HGamPhotonsAuxDyn_cl_E_TileGap3, &b_HGamPhotonsAuxDyn_cl_E_TileGap3);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es0", &HGamPhotonsAuxDyn_cl_Es0, &b_HGamPhotonsAuxDyn_cl_Es0);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es1", &HGamPhotonsAuxDyn_cl_Es1, &b_HGamPhotonsAuxDyn_cl_Es1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es2", &HGamPhotonsAuxDyn_cl_Es2, &b_HGamPhotonsAuxDyn_cl_Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es3", &HGamPhotonsAuxDyn_cl_Es3, &b_HGamPhotonsAuxDyn_cl_Es3);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_eta", &HGamPhotonsAuxDyn_cl_eta, &b_HGamPhotonsAuxDyn_cl_eta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_etaCalo", &HGamPhotonsAuxDyn_cl_etaCalo, &b_HGamPhotonsAuxDyn_cl_etaCalo);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_phiCalo", &HGamPhotonsAuxDyn_cl_phiCalo, &b_HGamPhotonsAuxDyn_cl_phiCalo);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_ratioEs1Es2", &HGamPhotonsAuxDyn_cl_ratioEs1Es2, &b_HGamPhotonsAuxDyn_cl_ratioEs1Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.conversionType", &HGamPhotonsAuxDyn_conversionType, &b_HGamPhotonsAuxDyn_conversionType);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk1nPixHits", &HGamPhotonsAuxDyn_convtrk1nPixHits, &b_HGamPhotonsAuxDyn_convtrk1nPixHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk1nSCTHits", &HGamPhotonsAuxDyn_convtrk1nSCTHits, &b_HGamPhotonsAuxDyn_convtrk1nSCTHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk2nPixHits", &HGamPhotonsAuxDyn_convtrk2nPixHits, &b_HGamPhotonsAuxDyn_convtrk2nPixHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk2nSCTHits", &HGamPhotonsAuxDyn_convtrk2nSCTHits, &b_HGamPhotonsAuxDyn_convtrk2nSCTHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.e277", &HGamPhotonsAuxDyn_e277, &b_HGamPhotonsAuxDyn_e277);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.eta", &HGamPhotonsAuxDyn_eta, &b_HGamPhotonsAuxDyn_eta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.eta_s1", &HGamPhotonsAuxDyn_eta_s1, &b_HGamPhotonsAuxDyn_eta_s1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.eta_s2", &HGamPhotonsAuxDyn_eta_s2, &b_HGamPhotonsAuxDyn_eta_s2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.f1", &HGamPhotonsAuxDyn_f1, &b_HGamPhotonsAuxDyn_f1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.fracs1", &HGamPhotonsAuxDyn_fracs1, &b_HGamPhotonsAuxDyn_fracs1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isEMTight", &HGamPhotonsAuxDyn_isEMTight, &b_HGamPhotonsAuxDyn_isEMTight);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoFixedCutLoose", &HGamPhotonsAuxDyn_isIsoFixedCutLoose, &b_HGamPhotonsAuxDyn_isIsoFixedCutLoose);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoFixedCutTight", &HGamPhotonsAuxDyn_isIsoFixedCutTight, &b_HGamPhotonsAuxDyn_isIsoFixedCutTight);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoTightCaloOnly", &HGamPhotonsAuxDyn_isIsoTightCaloOnly, &b_HGamPhotonsAuxDyn_isIsoTightCaloOnly);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isTight", &HGamPhotonsAuxDyn_isTight, &b_HGamPhotonsAuxDyn_isTight);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isTight_nofudge", &HGamPhotonsAuxDyn_isTight_nofudge, &b_HGamPhotonsAuxDyn_isTight_nofudge);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.m", &HGamPhotonsAuxDyn_m, &b_HGamPhotonsAuxDyn_m);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_energy", &HGamPhotonsAuxDyn_maxEcell_energy, &b_HGamPhotonsAuxDyn_maxEcell_energy);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_gain", &HGamPhotonsAuxDyn_maxEcell_gain, &b_HGamPhotonsAuxDyn_maxEcell_gain);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_onlId", &HGamPhotonsAuxDyn_maxEcell_onlId, &b_HGamPhotonsAuxDyn_maxEcell_onlId);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_time", &HGamPhotonsAuxDyn_maxEcell_time, &b_HGamPhotonsAuxDyn_maxEcell_time);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.parentPdgId", &HGamPhotonsAuxDyn_parentPdgId, &b_HGamPhotonsAuxDyn_parentPdgId);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pdgId", &HGamPhotonsAuxDyn_pdgId, &b_HGamPhotonsAuxDyn_pdgId);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.phi", &HGamPhotonsAuxDyn_phi, &b_HGamPhotonsAuxDyn_phi);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt", &HGamPhotonsAuxDyn_pt, &b_HGamPhotonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt1conv", &HGamPhotonsAuxDyn_pt1conv, &b_HGamPhotonsAuxDyn_pt1conv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt2conv", &HGamPhotonsAuxDyn_pt2conv, &b_HGamPhotonsAuxDyn_pt2conv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt_s2", &HGamPhotonsAuxDyn_pt_s2, &b_HGamPhotonsAuxDyn_pt_s2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptcone20", &HGamPhotonsAuxDyn_ptcone20, &b_HGamPhotonsAuxDyn_ptcone20);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptcone20_TightTTVA_pt1000", &HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000, &b_HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptconv", &HGamPhotonsAuxDyn_ptconv, &b_HGamPhotonsAuxDyn_ptconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es0", &HGamPhotonsAuxDyn_rawcl_Es0, &b_HGamPhotonsAuxDyn_rawcl_Es0);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es1", &HGamPhotonsAuxDyn_rawcl_Es1, &b_HGamPhotonsAuxDyn_rawcl_Es1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es2", &HGamPhotonsAuxDyn_rawcl_Es2, &b_HGamPhotonsAuxDyn_rawcl_Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es3", &HGamPhotonsAuxDyn_rawcl_Es3, &b_HGamPhotonsAuxDyn_rawcl_Es3);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_ratioEs1Es2", &HGamPhotonsAuxDyn_rawcl_ratioEs1Es2, &b_HGamPhotonsAuxDyn_rawcl_ratioEs1Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.relEreso", &HGamPhotonsAuxDyn_relEreso, &b_HGamPhotonsAuxDyn_relEreso);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.scaleFactor", &HGamPhotonsAuxDyn_scaleFactor, &b_HGamPhotonsAuxDyn_scaleFactor);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone20", &HGamPhotonsAuxDyn_topoetcone20, &b_HGamPhotonsAuxDyn_topoetcone20);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone20_DDcorr", &HGamPhotonsAuxDyn_topoetcone20_DDcorr, &b_HGamPhotonsAuxDyn_topoetcone20_DDcorr);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone40", &HGamPhotonsAuxDyn_topoetcone40, &b_HGamPhotonsAuxDyn_topoetcone40);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone40_DDcorr", &HGamPhotonsAuxDyn_topoetcone40_DDcorr, &b_HGamPhotonsAuxDyn_topoetcone40_DDcorr);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthLink", &HGamPhotonsAuxDyn_truthLink_, &b_HGamPhotonsAuxDyn_truthLink_);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthLink.m_persKey", HGamPhotonsAuxDyn_truthLink_m_persKey, &b_HGamPhotonsAuxDyn_truthLink_m_persKey);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthLink.m_persIndex", HGamPhotonsAuxDyn_truthLink_m_persIndex, &b_HGamPhotonsAuxDyn_truthLink_m_persIndex);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthOrigin", &HGamPhotonsAuxDyn_truthOrigin, &b_HGamPhotonsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthRconv", &HGamPhotonsAuxDyn_truthRconv, &b_HGamPhotonsAuxDyn_truthRconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthType", &HGamPhotonsAuxDyn_truthType, &b_HGamPhotonsAuxDyn_truthType);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.weta1", &HGamPhotonsAuxDyn_weta1, &b_HGamPhotonsAuxDyn_weta1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.weta2", &HGamPhotonsAuxDyn_weta2, &b_HGamPhotonsAuxDyn_weta2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.wtots1", &HGamPhotonsAuxDyn_wtots1, &b_HGamPhotonsAuxDyn_wtots1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.zconv", &HGamPhotonsAuxDyn_zconv, &b_HGamPhotonsAuxDyn_zconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.zvertex", &HGamPhotonsAuxDyn_zvertex, &b_HGamPhotonsAuxDyn_zvertex);
   fChain->SetBranchAddress("HGamElectrons", &HGamElectrons, &b_HGamElectrons);
   fChain->SetBranchAddress("HGamElectronsAux.", &HGamElectronsAux_, &b_HGamElectronsAux_);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtx", &BTagging_HGamAntiKt4EMPFlowCustomVtx, &b_BTagging_HGamAntiKt4EMPFlowCustomVtx);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAux.", &BTagging_HGamAntiKt4EMPFlowCustomVtxAux_, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAux_);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn.DL1dv01_pb", &BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pb, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn.DL1dv01_pc", &BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pc, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn.DL1dv01_pu", &BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pu, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_DL1dv01_pu);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn.GN120220509_pb", &BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pb, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn.GN120220509_pc", &BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pc, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn.GN120220509_pu", &BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pu, &b_BTagging_HGamAntiKt4EMPFlowCustomVtxAuxDyn_GN120220509_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJets", &HGamAntiKt4EMPFlowCustomVtxJets, &b_HGamAntiKt4EMPFlowCustomVtxJets);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAux.", &HGamAntiKt4EMPFlowCustomVtxJetsAux_, &b_HGamAntiKt4EMPFlowCustomVtxJetsAux_);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.CorrJvf", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_CorrJvf, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_CorrJvf);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DFCommonJets_fJvt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DFCommonJets_fJvt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DFCommonJets_fJvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_FixedCutBEff_60", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_FixedCutBEff_70", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_FixedCutBEff_77", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_FixedCutBEff_85", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_bin", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_bin, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_bin);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_pb", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pb, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pb);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_pc", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pc, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pc);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DL1dv01_pu", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pu, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DL1dv01_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.DetectorEta", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DetectorEta, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_DetectorEta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_60", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_70", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_77", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_85", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_GN120220509_FixedCutBEff_60", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_GN120220509_FixedCutBEff_70", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_GN120220509_FixedCutBEff_77", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Eff_GN120220509_FixedCutBEff_85", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_FixedCutBEff_60", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_FixedCutBEff_70", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_FixedCutBEff_77", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_FixedCutBEff_85", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_bin", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_bin, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_bin);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_pb", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pb, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pb);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_pc", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pc, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pc);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.GN120220509_pu", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pu, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_GN120220509_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.HadronConeExclTruthLabelID", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_HadronConeExclTruthLabelID, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Jvt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Jvt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Jvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.NNRegPtSF", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_NNRegPtSF, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_NNRegPtSF);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.OneMu_eta", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_eta, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_eta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.OneMu_m", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_m, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_m);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.OneMu_phi", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_phi, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_phi);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.OneMu_pt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_pt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_OneMu_pt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.PartonTruthLabelID", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PartonTruthLabelID, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PartonTruthLabelID);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.PtRecoSF", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PtRecoSF, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_PtRecoSF);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Rpt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Rpt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Rpt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_DL1dv01_FixedCutBEff_60", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_DL1dv01_FixedCutBEff_70", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_DL1dv01_FixedCutBEff_77", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_DL1dv01_FixedCutBEff_85", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_GN120220509_FixedCutBEff_60", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_GN120220509_FixedCutBEff_70", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_GN120220509_FixedCutBEff_77", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_GN120220509_FixedCutBEff_85", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_fjvt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_fjvt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_fjvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.SF_jvt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_jvt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_SF_jvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.Timing", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Timing, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_Timing);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.btaggingLink", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.btaggingLink.m_persKey", HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persKey, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persKey);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.btaggingLink.m_persIndex", HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persIndex, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_btaggingLink_m_persIndex);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.eta", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_eta, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_eta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.isJvtHS", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtHS, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtHS);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.isJvtPU", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtPU, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_isJvtPU);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.m", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_m, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_m);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.overlapTau", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_overlapTau, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_overlapTau);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.passFJVT", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_passFJVT, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_passFJVT);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.phi", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_phi, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_phi);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn.pt", &HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_pt, &b_HGamAntiKt4EMPFlowCustomVtxJetsAuxDyn_pt);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow", &BTagging_HGamAntiKt4EMPFlow, &b_BTagging_HGamAntiKt4EMPFlow);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAux.", &BTagging_HGamAntiKt4EMPFlowAux_, &b_BTagging_HGamAntiKt4EMPFlowAux_);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAuxDyn.DL1dv01_pb", &BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pb, &b_BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAuxDyn.DL1dv01_pc", &BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pc, &b_BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAuxDyn.DL1dv01_pu", &BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pu, &b_BTagging_HGamAntiKt4EMPFlowAuxDyn_DL1dv01_pu);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAuxDyn.GN120220509_pb", &BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pb, &b_BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAuxDyn.GN120220509_pc", &BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pc, &b_BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlowAuxDyn.GN120220509_pu", &BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pu, &b_BTagging_HGamAntiKt4EMPFlowAuxDyn_GN120220509_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets", &HGamAntiKt4EMPFlowJets, &b_HGamAntiKt4EMPFlowJets);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAux.", &HGamAntiKt4EMPFlowJetsAux_, &b_HGamAntiKt4EMPFlowJetsAux_);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DFCommonJets_fJvt", &HGamAntiKt4EMPFlowJetsAuxDyn_DFCommonJets_fJvt, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DFCommonJets_fJvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_FixedCutBEff_60", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_FixedCutBEff_70", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_FixedCutBEff_77", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_FixedCutBEff_85", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_bin", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_bin, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_bin);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_pb", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pb, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pb);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_pc", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pc, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pc);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DL1dv01_pu", &HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pu, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DL1dv01_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.DetectorEta", &HGamAntiKt4EMPFlowJetsAuxDyn_DetectorEta, &b_HGamAntiKt4EMPFlowJetsAuxDyn_DetectorEta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_60", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_70", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_77", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_DL1dv01_FixedCutBEff_85", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_GN120220509_FixedCutBEff_60", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_GN120220509_FixedCutBEff_70", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_GN120220509_FixedCutBEff_77", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Eff_GN120220509_FixedCutBEff_85", &HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Eff_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_FixedCutBEff_60", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_FixedCutBEff_70", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_FixedCutBEff_77", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_FixedCutBEff_85", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_bin", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_bin, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_bin);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_pb", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pb, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pb);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_pc", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pc, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pc);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.GN120220509_pu", &HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pu, &b_HGamAntiKt4EMPFlowJetsAuxDyn_GN120220509_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.HadronConeExclTruthLabelID", &HGamAntiKt4EMPFlowJetsAuxDyn_HadronConeExclTruthLabelID, &b_HGamAntiKt4EMPFlowJetsAuxDyn_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.Jvt", &HGamAntiKt4EMPFlowJetsAuxDyn_Jvt, &b_HGamAntiKt4EMPFlowJetsAuxDyn_Jvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_DL1dv01_FixedCutBEff_60", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_DL1dv01_FixedCutBEff_70", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_DL1dv01_FixedCutBEff_77", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_DL1dv01_FixedCutBEff_85", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_GN120220509_FixedCutBEff_60", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_GN120220509_FixedCutBEff_70", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_GN120220509_FixedCutBEff_77", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_GN120220509_FixedCutBEff_85", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.SF_jvt", &HGamAntiKt4EMPFlowJetsAuxDyn_SF_jvt, &b_HGamAntiKt4EMPFlowJetsAuxDyn_SF_jvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.btaggingLink", &HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_, &b_HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.btaggingLink.m_persKey", HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persKey, &b_HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persKey);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.btaggingLink.m_persIndex", HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persIndex, &b_HGamAntiKt4EMPFlowJetsAuxDyn_btaggingLink_m_persIndex);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.eta", &HGamAntiKt4EMPFlowJetsAuxDyn_eta, &b_HGamAntiKt4EMPFlowJetsAuxDyn_eta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.isJvtHS", &HGamAntiKt4EMPFlowJetsAuxDyn_isJvtHS, &b_HGamAntiKt4EMPFlowJetsAuxDyn_isJvtHS);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.isJvtPU", &HGamAntiKt4EMPFlowJetsAuxDyn_isJvtPU, &b_HGamAntiKt4EMPFlowJetsAuxDyn_isJvtPU);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.m", &HGamAntiKt4EMPFlowJetsAuxDyn_m, &b_HGamAntiKt4EMPFlowJetsAuxDyn_m);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.overlapTau", &HGamAntiKt4EMPFlowJetsAuxDyn_overlapTau, &b_HGamAntiKt4EMPFlowJetsAuxDyn_overlapTau);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.passFJVT", &HGamAntiKt4EMPFlowJetsAuxDyn_passFJVT, &b_HGamAntiKt4EMPFlowJetsAuxDyn_passFJVT);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.phi", &HGamAntiKt4EMPFlowJetsAuxDyn_phi, &b_HGamAntiKt4EMPFlowJetsAuxDyn_phi);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJetsAuxDyn.pt", &HGamAntiKt4EMPFlowJetsAuxDyn_pt, &b_HGamAntiKt4EMPFlowJetsAuxDyn_pt);
   fChain->SetBranchAddress("HGamMuons", &HGamMuons, &b_HGamMuons);
   fChain->SetBranchAddress("HGamMuonsAux.", &HGamMuonsAux_, &b_HGamMuonsAux_);
   fChain->SetBranchAddress("HGamTauJets", &HGamTauJets, &b_HGamTauJets);
   fChain->SetBranchAddress("HGamTauJetsAux.", &HGamTauJetsAux_, &b_HGamTauJetsAux_);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlow", &HGamMET_Reference_AntiKt4EMPFlow, &b_HGamMET_Reference_AntiKt4EMPFlow);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAux.", &HGamMET_Reference_AntiKt4EMPFlowAux_, &b_HGamMET_Reference_AntiKt4EMPFlowAux_);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.mpx", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.mpy", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.name", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.source", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.sumet", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet);
   fChain->SetBranchAddress("HGamEventInfo", &HGamEventInfo, &b_HGamEventInfo);
   fChain->SetBranchAddress("HGamEventInfoAux.", &HGamEventInfoAux_, &b_HGamEventInfoAux_);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.DR_y_y", &HGamEventInfoAuxDyn_DR_y_y, &b_HGamEventInfoAuxDyn_DR_y_y);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.DRmin_y_j", &HGamEventInfoAuxDyn_DRmin_y_j, &b_HGamEventInfoAuxDyn_DRmin_y_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.DRmin_y_j_2", &HGamEventInfoAuxDyn_DRmin_y_j_2, &b_HGamEventInfoAuxDyn_DRmin_y_j_2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Deta_j_j", &HGamEventInfoAuxDyn_Deta_j_j, &b_HGamEventInfoAuxDyn_Deta_j_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j", &HGamEventInfoAuxDyn_Dphi_j_j, &b_HGamEventInfoAuxDyn_Dphi_j_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_30", &HGamEventInfoAuxDyn_Dphi_j_j_30, &b_HGamEventInfoAuxDyn_Dphi_j_j_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_30_signed", &HGamEventInfoAuxDyn_Dphi_j_j_30_signed, &b_HGamEventInfoAuxDyn_Dphi_j_j_30_signed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_50", &HGamEventInfoAuxDyn_Dphi_j_j_50, &b_HGamEventInfoAuxDyn_Dphi_j_j_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_50_signed", &HGamEventInfoAuxDyn_Dphi_j_j_50_signed, &b_HGamEventInfoAuxDyn_Dphi_j_j_50_signed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_y_y", &HGamEventInfoAuxDyn_Dphi_y_y, &b_HGamEventInfoAuxDyn_Dphi_y_y);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_yy_jj", &HGamEventInfoAuxDyn_Dphi_yy_jj, &b_HGamEventInfoAuxDyn_Dphi_yy_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_yy_jj_30", &HGamEventInfoAuxDyn_Dphi_yy_jj_30, &b_HGamEventInfoAuxDyn_Dphi_yy_jj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_yy_jj_50", &HGamEventInfoAuxDyn_Dphi_yy_jj_50, &b_HGamEventInfoAuxDyn_Dphi_yy_jj_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_j_j", &HGamEventInfoAuxDyn_Dy_j_j, &b_HGamEventInfoAuxDyn_Dy_j_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_j_j_30", &HGamEventInfoAuxDyn_Dy_j_j_30, &b_HGamEventInfoAuxDyn_Dy_j_j_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_y_y", &HGamEventInfoAuxDyn_Dy_y_y, &b_HGamEventInfoAuxDyn_Dy_y_y);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_yy_jj", &HGamEventInfoAuxDyn_Dy_yy_jj, &b_HGamEventInfoAuxDyn_Dy_yy_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_yy_jj_30", &HGamEventInfoAuxDyn_Dy_yy_jj_30, &b_HGamEventInfoAuxDyn_Dy_yy_jj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.E_y1", &HGamEventInfoAuxDyn_E_y1, &b_HGamEventInfoAuxDyn_E_y1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.E_y2", &HGamEventInfoAuxDyn_E_y2, &b_HGamEventInfoAuxDyn_E_y2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HT_30", &HGamEventInfoAuxDyn_HT_30, &b_HGamEventInfoAuxDyn_HT_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HTall_30", &HGamEventInfoAuxDyn_HTall_30, &b_HGamEventInfoAuxDyn_HTall_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_cutFlowAllJet", &HGamEventInfoAuxDyn_Hc_cutFlowAllJet, &b_HGamEventInfoAuxDyn_Hc_cutFlowAllJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_cutFlowLeadJet", &HGamEventInfoAuxDyn_Hc_cutFlowLeadJet, &b_HGamEventInfoAuxDyn_Hc_cutFlowLeadJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_jet_truthlabel", &HGamEventInfoAuxDyn_Hc_jet_truthlabel, &b_HGamEventInfoAuxDyn_Hc_jet_truthlabel);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_leadJet_truthLabel", &HGamEventInfoAuxDyn_Hc_leadJet_truthLabel, &b_HGamEventInfoAuxDyn_Hc_leadJet_truthLabel);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_m_yy_cat", &HGamEventInfoAuxDyn_Hc_m_yy_cat, &b_HGamEventInfoAuxDyn_Hc_m_yy_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_y1_j_dr", &HGamEventInfoAuxDyn_Hc_y1_j_dr, &b_HGamEventInfoAuxDyn_Hc_y1_j_dr);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_y2_j_dr", &HGamEventInfoAuxDyn_Hc_y2_j_dr, &b_HGamEventInfoAuxDyn_Hc_y2_j_dr);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HggPrimaryVerticesScore", &HGamEventInfoAuxDyn_HggPrimaryVerticesScore, &b_HGamEventInfoAuxDyn_HggPrimaryVerticesScore);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HggPrimaryVerticesZ", &HGamEventInfoAuxDyn_HggPrimaryVerticesZ, &b_HGamEventInfoAuxDyn_HggPrimaryVerticesZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_BDT", &HGamEventInfoAuxDyn_HiggsHF_BDT, &b_HGamEventInfoAuxDyn_HiggsHF_BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_BDT_cat", &HGamEventInfoAuxDyn_HiggsHF_BDT_cat, &b_HGamEventInfoAuxDyn_HiggsHF_BDT_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_cutFlow", &HGamEventInfoAuxDyn_HiggsHF_cutFlow, &b_HGamEventInfoAuxDyn_HiggsHF_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dEta_yy", &HGamEventInfoAuxDyn_HiggsHF_dEta_yy, &b_HGamEventInfoAuxDyn_HiggsHF_dEta_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dPhi_yy_leadBtagjet", &HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet, &b_HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y1_leadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y1_subleadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y2_leadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y2_subleadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_yy", &HGamEventInfoAuxDyn_HiggsHF_dR_yy, &b_HGamEventInfoAuxDyn_HiggsHF_dR_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_effmass_yyjets", &HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets, &b_HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_foxw1_yyjets", &HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets, &b_HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_leadBtagJet_btagbin", &HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin, &b_HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_leadBtagjet_eta", &HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta, &b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_leadBtagjet_pt", &HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt, &b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_truth_label_LeadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_weight", &HGamEventInfoAuxDyn_HiggsHF_weight, &b_HGamEventInfoAuxDyn_HiggsHF_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.NLoosePhotons", &HGamEventInfoAuxDyn_NLoosePhotons, &b_HGamEventInfoAuxDyn_NLoosePhotons);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_e", &HGamEventInfoAuxDyn_N_e, &b_HGamEventInfoAuxDyn_N_e);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j", &HGamEventInfoAuxDyn_N_j, &b_HGamEventInfoAuxDyn_N_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_30", &HGamEventInfoAuxDyn_N_j_30, &b_HGamEventInfoAuxDyn_N_j_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_50", &HGamEventInfoAuxDyn_N_j_50, &b_HGamEventInfoAuxDyn_N_j_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_btag", &HGamEventInfoAuxDyn_N_j_btag, &b_HGamEventInfoAuxDyn_N_j_btag);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_btag30", &HGamEventInfoAuxDyn_N_j_btag30, &b_HGamEventInfoAuxDyn_N_j_btag30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_central", &HGamEventInfoAuxDyn_N_j_central, &b_HGamEventInfoAuxDyn_N_j_central);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_central30", &HGamEventInfoAuxDyn_N_j_central30, &b_HGamEventInfoAuxDyn_N_j_central30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_lep", &HGamEventInfoAuxDyn_N_lep, &b_HGamEventInfoAuxDyn_N_lep);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_lep_15", &HGamEventInfoAuxDyn_N_lep_15, &b_HGamEventInfoAuxDyn_N_lep_15);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_mu", &HGamEventInfoAuxDyn_N_mu, &b_HGamEventInfoAuxDyn_N_mu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Zepp", &HGamEventInfoAuxDyn_Zepp, &b_HGamEventInfoAuxDyn_Zepp);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.beamSpotWeight", &HGamEventInfoAuxDyn_beamSpotWeight, &b_HGamEventInfoAuxDyn_beamSpotWeight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_GlobalICHEP", &HGamEventInfoAuxDyn_catCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_catCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_HybridICHEP", &HGamEventInfoAuxDyn_catCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_catCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_MonoH_2var", &HGamEventInfoAuxDyn_catCoup_MonoH_2var, &b_HGamEventInfoAuxDyn_catCoup_MonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_Moriond2017BDT", &HGamEventInfoAuxDyn_catCoup_Moriond2017BDT, &b_HGamEventInfoAuxDyn_catCoup_Moriond2017BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_XGBoost_ttH", &HGamEventInfoAuxDyn_catCoup_XGBoost_ttH, &b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_XGBoost_ttHCP", &HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP, &b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catLowHighMyy_conv", &HGamEventInfoAuxDyn_catLowHighMyy_conv, &b_HGamEventInfoAuxDyn_catLowHighMyy_conv);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_Run1", &HGamEventInfoAuxDyn_catMass_Run1, &b_HGamEventInfoAuxDyn_catMass_Run1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_conv", &HGamEventInfoAuxDyn_catMass_conv, &b_HGamEventInfoAuxDyn_catMass_conv);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_eta", &HGamEventInfoAuxDyn_catMass_eta, &b_HGamEventInfoAuxDyn_catMass_eta);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_mu", &HGamEventInfoAuxDyn_catMass_mu, &b_HGamEventInfoAuxDyn_catMass_mu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_pT", &HGamEventInfoAuxDyn_catMass_pT, &b_HGamEventInfoAuxDyn_catMass_pT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_HiggsHF", &HGamEventInfoAuxDyn_catXS_HiggsHF, &b_HGamEventInfoAuxDyn_catXS_HiggsHF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_VBF", &HGamEventInfoAuxDyn_catXS_VBF, &b_HGamEventInfoAuxDyn_catXS_VBF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_nbjet", &HGamEventInfoAuxDyn_catXS_nbjet, &b_HGamEventInfoAuxDyn_catXS_nbjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_ttH", &HGamEventInfoAuxDyn_catXS_ttH, &b_HGamEventInfoAuxDyn_catXS_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.cosTS_yy", &HGamEventInfoAuxDyn_cosTS_yy, &b_HGamEventInfoAuxDyn_cosTS_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.cosTS_yyjj", &HGamEventInfoAuxDyn_cosTS_yyjj, &b_HGamEventInfoAuxDyn_cosTS_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.crossSectionBRfilterEff", &HGamEventInfoAuxDyn_crossSectionBRfilterEff, &b_HGamEventInfoAuxDyn_crossSectionBRfilterEff);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.cutFlow", &HGamEventInfoAuxDyn_cutFlow, &b_HGamEventInfoAuxDyn_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.eta_hybridtop2", &HGamEventInfoAuxDyn_eta_hybridtop2, &b_HGamEventInfoAuxDyn_eta_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.eta_recotop1", &HGamEventInfoAuxDyn_eta_recotop1, &b_HGamEventInfoAuxDyn_eta_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_Score", &HGamEventInfoAuxDyn_fcnc_Score, &b_HGamEventInfoAuxDyn_fcnc_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_cat", &HGamEventInfoAuxDyn_fcnc_cat, &b_HGamEventInfoAuxDyn_fcnc_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_cutFlow", &HGamEventInfoAuxDyn_fcnc_cutFlow, &b_HGamEventInfoAuxDyn_fcnc_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_fJvtweight", &HGamEventInfoAuxDyn_fcnc_fJvtweight, &b_HGamEventInfoAuxDyn_fcnc_fJvtweight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_weight", &HGamEventInfoAuxDyn_fcnc_weight, &b_HGamEventInfoAuxDyn_fcnc_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.hardestVertexPhi", &HGamEventInfoAuxDyn_hardestVertexPhi, &b_HGamEventInfoAuxDyn_hardestVertexPhi);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.hardestVertexSumPt2", &HGamEventInfoAuxDyn_hardestVertexSumPt2, &b_HGamEventInfoAuxDyn_hardestVertexSumPt2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.hardestVertexZ", &HGamEventInfoAuxDyn_hardestVertexZ, &b_HGamEventInfoAuxDyn_hardestVertexZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.idx_jets_recotop1", &HGamEventInfoAuxDyn_idx_jets_recotop1, &b_HGamEventInfoAuxDyn_idx_jets_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.idx_jets_recotop2", &HGamEventInfoAuxDyn_idx_jets_recotop2, &b_HGamEventInfoAuxDyn_idx_jets_recotop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isDalitz", &HGamEventInfoAuxDyn_isDalitz, &b_HGamEventInfoAuxDyn_isDalitz);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassed", &HGamEventInfoAuxDyn_isPassed, &b_HGamEventInfoAuxDyn_isPassed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedBasic", &HGamEventInfoAuxDyn_isPassedBasic, &b_HGamEventInfoAuxDyn_isPassedBasic);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedHighMyy", &HGamEventInfoAuxDyn_isPassedHighMyy, &b_HGamEventInfoAuxDyn_isPassedHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedIsolation", &HGamEventInfoAuxDyn_isPassedIsolation, &b_HGamEventInfoAuxDyn_isPassedIsolation);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedIsolationHighMyy", &HGamEventInfoAuxDyn_isPassedIsolationHighMyy, &b_HGamEventInfoAuxDyn_isPassedIsolationHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedIsolationLowMyy", &HGamEventInfoAuxDyn_isPassedIsolationLowMyy, &b_HGamEventInfoAuxDyn_isPassedIsolationLowMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedJetEventClean", &HGamEventInfoAuxDyn_isPassedJetEventClean, &b_HGamEventInfoAuxDyn_isPassedJetEventClean);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedLowMyy", &HGamEventInfoAuxDyn_isPassedLowMyy, &b_HGamEventInfoAuxDyn_isPassedLowMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedMassCut", &HGamEventInfoAuxDyn_isPassedMassCut, &b_HGamEventInfoAuxDyn_isPassedMassCut);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedPID", &HGamEventInfoAuxDyn_isPassedPID, &b_HGamEventInfoAuxDyn_isPassedPID);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedPreselection", &HGamEventInfoAuxDyn_isPassedPreselection, &b_HGamEventInfoAuxDyn_isPassedPreselection);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedPtCutsLowHighMyy", &HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy, &b_HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedRelPtCuts", &HGamEventInfoAuxDyn_isPassedRelPtCuts, &b_HGamEventInfoAuxDyn_isPassedRelPtCuts);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedTrigMatchLowHighMyy", &HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy, &b_HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedTriggerMatch", &HGamEventInfoAuxDyn_isPassedTriggerMatch, &b_HGamEventInfoAuxDyn_isPassedTriggerMatch);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_alljet", &HGamEventInfoAuxDyn_m_alljet, &b_HGamEventInfoAuxDyn_m_alljet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_alljet_30", &HGamEventInfoAuxDyn_m_alljet_30, &b_HGamEventInfoAuxDyn_m_alljet_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_ee", &HGamEventInfoAuxDyn_m_ee, &b_HGamEventInfoAuxDyn_m_ee);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_hybridtop2", &HGamEventInfoAuxDyn_m_hybridtop2, &b_HGamEventInfoAuxDyn_m_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_jj", &HGamEventInfoAuxDyn_m_jj, &b_HGamEventInfoAuxDyn_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_jj_30", &HGamEventInfoAuxDyn_m_jj_30, &b_HGamEventInfoAuxDyn_m_jj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_jj_50", &HGamEventInfoAuxDyn_m_jj_50, &b_HGamEventInfoAuxDyn_m_jj_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_mumu", &HGamEventInfoAuxDyn_m_mumu, &b_HGamEventInfoAuxDyn_m_mumu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_recotop1", &HGamEventInfoAuxDyn_m_recotop1, &b_HGamEventInfoAuxDyn_m_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy", &HGamEventInfoAuxDyn_m_yy, &b_HGamEventInfoAuxDyn_m_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_hardestVertex", &HGamEventInfoAuxDyn_m_yy_hardestVertex, &b_HGamEventInfoAuxDyn_m_yy_hardestVertex);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_resolution", &HGamEventInfoAuxDyn_m_yy_resolution, &b_HGamEventInfoAuxDyn_m_yy_resolution);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_truthVertex", &HGamEventInfoAuxDyn_m_yy_truthVertex, &b_HGamEventInfoAuxDyn_m_yy_truthVertex);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_zCommon", &HGamEventInfoAuxDyn_m_yy_zCommon, &b_HGamEventInfoAuxDyn_m_yy_zCommon);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yyj", &HGamEventInfoAuxDyn_m_yyj, &b_HGamEventInfoAuxDyn_m_yyj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yyj_30", &HGamEventInfoAuxDyn_m_yyj_30, &b_HGamEventInfoAuxDyn_m_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yyjj", &HGamEventInfoAuxDyn_m_yyjj, &b_HGamEventInfoAuxDyn_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.massTrans", &HGamEventInfoAuxDyn_massTrans, &b_HGamEventInfoAuxDyn_massTrans);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.maxTau_yyj_30", &HGamEventInfoAuxDyn_maxTau_yyj_30, &b_HGamEventInfoAuxDyn_maxTau_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_TST", &HGamEventInfoAuxDyn_met_TST, &b_HGamEventInfoAuxDyn_met_TST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_cat", &HGamEventInfoAuxDyn_met_cat, &b_HGamEventInfoAuxDyn_met_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_hardVertexTST", &HGamEventInfoAuxDyn_met_hardVertexTST, &b_HGamEventInfoAuxDyn_met_hardVertexTST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_weight", &HGamEventInfoAuxDyn_met_weight, &b_HGamEventInfoAuxDyn_met_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.mu", &HGamEventInfoAuxDyn_mu, &b_HGamEventInfoAuxDyn_mu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.multiClassCatCoup_GlobalICHEP", &HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.multiClassCatCoup_HybridICHEP", &HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.numberOfPrimaryVertices", &HGamEventInfoAuxDyn_numberOfPrimaryVertices, &b_HGamEventInfoAuxDyn_numberOfPrimaryVertices);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_hard", &HGamEventInfoAuxDyn_pT_hard, &b_HGamEventInfoAuxDyn_pT_hard);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_hybridtop2", &HGamEventInfoAuxDyn_pT_hybridtop2, &b_HGamEventInfoAuxDyn_pT_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j1", &HGamEventInfoAuxDyn_pT_j1, &b_HGamEventInfoAuxDyn_pT_j1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j1_30", &HGamEventInfoAuxDyn_pT_j1_30, &b_HGamEventInfoAuxDyn_pT_j1_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j2", &HGamEventInfoAuxDyn_pT_j2, &b_HGamEventInfoAuxDyn_pT_j2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j2_30", &HGamEventInfoAuxDyn_pT_j2_30, &b_HGamEventInfoAuxDyn_pT_j2_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j3_30", &HGamEventInfoAuxDyn_pT_j3_30, &b_HGamEventInfoAuxDyn_pT_j3_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_jj", &HGamEventInfoAuxDyn_pT_jj, &b_HGamEventInfoAuxDyn_pT_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_recotop1", &HGamEventInfoAuxDyn_pT_recotop1, &b_HGamEventInfoAuxDyn_pT_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_y1", &HGamEventInfoAuxDyn_pT_y1, &b_HGamEventInfoAuxDyn_pT_y1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_y2", &HGamEventInfoAuxDyn_pT_y2, &b_HGamEventInfoAuxDyn_pT_y2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yy", &HGamEventInfoAuxDyn_pT_yy, &b_HGamEventInfoAuxDyn_pT_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyj", &HGamEventInfoAuxDyn_pT_yyj, &b_HGamEventInfoAuxDyn_pT_yyj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyj_30", &HGamEventInfoAuxDyn_pT_yyj_30, &b_HGamEventInfoAuxDyn_pT_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyjj", &HGamEventInfoAuxDyn_pT_yyjj, &b_HGamEventInfoAuxDyn_pT_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyjj_30", &HGamEventInfoAuxDyn_pT_yyjj_30, &b_HGamEventInfoAuxDyn_pT_yyjj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyjj_50", &HGamEventInfoAuxDyn_pT_yyjj_50, &b_HGamEventInfoAuxDyn_pT_yyjj_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pTlepMET", &HGamEventInfoAuxDyn_pTlepMET, &b_HGamEventInfoAuxDyn_pTlepMET);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pTt_yy", &HGamEventInfoAuxDyn_pTt_yy, &b_HGamEventInfoAuxDyn_pTt_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.passCrackVetoCleaning", &HGamEventInfoAuxDyn_passCrackVetoCleaning, &b_HGamEventInfoAuxDyn_passCrackVetoCleaning);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.passMeyCut", &HGamEventInfoAuxDyn_passMeyCut, &b_HGamEventInfoAuxDyn_passMeyCut);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phiStar_yy", &HGamEventInfoAuxDyn_phiStar_yy, &b_HGamEventInfoAuxDyn_phiStar_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_TST", &HGamEventInfoAuxDyn_phi_TST, &b_HGamEventInfoAuxDyn_phi_TST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_hardVertexTST", &HGamEventInfoAuxDyn_phi_hardVertexTST, &b_HGamEventInfoAuxDyn_phi_hardVertexTST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_hybridtop2", &HGamEventInfoAuxDyn_phi_hybridtop2, &b_HGamEventInfoAuxDyn_phi_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_recotop1", &HGamEventInfoAuxDyn_phi_recotop1, &b_HGamEventInfoAuxDyn_phi_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupVertexPhi", &HGamEventInfoAuxDyn_pileupVertexPhi, &b_HGamEventInfoAuxDyn_pileupVertexPhi);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupVertexSumPt2", &HGamEventInfoAuxDyn_pileupVertexSumPt2, &b_HGamEventInfoAuxDyn_pileupVertexSumPt2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupVertexZ", &HGamEventInfoAuxDyn_pileupVertexZ, &b_HGamEventInfoAuxDyn_pileupVertexZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupWeight", &HGamEventInfoAuxDyn_pileupWeight, &b_HGamEventInfoAuxDyn_pileupWeight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pt_llmax", &HGamEventInfoAuxDyn_pt_llmax, &b_HGamEventInfoAuxDyn_pt_llmax);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.scoreBinaryCatCoup_GlobalICHEP", &HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.scoreBinaryCatCoup_HybridICHEP", &HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_MonoH_2var", &HGamEventInfoAuxDyn_score_MonoH_2var, &b_HGamEventInfoAuxDyn_score_MonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_WH_ICHEP2020", &HGamEventInfoAuxDyn_score_WH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_WH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ZH_ICHEP2020", &HGamEventInfoAuxDyn_score_ZH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_ZH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_recotop1", &HGamEventInfoAuxDyn_score_recotop1, &b_HGamEventInfoAuxDyn_score_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_recotop2", &HGamEventInfoAuxDyn_score_recotop2, &b_HGamEventInfoAuxDyn_score_recotop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_tHjb_ICHEP2020", &HGamEventInfoAuxDyn_score_tHjb_ICHEP2020, &b_HGamEventInfoAuxDyn_score_tHjb_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_tWH_ICHEP2020", &HGamEventInfoAuxDyn_score_tWH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_tWH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ttH", &HGamEventInfoAuxDyn_score_ttH, &b_HGamEventInfoAuxDyn_score_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ttHCP", &HGamEventInfoAuxDyn_score_ttHCP, &b_HGamEventInfoAuxDyn_score_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ttH_ICHEP2020", &HGamEventInfoAuxDyn_score_ttH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_ttH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.selectedVertexPhi", &HGamEventInfoAuxDyn_selectedVertexPhi, &b_HGamEventInfoAuxDyn_selectedVertexPhi);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.selectedVertexSumPt2", &HGamEventInfoAuxDyn_selectedVertexSumPt2, &b_HGamEventInfoAuxDyn_selectedVertexSumPt2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.selectedVertexZ", &HGamEventInfoAuxDyn_selectedVertexZ, &b_HGamEventInfoAuxDyn_selectedVertexZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.sumTau_yyj_30", &HGamEventInfoAuxDyn_sumTau_yyj_30, &b_HGamEventInfoAuxDyn_sumTau_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.sumet_TST", &HGamEventInfoAuxDyn_sumet_TST, &b_HGamEventInfoAuxDyn_sumet_TST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.sumet_hardVertexTST", &HGamEventInfoAuxDyn_sumet_hardVertexTST, &b_HGamEventInfoAuxDyn_sumet_hardVertexTST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weight", &HGamEventInfoAuxDyn_weight, &b_HGamEventInfoAuxDyn_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_GlobalICHEP", &HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_HybridICHEP", &HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_MonoH_2var", &HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var, &b_HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_Moriond2017BDT", &HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT, &b_HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFGlobalICHEP", &HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFHybridICHEP", &HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFMonoH_2var", &HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var, &b_HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFMoriond2017BDT", &HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT, &b_HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFXGBoost_ttH", &HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH, &b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFXGBoost_ttHCP", &HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP, &b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_XGBoost_ttH", &HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH, &b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_XGBoost_ttHCP", &HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP, &b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatXS_HiggsHF", &HGamEventInfoAuxDyn_weightCatXS_HiggsHF, &b_HGamEventInfoAuxDyn_weightCatXS_HiggsHF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatXS_nbjet", &HGamEventInfoAuxDyn_weightCatXS_nbjet, &b_HGamEventInfoAuxDyn_weightCatXS_nbjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatXS_ttH", &HGamEventInfoAuxDyn_weightCatXS_ttH, &b_HGamEventInfoAuxDyn_weightCatXS_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightFJvt", &HGamEventInfoAuxDyn_weightFJvt, &b_HGamEventInfoAuxDyn_weightFJvt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightFJvt_30", &HGamEventInfoAuxDyn_weightFJvt_30, &b_HGamEventInfoAuxDyn_weightFJvt_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightInitial", &HGamEventInfoAuxDyn_weightInitial, &b_HGamEventInfoAuxDyn_weightInitial);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightJvt", &HGamEventInfoAuxDyn_weightJvt, &b_HGamEventInfoAuxDyn_weightJvt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightJvt_30", &HGamEventInfoAuxDyn_weightJvt_30, &b_HGamEventInfoAuxDyn_weightJvt_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightJvt_50", &HGamEventInfoAuxDyn_weightJvt_50, &b_HGamEventInfoAuxDyn_weightJvt_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightLowHighMyy", &HGamEventInfoAuxDyn_weightLowHighMyy, &b_HGamEventInfoAuxDyn_weightLowHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightN_lep", &HGamEventInfoAuxDyn_weightN_lep, &b_HGamEventInfoAuxDyn_weightN_lep);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightN_lep_15", &HGamEventInfoAuxDyn_weightN_lep_15, &b_HGamEventInfoAuxDyn_weightN_lep_15);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightSF", &HGamEventInfoAuxDyn_weightSF, &b_HGamEventInfoAuxDyn_weightSF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightTrigSF", &HGamEventInfoAuxDyn_weightTrigSF, &b_HGamEventInfoAuxDyn_weightTrigSF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j1", &HGamEventInfoAuxDyn_yAbs_j1, &b_HGamEventInfoAuxDyn_yAbs_j1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j1_30", &HGamEventInfoAuxDyn_yAbs_j1_30, &b_HGamEventInfoAuxDyn_yAbs_j1_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j2", &HGamEventInfoAuxDyn_yAbs_j2, &b_HGamEventInfoAuxDyn_yAbs_j2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j2_30", &HGamEventInfoAuxDyn_yAbs_j2_30, &b_HGamEventInfoAuxDyn_yAbs_j2_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_yy", &HGamEventInfoAuxDyn_yAbs_yy, &b_HGamEventInfoAuxDyn_yAbs_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_cutFlow", &HGamEventInfoAuxDyn_yyb_cutFlow, &b_HGamEventInfoAuxDyn_yyb_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_jj", &HGamEventInfoAuxDyn_yybb_BCal_m_jj, &b_HGamEventInfoAuxDyn_yybb_BCal_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_yyjj", &HGamEventInfoAuxDyn_yybb_BCal_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_planarFlow", &HGamEventInfoAuxDyn_yybb_BCal_planarFlow, &b_HGamEventInfoAuxDyn_yybb_BCal_planarFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_sphericityT", &HGamEventInfoAuxDyn_yybb_BCal_sphericityT, &b_HGamEventInfoAuxDyn_yybb_BCal_sphericityT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_eta_1", &HGamEventInfoAuxDyn_yybb_KF_eta_1, &b_HGamEventInfoAuxDyn_yybb_KF_eta_1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_eta_2", &HGamEventInfoAuxDyn_yybb_KF_eta_2, &b_HGamEventInfoAuxDyn_yybb_KF_eta_2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_1", &HGamEventInfoAuxDyn_yybb_KF_m_1, &b_HGamEventInfoAuxDyn_yybb_KF_m_1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_2", &HGamEventInfoAuxDyn_yybb_KF_m_2, &b_HGamEventInfoAuxDyn_yybb_KF_m_2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_jj", &HGamEventInfoAuxDyn_yybb_KF_m_jj, &b_HGamEventInfoAuxDyn_yybb_KF_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_yyjj", &HGamEventInfoAuxDyn_yybb_KF_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_phi_1", &HGamEventInfoAuxDyn_yybb_KF_phi_1, &b_HGamEventInfoAuxDyn_yybb_KF_phi_1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_phi_2", &HGamEventInfoAuxDyn_yybb_KF_phi_2, &b_HGamEventInfoAuxDyn_yybb_KF_phi_2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_planarFlow", &HGamEventInfoAuxDyn_yybb_KF_planarFlow, &b_HGamEventInfoAuxDyn_yybb_KF_planarFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_pt_1", &HGamEventInfoAuxDyn_yybb_KF_pt_1, &b_HGamEventInfoAuxDyn_yybb_KF_pt_1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_pt_2", &HGamEventInfoAuxDyn_yybb_KF_pt_2, &b_HGamEventInfoAuxDyn_yybb_KF_pt_2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_sphericityT", &HGamEventInfoAuxDyn_yybb_KF_sphericityT, &b_HGamEventInfoAuxDyn_yybb_KF_sphericityT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_BCal_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_BCal_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_KF_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_KF_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_candidate_jet1_fix", &HGamEventInfoAuxDyn_yybb_candidate_jet1_fix, &b_HGamEventInfoAuxDyn_yybb_candidate_jet1_fix);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_candidate_jet2_fix", &HGamEventInfoAuxDyn_yybb_candidate_jet2_fix, &b_HGamEventInfoAuxDyn_yybb_candidate_jet2_fix);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_jj", &HGamEventInfoAuxDyn_yybb_m_jj, &b_HGamEventInfoAuxDyn_yybb_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_yyjj", &HGamEventInfoAuxDyn_yybb_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_VBF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_VBF_Signal_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Signal_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Signal_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_VBF_ggF_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ggF_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ggF_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_VBF_ttH_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_VBF_yy_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_yy_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_VBF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_VBF_Signal_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Signal_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_Signal_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_VBF_ggF_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ggF_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ggF_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_VBF_ttH_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_VBF_yy_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_yy_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_VBF_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_KF_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_KF_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_VBF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_VBF_Signal_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Signal_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_Signal_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_VBF_ggF_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ggF_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ggF_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_VBF_ttH_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_VBF_yy_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_yy_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_VBF_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_BCal_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_BCal_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_BCal_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_KF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_KF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_KF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_planarFlow", &HGamEventInfoAuxDyn_yybb_planarFlow, &b_HGamEventInfoAuxDyn_yybb_planarFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_sphericityT", &HGamEventInfoAuxDyn_yybb_sphericityT, &b_HGamEventInfoAuxDyn_yybb_sphericityT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_vbf_jet1", &HGamEventInfoAuxDyn_yybb_vbf_jet1, &b_HGamEventInfoAuxDyn_yybb_vbf_jet1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_vbf_jet2", &HGamEventInfoAuxDyn_yybb_vbf_jet2, &b_HGamEventInfoAuxDyn_yybb_vbf_jet2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_weight", &HGamEventInfoAuxDyn_yybb_weight, &b_HGamEventInfoAuxDyn_yybb_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.zCommon", &HGamEventInfoAuxDyn_zCommon, &b_HGamEventInfoAuxDyn_zCommon);
   fChain->SetBranchAddress("HGamTruthPhotons", &HGamTruthPhotons, &b_HGamTruthPhotons);
   fChain->SetBranchAddress("HGamTruthPhotonsAux.", &HGamTruthPhotonsAux_, &b_HGamTruthPhotonsAux_);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.e", &HGamTruthPhotonsAuxDyn_e, &b_HGamTruthPhotonsAuxDyn_e);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.eta", &HGamTruthPhotonsAuxDyn_eta, &b_HGamTruthPhotonsAuxDyn_eta);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.etcone20", &HGamTruthPhotonsAuxDyn_etcone20, &b_HGamTruthPhotonsAuxDyn_etcone20);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.etcone40", &HGamTruthPhotonsAuxDyn_etcone40, &b_HGamTruthPhotonsAuxDyn_etcone40);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.isIsolated", &HGamTruthPhotonsAuxDyn_isIsolated, &b_HGamTruthPhotonsAuxDyn_isIsolated);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.m", &HGamTruthPhotonsAuxDyn_m, &b_HGamTruthPhotonsAuxDyn_m);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.partonetcone20", &HGamTruthPhotonsAuxDyn_partonetcone20, &b_HGamTruthPhotonsAuxDyn_partonetcone20);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.partonetcone40", &HGamTruthPhotonsAuxDyn_partonetcone40, &b_HGamTruthPhotonsAuxDyn_partonetcone40);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.pt", &HGamTruthPhotonsAuxDyn_pt, &b_HGamTruthPhotonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.ptcone20", &HGamTruthPhotonsAuxDyn_ptcone20, &b_HGamTruthPhotonsAuxDyn_ptcone20);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.ptcone40", &HGamTruthPhotonsAuxDyn_ptcone40, &b_HGamTruthPhotonsAuxDyn_ptcone40);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.px", &HGamTruthPhotonsAuxDyn_px, &b_HGamTruthPhotonsAuxDyn_px);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.py", &HGamTruthPhotonsAuxDyn_py, &b_HGamTruthPhotonsAuxDyn_py);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.pz", &HGamTruthPhotonsAuxDyn_pz, &b_HGamTruthPhotonsAuxDyn_pz);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.recoLink", &HGamTruthPhotonsAuxDyn_recoLink_, &b_HGamTruthPhotonsAuxDyn_recoLink_);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.recoLink.m_persKey", HGamTruthPhotonsAuxDyn_recoLink_m_persKey, &b_HGamTruthPhotonsAuxDyn_recoLink_m_persKey);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.recoLink.m_persIndex", HGamTruthPhotonsAuxDyn_recoLink_m_persIndex, &b_HGamTruthPhotonsAuxDyn_recoLink_m_persIndex);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.truthOrigin", &HGamTruthPhotonsAuxDyn_truthOrigin, &b_HGamTruthPhotonsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamTruthPhotonsAuxDyn.truthType", &HGamTruthPhotonsAuxDyn_truthType, &b_HGamTruthPhotonsAuxDyn_truthType);
   fChain->SetBranchAddress("HGamTruthElectrons", &HGamTruthElectrons, &b_HGamTruthElectrons);
   fChain->SetBranchAddress("HGamTruthElectronsAux.", &HGamTruthElectronsAux_, &b_HGamTruthElectronsAux_);
   fChain->SetBranchAddress("HGamTruthMuons", &HGamTruthMuons, &b_HGamTruthMuons);
   fChain->SetBranchAddress("HGamTruthMuonsAux.", &HGamTruthMuonsAux_, &b_HGamTruthMuonsAux_);
   fChain->SetBranchAddress("HGamTruthTaus", &HGamTruthTaus, &b_HGamTruthTaus);
   fChain->SetBranchAddress("HGamTruthTausAux.", &HGamTruthTausAux_, &b_HGamTruthTausAux_);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJets", &HGamAntiKt4TruthWZJets, &b_HGamAntiKt4TruthWZJets);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAux.", &HGamAntiKt4TruthWZJetsAux_, &b_HGamAntiKt4TruthWZJetsAux_);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAuxDyn.HadronConeExclTruthLabelID", &HGamAntiKt4TruthWZJetsAuxDyn_HadronConeExclTruthLabelID, &b_HGamAntiKt4TruthWZJetsAuxDyn_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAuxDyn.constituentLinks", &HGamAntiKt4TruthWZJetsAuxDyn_constituentLinks, &b_HGamAntiKt4TruthWZJetsAuxDyn_constituentLinks);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAuxDyn.eta", &HGamAntiKt4TruthWZJetsAuxDyn_eta, &b_HGamAntiKt4TruthWZJetsAuxDyn_eta);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAuxDyn.m", &HGamAntiKt4TruthWZJetsAuxDyn_m, &b_HGamAntiKt4TruthWZJetsAuxDyn_m);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAuxDyn.phi", &HGamAntiKt4TruthWZJetsAuxDyn_phi, &b_HGamAntiKt4TruthWZJetsAuxDyn_phi);
   fChain->SetBranchAddress("HGamAntiKt4TruthWZJetsAuxDyn.pt", &HGamAntiKt4TruthWZJetsAuxDyn_pt, &b_HGamAntiKt4TruthWZJetsAuxDyn_pt);
   fChain->SetBranchAddress("HGamMET_Truth", &HGamMET_Truth, &b_HGamMET_Truth);
   fChain->SetBranchAddress("HGamMET_TruthAux.", &HGamMET_TruthAux_, &b_HGamMET_TruthAux_);
   fChain->SetBranchAddress("HGamMET_TruthAuxDyn.mpx", &HGamMET_TruthAuxDyn_mpx, &b_HGamMET_TruthAuxDyn_mpx);
   fChain->SetBranchAddress("HGamMET_TruthAuxDyn.mpy", &HGamMET_TruthAuxDyn_mpy, &b_HGamMET_TruthAuxDyn_mpy);
   fChain->SetBranchAddress("HGamMET_TruthAuxDyn.name", &HGamMET_TruthAuxDyn_name, &b_HGamMET_TruthAuxDyn_name);
   fChain->SetBranchAddress("HGamMET_TruthAuxDyn.source", &HGamMET_TruthAuxDyn_source, &b_HGamMET_TruthAuxDyn_source);
   fChain->SetBranchAddress("HGamMET_TruthAuxDyn.sumet", &HGamMET_TruthAuxDyn_sumet, &b_HGamMET_TruthAuxDyn_sumet);
   fChain->SetBranchAddress("HGamTruthHiggsBosons", &HGamTruthHiggsBosons, &b_HGamTruthHiggsBosons);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAux.", &HGamTruthHiggsBosonsAux_, &b_HGamTruthHiggsBosonsAux_);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAuxDyn.e", &HGamTruthHiggsBosonsAuxDyn_e, &b_HGamTruthHiggsBosonsAuxDyn_e);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAuxDyn.m", &HGamTruthHiggsBosonsAuxDyn_m, &b_HGamTruthHiggsBosonsAuxDyn_m);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAuxDyn.pt", &HGamTruthHiggsBosonsAuxDyn_pt, &b_HGamTruthHiggsBosonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAuxDyn.px", &HGamTruthHiggsBosonsAuxDyn_px, &b_HGamTruthHiggsBosonsAuxDyn_px);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAuxDyn.py", &HGamTruthHiggsBosonsAuxDyn_py, &b_HGamTruthHiggsBosonsAuxDyn_py);
   fChain->SetBranchAddress("HGamTruthHiggsBosonsAuxDyn.pz", &HGamTruthHiggsBosonsAuxDyn_pz, &b_HGamTruthHiggsBosonsAuxDyn_pz);
   fChain->SetBranchAddress("TruthEvents", &TruthEvents, &b_TruthEvents);
   fChain->SetBranchAddress("TruthEventsAux.", &TruthEventsAux_, &b_TruthEventsAux_);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDFID1", &TruthEventsAuxDyn_PDFID1, &b_TruthEventsAuxDyn_PDFID1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDFID2", &TruthEventsAuxDyn_PDFID2, &b_TruthEventsAuxDyn_PDFID2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDGID1", &TruthEventsAuxDyn_PDGID1, &b_TruthEventsAuxDyn_PDGID1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDGID2", &TruthEventsAuxDyn_PDGID2, &b_TruthEventsAuxDyn_PDGID2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.Q", &TruthEventsAuxDyn_Q, &b_TruthEventsAuxDyn_Q);
   fChain->SetBranchAddress("TruthEventsAuxDyn.X1", &TruthEventsAuxDyn_X1, &b_TruthEventsAuxDyn_X1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.X2", &TruthEventsAuxDyn_X2, &b_TruthEventsAuxDyn_X2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.XF1", &TruthEventsAuxDyn_XF1, &b_TruthEventsAuxDyn_XF1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.XF2", &TruthEventsAuxDyn_XF2, &b_TruthEventsAuxDyn_XF2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.weights", &TruthEventsAuxDyn_weights, &b_TruthEventsAuxDyn_weights);
   fChain->SetBranchAddress("HGamTruthEventInfo", &HGamTruthEventInfo, &b_HGamTruthEventInfo);
   fChain->SetBranchAddress("HGamTruthEventInfoAux.", &HGamTruthEventInfoAux_, &b_HGamTruthEventInfoAux_);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.DR_y_y", &HGamTruthEventInfoAuxDyn_DR_y_y, &b_HGamTruthEventInfoAuxDyn_DR_y_y);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.DRmin_y_j", &HGamTruthEventInfoAuxDyn_DRmin_y_j, &b_HGamTruthEventInfoAuxDyn_DRmin_y_j);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_j_j", &HGamTruthEventInfoAuxDyn_Dphi_j_j, &b_HGamTruthEventInfoAuxDyn_Dphi_j_j);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_j_j_30", &HGamTruthEventInfoAuxDyn_Dphi_j_j_30, &b_HGamTruthEventInfoAuxDyn_Dphi_j_j_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_j_j_30_signed", &HGamTruthEventInfoAuxDyn_Dphi_j_j_30_signed, &b_HGamTruthEventInfoAuxDyn_Dphi_j_j_30_signed);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_j_j_50", &HGamTruthEventInfoAuxDyn_Dphi_j_j_50, &b_HGamTruthEventInfoAuxDyn_Dphi_j_j_50);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_j_j_50_signed", &HGamTruthEventInfoAuxDyn_Dphi_j_j_50_signed, &b_HGamTruthEventInfoAuxDyn_Dphi_j_j_50_signed);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_y_y", &HGamTruthEventInfoAuxDyn_Dphi_y_y, &b_HGamTruthEventInfoAuxDyn_Dphi_y_y);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_yy_jj", &HGamTruthEventInfoAuxDyn_Dphi_yy_jj, &b_HGamTruthEventInfoAuxDyn_Dphi_yy_jj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_yy_jj_30", &HGamTruthEventInfoAuxDyn_Dphi_yy_jj_30, &b_HGamTruthEventInfoAuxDyn_Dphi_yy_jj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dphi_yy_jj_50", &HGamTruthEventInfoAuxDyn_Dphi_yy_jj_50, &b_HGamTruthEventInfoAuxDyn_Dphi_yy_jj_50);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dy_j_j", &HGamTruthEventInfoAuxDyn_Dy_j_j, &b_HGamTruthEventInfoAuxDyn_Dy_j_j);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dy_j_j_30", &HGamTruthEventInfoAuxDyn_Dy_j_j_30, &b_HGamTruthEventInfoAuxDyn_Dy_j_j_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dy_y_y", &HGamTruthEventInfoAuxDyn_Dy_y_y, &b_HGamTruthEventInfoAuxDyn_Dy_y_y);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dy_yy_jj", &HGamTruthEventInfoAuxDyn_Dy_yy_jj, &b_HGamTruthEventInfoAuxDyn_Dy_yy_jj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Dy_yy_jj_30", &HGamTruthEventInfoAuxDyn_Dy_yy_jj_30, &b_HGamTruthEventInfoAuxDyn_Dy_yy_jj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.E_y1", &HGamTruthEventInfoAuxDyn_E_y1, &b_HGamTruthEventInfoAuxDyn_E_y1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.E_y2", &HGamTruthEventInfoAuxDyn_E_y2, &b_HGamTruthEventInfoAuxDyn_E_y2);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.HT_30", &HGamTruthEventInfoAuxDyn_HT_30, &b_HGamTruthEventInfoAuxDyn_HT_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.HTall_30", &HGamTruthEventInfoAuxDyn_HTall_30, &b_HGamTruthEventInfoAuxDyn_HTall_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Hc_N_bjets25", &HGamTruthEventInfoAuxDyn_Hc_N_bjets25, &b_HGamTruthEventInfoAuxDyn_Hc_N_bjets25);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Hc_N_cjets25", &HGamTruthEventInfoAuxDyn_Hc_N_cjets25, &b_HGamTruthEventInfoAuxDyn_Hc_N_cjets25);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Hc_N_cjets30", &HGamTruthEventInfoAuxDyn_Hc_N_cjets30, &b_HGamTruthEventInfoAuxDyn_Hc_N_cjets30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Hc_cutFlowAllJet", &HGamTruthEventInfoAuxDyn_Hc_cutFlowAllJet, &b_HGamTruthEventInfoAuxDyn_Hc_cutFlowAllJet);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.HiggsHF_cutFlow", &HGamTruthEventInfoAuxDyn_HiggsHF_cutFlow, &b_HGamTruthEventInfoAuxDyn_HiggsHF_cutFlow);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_e", &HGamTruthEventInfoAuxDyn_N_e, &b_HGamTruthEventInfoAuxDyn_N_e);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j", &HGamTruthEventInfoAuxDyn_N_j, &b_HGamTruthEventInfoAuxDyn_N_j);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_30", &HGamTruthEventInfoAuxDyn_N_j_30, &b_HGamTruthEventInfoAuxDyn_N_j_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_50", &HGamTruthEventInfoAuxDyn_N_j_50, &b_HGamTruthEventInfoAuxDyn_N_j_50);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_btag30", &HGamTruthEventInfoAuxDyn_N_j_btag30, &b_HGamTruthEventInfoAuxDyn_N_j_btag30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_central", &HGamTruthEventInfoAuxDyn_N_j_central, &b_HGamTruthEventInfoAuxDyn_N_j_central);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_central30", &HGamTruthEventInfoAuxDyn_N_j_central30, &b_HGamTruthEventInfoAuxDyn_N_j_central30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_lep", &HGamTruthEventInfoAuxDyn_N_lep, &b_HGamTruthEventInfoAuxDyn_N_lep);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_lep_15", &HGamTruthEventInfoAuxDyn_N_lep_15, &b_HGamTruthEventInfoAuxDyn_N_lep_15);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_mu", &HGamTruthEventInfoAuxDyn_N_mu, &b_HGamTruthEventInfoAuxDyn_N_mu);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.Zepp", &HGamTruthEventInfoAuxDyn_Zepp, &b_HGamTruthEventInfoAuxDyn_Zepp);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.alldm_pt", &HGamTruthEventInfoAuxDyn_alldm_pt, &b_HGamTruthEventInfoAuxDyn_alldm_pt);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.catCoup", &HGamTruthEventInfoAuxDyn_catCoup, &b_HGamTruthEventInfoAuxDyn_catCoup);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.catXS_HiggsHF", &HGamTruthEventInfoAuxDyn_catXS_HiggsHF, &b_HGamTruthEventInfoAuxDyn_catXS_HiggsHF);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.catXS_VBF", &HGamTruthEventInfoAuxDyn_catXS_VBF, &b_HGamTruthEventInfoAuxDyn_catXS_VBF);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.catXS_nbjet", &HGamTruthEventInfoAuxDyn_catXS_nbjet, &b_HGamTruthEventInfoAuxDyn_catXS_nbjet);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.catXS_ttH", &HGamTruthEventInfoAuxDyn_catXS_ttH, &b_HGamTruthEventInfoAuxDyn_catXS_ttH);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.cosTS_yy", &HGamTruthEventInfoAuxDyn_cosTS_yy, &b_HGamTruthEventInfoAuxDyn_cosTS_yy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.cosTS_yyjj", &HGamTruthEventInfoAuxDyn_cosTS_yyjj, &b_HGamTruthEventInfoAuxDyn_cosTS_yyjj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.isFiducial", &HGamTruthEventInfoAuxDyn_isFiducial, &b_HGamTruthEventInfoAuxDyn_isFiducial);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.isFiducialHighMyy", &HGamTruthEventInfoAuxDyn_isFiducialHighMyy, &b_HGamTruthEventInfoAuxDyn_isFiducialHighMyy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.isFiducialKinOnly", &HGamTruthEventInfoAuxDyn_isFiducialKinOnly, &b_HGamTruthEventInfoAuxDyn_isFiducialKinOnly);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.isFiducialLowMyy", &HGamTruthEventInfoAuxDyn_isFiducialLowMyy, &b_HGamTruthEventInfoAuxDyn_isFiducialLowMyy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.isVyyOverlap", &HGamTruthEventInfoAuxDyn_isVyyOverlap, &b_HGamTruthEventInfoAuxDyn_isVyyOverlap);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_ee", &HGamTruthEventInfoAuxDyn_m_ee, &b_HGamTruthEventInfoAuxDyn_m_ee);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_h1", &HGamTruthEventInfoAuxDyn_m_h1, &b_HGamTruthEventInfoAuxDyn_m_h1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_h2", &HGamTruthEventInfoAuxDyn_m_h2, &b_HGamTruthEventInfoAuxDyn_m_h2);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_hh", &HGamTruthEventInfoAuxDyn_m_hh, &b_HGamTruthEventInfoAuxDyn_m_hh);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_jj", &HGamTruthEventInfoAuxDyn_m_jj, &b_HGamTruthEventInfoAuxDyn_m_jj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_jj_30", &HGamTruthEventInfoAuxDyn_m_jj_30, &b_HGamTruthEventInfoAuxDyn_m_jj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_jj_50", &HGamTruthEventInfoAuxDyn_m_jj_50, &b_HGamTruthEventInfoAuxDyn_m_jj_50);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_mumu", &HGamTruthEventInfoAuxDyn_m_mumu, &b_HGamTruthEventInfoAuxDyn_m_mumu);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_yy", &HGamTruthEventInfoAuxDyn_m_yy, &b_HGamTruthEventInfoAuxDyn_m_yy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_yyj", &HGamTruthEventInfoAuxDyn_m_yyj, &b_HGamTruthEventInfoAuxDyn_m_yyj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_yyj_30", &HGamTruthEventInfoAuxDyn_m_yyj_30, &b_HGamTruthEventInfoAuxDyn_m_yyj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.m_yyjj", &HGamTruthEventInfoAuxDyn_m_yyjj, &b_HGamTruthEventInfoAuxDyn_m_yyjj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.massTrans", &HGamTruthEventInfoAuxDyn_massTrans, &b_HGamTruthEventInfoAuxDyn_massTrans);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.maxTau_yyj_30", &HGamTruthEventInfoAuxDyn_maxTau_yyj_30, &b_HGamTruthEventInfoAuxDyn_maxTau_yyj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.met_NonHad", &HGamTruthEventInfoAuxDyn_met_NonHad, &b_HGamTruthEventInfoAuxDyn_met_NonHad);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.met_NonInt", &HGamTruthEventInfoAuxDyn_met_NonInt, &b_HGamTruthEventInfoAuxDyn_met_NonInt);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_h1", &HGamTruthEventInfoAuxDyn_pT_h1, &b_HGamTruthEventInfoAuxDyn_pT_h1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_h2", &HGamTruthEventInfoAuxDyn_pT_h2, &b_HGamTruthEventInfoAuxDyn_pT_h2);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_hard", &HGamTruthEventInfoAuxDyn_pT_hard, &b_HGamTruthEventInfoAuxDyn_pT_hard);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_j1", &HGamTruthEventInfoAuxDyn_pT_j1, &b_HGamTruthEventInfoAuxDyn_pT_j1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_j1_30", &HGamTruthEventInfoAuxDyn_pT_j1_30, &b_HGamTruthEventInfoAuxDyn_pT_j1_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_j2", &HGamTruthEventInfoAuxDyn_pT_j2, &b_HGamTruthEventInfoAuxDyn_pT_j2);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_j2_30", &HGamTruthEventInfoAuxDyn_pT_j2_30, &b_HGamTruthEventInfoAuxDyn_pT_j2_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_j3_30", &HGamTruthEventInfoAuxDyn_pT_j3_30, &b_HGamTruthEventInfoAuxDyn_pT_j3_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_jj", &HGamTruthEventInfoAuxDyn_pT_jj, &b_HGamTruthEventInfoAuxDyn_pT_jj);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_y1", &HGamTruthEventInfoAuxDyn_pT_y1, &b_HGamTruthEventInfoAuxDyn_pT_y1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_y2", &HGamTruthEventInfoAuxDyn_pT_y2, &b_HGamTruthEventInfoAuxDyn_pT_y2);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_yy", &HGamTruthEventInfoAuxDyn_pT_yy, &b_HGamTruthEventInfoAuxDyn_pT_yy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_yyj_30", &HGamTruthEventInfoAuxDyn_pT_yyj_30, &b_HGamTruthEventInfoAuxDyn_pT_yyj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_yyjj_30", &HGamTruthEventInfoAuxDyn_pT_yyjj_30, &b_HGamTruthEventInfoAuxDyn_pT_yyjj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pT_yyjj_50", &HGamTruthEventInfoAuxDyn_pT_yyjj_50, &b_HGamTruthEventInfoAuxDyn_pT_yyjj_50);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pTlepMET", &HGamTruthEventInfoAuxDyn_pTlepMET, &b_HGamTruthEventInfoAuxDyn_pTlepMET);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pTt_yy", &HGamTruthEventInfoAuxDyn_pTt_yy, &b_HGamTruthEventInfoAuxDyn_pTt_yy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.passMeyCut", &HGamTruthEventInfoAuxDyn_passMeyCut, &b_HGamTruthEventInfoAuxDyn_passMeyCut);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.phiStar_yy", &HGamTruthEventInfoAuxDyn_phiStar_yy, &b_HGamTruthEventInfoAuxDyn_phiStar_yy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.procCoup", &HGamTruthEventInfoAuxDyn_procCoup, &b_HGamTruthEventInfoAuxDyn_procCoup);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.pt_llmax", &HGamTruthEventInfoAuxDyn_pt_llmax, &b_HGamTruthEventInfoAuxDyn_pt_llmax);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.sumTau_yyj_30", &HGamTruthEventInfoAuxDyn_sumTau_yyj_30, &b_HGamTruthEventInfoAuxDyn_sumTau_yyj_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.sumet_Int", &HGamTruthEventInfoAuxDyn_sumet_Int, &b_HGamTruthEventInfoAuxDyn_sumet_Int);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.vertexZ", &HGamTruthEventInfoAuxDyn_vertexZ, &b_HGamTruthEventInfoAuxDyn_vertexZ);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.yAbs_j1", &HGamTruthEventInfoAuxDyn_yAbs_j1, &b_HGamTruthEventInfoAuxDyn_yAbs_j1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.yAbs_j1_30", &HGamTruthEventInfoAuxDyn_yAbs_j1_30, &b_HGamTruthEventInfoAuxDyn_yAbs_j1_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.yAbs_j2", &HGamTruthEventInfoAuxDyn_yAbs_j2, &b_HGamTruthEventInfoAuxDyn_yAbs_j2);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.yAbs_j2_30", &HGamTruthEventInfoAuxDyn_yAbs_j2_30, &b_HGamTruthEventInfoAuxDyn_yAbs_j2_30);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.yAbs_yy", &HGamTruthEventInfoAuxDyn_yAbs_yy, &b_HGamTruthEventInfoAuxDyn_yAbs_yy);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.y_h1", &HGamTruthEventInfoAuxDyn_y_h1, &b_HGamTruthEventInfoAuxDyn_y_h1);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.y_h2", &HGamTruthEventInfoAuxDyn_y_h2, &b_HGamTruthEventInfoAuxDyn_y_h2);
   fChain->SetBranchAddress("EventInfo", &EventInfo, &b_EventInfo);
   fChain->SetBranchAddress("EventInfoAux.", &EventInfoAux_, &b_EventInfoAux_);
   fChain->SetBranchAddress("EventInfoAuxDyn.DFCommonJets_eventClean_LooseBad", &EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad, &b_EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad);
   fChain->SetBranchAddress("EventInfoAuxDyn.DFCommonJets_isBadBatman", &EventInfoAuxDyn_DFCommonJets_isBadBatman, &b_EventInfoAuxDyn_DFCommonJets_isBadBatman);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Higgs_eta", &EventInfoAuxDyn_HTXS_Higgs_eta, &b_EventInfoAuxDyn_HTXS_Higgs_eta);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Higgs_m", &EventInfoAuxDyn_HTXS_Higgs_m, &b_EventInfoAuxDyn_HTXS_Higgs_m);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Higgs_phi", &EventInfoAuxDyn_HTXS_Higgs_phi, &b_EventInfoAuxDyn_HTXS_Higgs_phi);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Higgs_pt", &EventInfoAuxDyn_HTXS_Higgs_pt, &b_EventInfoAuxDyn_HTXS_Higgs_pt);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Njets_pTjet25", &EventInfoAuxDyn_HTXS_Njets_pTjet25, &b_EventInfoAuxDyn_HTXS_Njets_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Njets_pTjet30", &EventInfoAuxDyn_HTXS_Njets_pTjet30, &b_EventInfoAuxDyn_HTXS_Njets_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage0_Category", &EventInfoAuxDyn_HTXS_Stage0_Category, &b_EventInfoAuxDyn_HTXS_Stage0_Category);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_Category_pTjet25", &EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet25, &b_EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_Category_pTjet30", &EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet30, &b_EventInfoAuxDyn_HTXS_Stage1_2_Category_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_FineIndex_pTjet25", &EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet25, &b_EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_FineIndex_pTjet30", &EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet30, &b_EventInfoAuxDyn_HTXS_Stage1_2_FineIndex_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_Fine_Category_pTjet25", &EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet25, &b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_Fine_Category_pTjet30", &EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet30, &b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_Category_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_Fine_FineIndex_pTjet25", &EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet25, &b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_2_Fine_FineIndex_pTjet30", &EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet30, &b_EventInfoAuxDyn_HTXS_Stage1_2_Fine_FineIndex_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_Category_pTjet25", &EventInfoAuxDyn_HTXS_Stage1_Category_pTjet25, &b_EventInfoAuxDyn_HTXS_Stage1_Category_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_Category_pTjet30", &EventInfoAuxDyn_HTXS_Stage1_Category_pTjet30, &b_EventInfoAuxDyn_HTXS_Stage1_Category_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_FineIndex_pTjet25", &EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet25, &b_EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet25);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_Stage1_FineIndex_pTjet30", &EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet30, &b_EventInfoAuxDyn_HTXS_Stage1_FineIndex_pTjet30);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_V_jets30_eta", &EventInfoAuxDyn_HTXS_V_jets30_eta, &b_EventInfoAuxDyn_HTXS_V_jets30_eta);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_V_jets30_m", &EventInfoAuxDyn_HTXS_V_jets30_m, &b_EventInfoAuxDyn_HTXS_V_jets30_m);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_V_jets30_phi", &EventInfoAuxDyn_HTXS_V_jets30_phi, &b_EventInfoAuxDyn_HTXS_V_jets30_phi);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_V_jets30_pt", &EventInfoAuxDyn_HTXS_V_jets30_pt, &b_EventInfoAuxDyn_HTXS_V_jets30_pt);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_V_pt", &EventInfoAuxDyn_HTXS_V_pt, &b_EventInfoAuxDyn_HTXS_V_pt);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_errorCode", &EventInfoAuxDyn_HTXS_errorCode, &b_EventInfoAuxDyn_HTXS_errorCode);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_isZ2vvDecay", &EventInfoAuxDyn_HTXS_isZ2vvDecay, &b_EventInfoAuxDyn_HTXS_isZ2vvDecay);
   fChain->SetBranchAddress("EventInfoAuxDyn.HTXS_prodMode", &EventInfoAuxDyn_HTXS_prodMode, &b_EventInfoAuxDyn_HTXS_prodMode);
   fChain->SetBranchAddress("EventInfoAuxDyn.actualInteractionsPerCrossing", &EventInfoAuxDyn_actualInteractionsPerCrossing, &b_EventInfoAuxDyn_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfoAuxDyn.averageInteractionsPerCrossing", &EventInfoAuxDyn_averageInteractionsPerCrossing, &b_EventInfoAuxDyn_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfoAuxDyn.centralEventShapeDensity", &EventInfoAuxDyn_centralEventShapeDensity, &b_EventInfoAuxDyn_centralEventShapeDensity);
   fChain->SetBranchAddress("EventInfoAuxDyn.coreFlags", &EventInfoAuxDyn_coreFlags, &b_EventInfoAuxDyn_coreFlags);
   fChain->SetBranchAddress("EventInfoAuxDyn.eventNumber", &EventInfoAuxDyn_eventNumber, &b_EventInfoAuxDyn_eventNumber);
   fChain->SetBranchAddress("EventInfoAuxDyn.eventTypeBitmask", &EventInfoAuxDyn_eventTypeBitmask, &b_EventInfoAuxDyn_eventTypeBitmask);
   fChain->SetBranchAddress("EventInfoAuxDyn.forwardEventShapeDensity", &EventInfoAuxDyn_forwardEventShapeDensity, &b_EventInfoAuxDyn_forwardEventShapeDensity);
   fChain->SetBranchAddress("EventInfoAuxDyn.lumiBlock", &EventInfoAuxDyn_lumiBlock, &b_EventInfoAuxDyn_lumiBlock);
   fChain->SetBranchAddress("EventInfoAuxDyn.mcChannelNumber", &EventInfoAuxDyn_mcChannelNumber, &b_EventInfoAuxDyn_mcChannelNumber);
   fChain->SetBranchAddress("EventInfoAuxDyn.mcEventWeights", &EventInfoAuxDyn_mcEventWeights, &b_EventInfoAuxDyn_mcEventWeights);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g20_tight_icaloloose_L12EM15VHI", &EventInfoAuxDyn_passTrig_HLT_2g20_tight_icaloloose_L12EM15VHI, &b_EventInfoAuxDyn_passTrig_HLT_2g20_tight_icaloloose_L12EM15VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g22_tight_L12EM15VHI", &EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI, &b_EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g120_loose_L1EM22VHI", &EventInfoAuxDyn_passTrig_HLT_g120_loose_L1EM22VHI, &b_EventInfoAuxDyn_passTrig_HLT_g120_loose_L1EM22VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g140_loose_L1EM22VHI", &EventInfoAuxDyn_passTrig_HLT_g140_loose_L1EM22VHI, &b_EventInfoAuxDyn_passTrig_HLT_g140_loose_L1EM22VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g35_medium_g25_medium_L12EM20VH", &EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH, &b_EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH);
   fChain->SetBranchAddress("EventInfoAuxDyn.runNumber", &EventInfoAuxDyn_runNumber, &b_EventInfoAuxDyn_runNumber);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.author", &HGamElectronsAuxDyn_author, &b_HGamElectronsAuxDyn_author);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.charge", &HGamElectronsAuxDyn_charge, &b_HGamElectronsAuxDyn_charge);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.eta", &HGamElectronsAuxDyn_eta, &b_HGamElectronsAuxDyn_eta);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.eta_s2", &HGamElectronsAuxDyn_eta_s2, &b_HGamElectronsAuxDyn_eta_s2);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.isTight", &HGamElectronsAuxDyn_isTight, &b_HGamElectronsAuxDyn_isTight);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.m", &HGamElectronsAuxDyn_m, &b_HGamElectronsAuxDyn_m);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.phi", &HGamElectronsAuxDyn_phi, &b_HGamElectronsAuxDyn_phi);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.pt", &HGamElectronsAuxDyn_pt, &b_HGamElectronsAuxDyn_pt);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000", &HGamElectronsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000, &b_HGamElectronsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000", &HGamElectronsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000, &b_HGamElectronsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.scaleFactor", &HGamElectronsAuxDyn_scaleFactor, &b_HGamElectronsAuxDyn_scaleFactor);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.topoetcone20", &HGamElectronsAuxDyn_topoetcone20, &b_HGamElectronsAuxDyn_topoetcone20);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.truthOrigin", &HGamElectronsAuxDyn_truthOrigin, &b_HGamElectronsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.truthType", &HGamElectronsAuxDyn_truthType, &b_HGamElectronsAuxDyn_truthType);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.e", &HGamTruthElectronsAuxDyn_e, &b_HGamTruthElectronsAuxDyn_e);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.eta", &HGamTruthElectronsAuxDyn_eta, &b_HGamTruthElectronsAuxDyn_eta);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.m", &HGamTruthElectronsAuxDyn_m, &b_HGamTruthElectronsAuxDyn_m);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.pt", &HGamTruthElectronsAuxDyn_pt, &b_HGamTruthElectronsAuxDyn_pt);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.px", &HGamTruthElectronsAuxDyn_px, &b_HGamTruthElectronsAuxDyn_px);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.py", &HGamTruthElectronsAuxDyn_py, &b_HGamTruthElectronsAuxDyn_py);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.pz", &HGamTruthElectronsAuxDyn_pz, &b_HGamTruthElectronsAuxDyn_pz);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.truthOrigin", &HGamTruthElectronsAuxDyn_truthOrigin, &b_HGamTruthElectronsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.truthType", &HGamTruthElectronsAuxDyn_truthType, &b_HGamTruthElectronsAuxDyn_truthType);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.truthLink", &HGamElectronsAuxDyn_truthLink, &b_HGamElectronsAuxDyn_truthLink);
   fChain->SetBranchAddress("HGamTruthElectronsAuxDyn.recoLink", &HGamTruthElectronsAuxDyn_recoLink, &b_HGamTruthElectronsAuxDyn_recoLink);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.charge", &HGamMuonsAuxDyn_charge, &b_HGamMuonsAuxDyn_charge);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.eta", &HGamMuonsAuxDyn_eta, &b_HGamMuonsAuxDyn_eta);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.isBad", &HGamMuonsAuxDyn_isBad, &b_HGamMuonsAuxDyn_isBad);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.muonType", &HGamMuonsAuxDyn_muonType, &b_HGamMuonsAuxDyn_muonType);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.neflowisol20", &HGamMuonsAuxDyn_neflowisol20, &b_HGamMuonsAuxDyn_neflowisol20);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.passIPCut", &HGamMuonsAuxDyn_passIPCut, &b_HGamMuonsAuxDyn_passIPCut);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.phi", &HGamMuonsAuxDyn_phi, &b_HGamMuonsAuxDyn_phi);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.pt", &HGamMuonsAuxDyn_pt, &b_HGamMuonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.ptcone20_Nonprompt_All_MaxWeightTTVA_pt500", &HGamMuonsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500, &b_HGamMuonsAuxDyn_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500", &HGamMuonsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500, &b_HGamMuonsAuxDyn_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.scaleFactor", &HGamMuonsAuxDyn_scaleFactor, &b_HGamMuonsAuxDyn_scaleFactor);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.truthOrigin", &HGamMuonsAuxDyn_truthOrigin, &b_HGamMuonsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.truthType", &HGamMuonsAuxDyn_truthType, &b_HGamMuonsAuxDyn_truthType);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.e", &HGamTruthMuonsAuxDyn_e, &b_HGamTruthMuonsAuxDyn_e);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.eta", &HGamTruthMuonsAuxDyn_eta, &b_HGamTruthMuonsAuxDyn_eta);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.m", &HGamTruthMuonsAuxDyn_m, &b_HGamTruthMuonsAuxDyn_m);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.pt", &HGamTruthMuonsAuxDyn_pt, &b_HGamTruthMuonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.px", &HGamTruthMuonsAuxDyn_px, &b_HGamTruthMuonsAuxDyn_px);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.py", &HGamTruthMuonsAuxDyn_py, &b_HGamTruthMuonsAuxDyn_py);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.pz", &HGamTruthMuonsAuxDyn_pz, &b_HGamTruthMuonsAuxDyn_pz);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.truthOrigin", &HGamTruthMuonsAuxDyn_truthOrigin, &b_HGamTruthMuonsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamTruthMuonsAuxDyn.truthType", &HGamTruthMuonsAuxDyn_truthType, &b_HGamTruthMuonsAuxDyn_truthType);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.RNNEleScore", &HGamTauJetsAuxDyn_RNNEleScore, &b_HGamTauJetsAuxDyn_RNNEleScore);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.RNNEleScoreSigTrans", &HGamTauJetsAuxDyn_RNNEleScoreSigTrans, &b_HGamTauJetsAuxDyn_RNNEleScoreSigTrans);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.RNNJetScore", &HGamTauJetsAuxDyn_RNNJetScore, &b_HGamTauJetsAuxDyn_RNNJetScore);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.RNNJetScoreSigTrans", &HGamTauJetsAuxDyn_RNNJetScoreSigTrans, &b_HGamTauJetsAuxDyn_RNNJetScoreSigTrans);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.TauEffSF_EleRNNLoose", &HGamTauJetsAuxDyn_TauEffSF_EleRNNLoose, &b_HGamTauJetsAuxDyn_TauEffSF_EleRNNLoose);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.TauEffSF_EleRNNMedium", &HGamTauJetsAuxDyn_TauEffSF_EleRNNMedium, &b_HGamTauJetsAuxDyn_TauEffSF_EleRNNMedium);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.TauEffSF_JetIDRNNLoose", &HGamTauJetsAuxDyn_TauEffSF_JetIDRNNLoose, &b_HGamTauJetsAuxDyn_TauEffSF_JetIDRNNLoose);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.TauEffSF_JetIDRNNMedium", &HGamTauJetsAuxDyn_TauEffSF_JetIDRNNMedium, &b_HGamTauJetsAuxDyn_TauEffSF_JetIDRNNMedium);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.TauEffSF_JetIDRNNTight", &HGamTauJetsAuxDyn_TauEffSF_JetIDRNNTight, &b_HGamTauJetsAuxDyn_TauEffSF_JetIDRNNTight);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.TauEffSF_Reco", &HGamTauJetsAuxDyn_TauEffSF_Reco, &b_HGamTauJetsAuxDyn_TauEffSF_Reco);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.charge", &HGamTauJetsAuxDyn_charge, &b_HGamTauJetsAuxDyn_charge);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.classifierType", &HGamTauJetsAuxDyn_classifierType, &b_HGamTauJetsAuxDyn_classifierType);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.eta", &HGamTauJetsAuxDyn_eta, &b_HGamTauJetsAuxDyn_eta);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.isHadronicTau", &HGamTauJetsAuxDyn_isHadronicTau, &b_HGamTauJetsAuxDyn_isHadronicTau);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.isTauFlags", &HGamTauJetsAuxDyn_isTauFlags, &b_HGamTauJetsAuxDyn_isTauFlags);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.isTruthJet", &HGamTauJetsAuxDyn_isTruthJet, &b_HGamTauJetsAuxDyn_isTruthJet);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.nTracks", &HGamTauJetsAuxDyn_nTracks, &b_HGamTauJetsAuxDyn_nTracks);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.overlapJet", &HGamTauJetsAuxDyn_overlapJet, &b_HGamTauJetsAuxDyn_overlapJet);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.phi", &HGamTauJetsAuxDyn_phi, &b_HGamTauJetsAuxDyn_phi);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.pt", &HGamTauJetsAuxDyn_pt, &b_HGamTauJetsAuxDyn_pt);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.truthMatchID", &HGamTauJetsAuxDyn_truthMatchID, &b_HGamTauJetsAuxDyn_truthMatchID);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.HiggsHF_N_bjets", &HGamTruthEventInfoAuxDyn_HiggsHF_N_bjets, &b_HGamTruthEventInfoAuxDyn_HiggsHF_N_bjets);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.HiggsHF_N_cjets", &HGamTruthEventInfoAuxDyn_HiggsHF_N_cjets, &b_HGamTruthEventInfoAuxDyn_HiggsHF_N_cjets);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.e", &HGamTruthTausAuxDyn_e, &b_HGamTruthTausAuxDyn_e);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.eta", &HGamTruthTausAuxDyn_eta, &b_HGamTruthTausAuxDyn_eta);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.pt", &HGamTruthTausAuxDyn_pt, &b_HGamTruthTausAuxDyn_pt);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.px", &HGamTruthTausAuxDyn_px, &b_HGamTruthTausAuxDyn_px);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.py", &HGamTruthTausAuxDyn_py, &b_HGamTruthTausAuxDyn_py);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.pz", &HGamTruthTausAuxDyn_pz, &b_HGamTruthTausAuxDyn_pz);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.truthOrigin", &HGamTruthTausAuxDyn_truthOrigin, &b_HGamTruthTausAuxDyn_truthOrigin);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.truthType", &HGamTruthTausAuxDyn_truthType, &b_HGamTruthTausAuxDyn_truthType);
   fChain->SetBranchAddress("HGamTruthTausAuxDyn.recoLink", &HGamTruthTausAuxDyn_recoLink, &b_HGamTruthTausAuxDyn_recoLink);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_m_yy_cat", &HGamEventInfoAuxDyn_yyb_m_yy_cat, &b_HGamEventInfoAuxDyn_yyb_m_yy_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_SelectedForwardJet", &HGamEventInfoAuxDyn_yyb_SelectedForwardJet, &b_HGamEventInfoAuxDyn_yyb_SelectedForwardJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_SelectedTaggedJet", &HGamEventInfoAuxDyn_yyb_SelectedTaggedJet, &b_HGamEventInfoAuxDyn_yyb_SelectedTaggedJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_VLQmass", &HGamEventInfoAuxDyn_yyb_VLQmass, &b_HGamEventInfoAuxDyn_yyb_VLQmass);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_truth_label_forwardjet", &HGamEventInfoAuxDyn_yyb_truth_label_forwardjet, &b_HGamEventInfoAuxDyn_yyb_truth_label_forwardjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_truth_label_taggedjet", &HGamEventInfoAuxDyn_yyb_truth_label_taggedjet, &b_HGamEventInfoAuxDyn_yyb_truth_label_taggedjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_weight", &HGamEventInfoAuxDyn_yyb_weight, &b_HGamEventInfoAuxDyn_yyb_weight);
   Notify();
}

Bool_t h029_MxAOD::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void h029_MxAOD::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t h029_MxAOD::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef h029_MxAOD_cxx
