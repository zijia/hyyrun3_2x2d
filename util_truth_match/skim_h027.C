
void PrintProgressBar( int index, int total )
{
        if( index%10000 == 0 )
        {
                TString print_bar = " [";
                for( int bar = 0; bar < 20; bar++ )
                {
                        double current_fraction = double(bar) / 20.0;
                        if( double(index)/double(total) > current_fraction )
                                print_bar.Append("/");
                        else
                                print_bar.Append(".");
                }
                print_bar.Append("] ");
                std::cout << print_bar << 100.*(double(index)/double(total)) << "%\r" << std::flush;
        }
}


double deltaPhi(double angle1, double angle2)   {
        double dPhi = angle2 - angle1; 
        if (dPhi > TMath::Pi()) { dPhi -= 2 * TMath::Pi();}
        else if (dPhi < -TMath::Pi()) { dPhi += 2 * TMath::Pi();}

        return dPhi;
}

vector <double> get_sumWeights(TString _infilelist)
{
        vector <double> sum_of_weight = {0.,0.,0.};
        ifstream infile(_infilelist, ios::in);
        string line;
        while (getline(infile, line)){
                TFile *fin = new TFile(line.c_str(),"read");
                TIter next(fin->GetListOfKeys());
                TKey *key;
                while ((key=(TKey*)next())) {
                        TString name = key->GetName();
                        TString line_appo = line.c_str();
                        if(name.Contains("noDalitz_weighted")){
                                TH1F* histo = (TH1F*)fin->Get(name);

                                //cout<<"soW= "<<(histo->GetBinContent(1)/histo->GetBinContent(2))*histo->GetBinContent(3)<<endl;

                                if(line_appo.Contains("mc16a")){
                                        sum_of_weight[0] = (histo->GetBinContent(1)/histo->GetBinContent(2))*histo->GetBinContent(3);
                                }
                                if(line_appo.Contains("mc16d")){
                                        sum_of_weight[1] = (histo->GetBinContent(1)/histo->GetBinContent(2))*histo->GetBinContent(3);
                                }
                                if(line_appo.Contains("mc16e")){
                                        sum_of_weight[2] =(histo->GetBinContent(1)/histo->GetBinContent(2))*histo->GetBinContent(3);
                                }
                        }
                }
        }
        infile.close();
        return sum_of_weight;
}


struct myStruct { float pT; int bin; float eta; int ijet;};

map<TString, TString> map_variables_formula;
map<TString, TString> map_variables_label;
map<TString, TString> map_variables_type;



void skim_h027(TString sampleName)
{

        TChain* CollectionTree=new TChain("CollectionTree");

        ifstream fileList(Form("datalist/h027/%s.list",sampleName.Data()));
        TString fileLine;
        while(fileList>>fileLine)  {
                //cout<<fileLine<<endl;
                CollectionTree->Add(fileLine);
        }
        cout<<"sampleName= "<<sampleName<<endl;


        float SOW;
        if (!sampleName.Contains("yy")) {
                string fileName;
                ifstream SOWFile(Form("datalist/h027/%s.list",sampleName.Data()));
                while(getline(SOWFile,fileName))
                {
                        TFile * f1 = new TFile(fileName.c_str());
                        TIter next(f1->GetListOfKeys());
                        TKey *key;
                        while ((key = (TKey*)next())) {
                                TString objName=key->GetName();
                                if(objName.Contains("noDalitz_weighted"))    {
                                        TH1F* cutFlow=(TH1F*)f1->Get(objName);
                                        SOW=cutFlow->GetBinContent(3)*cutFlow->GetBinContent(1)/cutFlow->GetBinContent(2);
                                        break;
                                }
                                else SOW=1.;
                        }
                        f1->Close();
                }
                SOWFile.close();
                cout<<"SOW= "<<SOW<<endl;
        }

        map<TString,float> sum_of_weight;
        sum_of_weight["mc16e_myy"] = 357737408.;
        sum_of_weight["mc16e_ggH"] = 342522912.;
        sum_of_weight["mc16e_ttH"] = 1588790.25;

        CollectionTree->SetMakeClass(1);

        bool passTrig_HLT_g35_medium_g25_medium_L12EM20VH,passTrig_HLT_g35_loose_g25_loose, passTrig_HLT_g120_loose, passTrig_HLT_g140_loose;
        float HGamEventInfoAuxDyn_crossSectionBRfilterEff,HGamEventInfoAuxDyn_yybb_weight,HGamEventInfoAuxDyn_weight,HGamEventInfoAuxDyn_m_yy, HGamEventInfoAuxDyn_yybb_m_yyjj_tilde,HGamEventInfoAuxDyn_BCal_m_yyjj_tilde, HGamEventInfoAuxDyn_BCal_m_jj, HGamEventInfoAuxDyn_yybb_m_jj, HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_highMass_Score, HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score, HGamEventInfoAuxDyn_met_TST, HGamEventInfoAuxDyn_phi_TST, HGamEventInfoAuxDyn_weightFJvt, HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_highMass_Score,HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_lowMass_Score, HGamTruthEventInfoAuxDyn_m_hh, HGamEventInfoAuxDyn_yybb_BCal_planarFlow, HGamEventInfoAuxDyn_yybb_BCal_sphericityT, HGamEventInfoAuxDyn_weightInitial;
        float HGamEventInfoAuxDyn_yybb_BCal_m_yyjj;
        float HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde;
        char HGamEventInfoAuxDyn_isPassedTriggerMatch,HGamEventInfoAuxDyn_isPassedRelPtCuts, HGamEventInfoAuxDyn_isPassed, isPassedJetEventClean, HGamEventInfoAuxDyn_isPassedMassCut, HGamEventInfoAuxDyn_passCrackVetoCleaning;
        int HGamEventInfoAuxDyn_N_j, HGamEventInfoAuxDyn_N_j_central,HGamEventInfoAuxDyn_N_lep, yybb_nonRes_cutBased_btag77_BCal_Cat, HGamEventInfoAuxDyn_yybb_btag77_cutFlow, HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_Cat, HGamEventInfoAuxDyn_yybb_btag77_BCal_vbf_selected, HGamEventInfoAuxDyn_cutFlow, HGamEventInfoAuxDyn_yybb_candidate_jet1_fix, HGamEventInfoAuxDyn_yybb_candidate_jet2_fix, HGamEventInfoAuxDyn_yybb_vbf_jet1, HGamEventInfoAuxDyn_yybb_vbf_jet2, HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_Cat, HGamEventInfoAuxDyn_yybb_btag77_BCal_cutFlow, HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_BCal_VBF_Cat;
        vector<char>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_85=0;
        vector<char>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_77=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_eta=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_phi=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_m=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_85=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_77=0;
        vector<int>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_bin=0;
        vector<char>* HGamPhotonsAuxDyn_isIsoFixedCutLoose=0;
        vector<char>* HGamPhotonsAuxDyn_isTight=0;
        vector<float>* HGamPhotonsAuxDyn_eta=0;
        vector<float>* HGamPhotonsAuxDyn_phi=0;
        vector<float>* HGamPhotonsAuxDyn_pt=0;
        vector<float>* HGamPhotonsAuxDyn_m=0;
        vector<float>* HGamPhotonsAuxDyn_scaleFactor=0;
        vector<int>* HGamPhotonsAuxDyn_conversionType=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_eta=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_pt=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_phi=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_m=0;
        vector<float>* HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_PtRecoSF=0;
        vector<int>     *HGamPhotonsAuxDyn_parentPdgId=0;


        ULong64_t EventInfoAux_eventNumber, EventInfoAuxDyn_eventNumber;	

        CollectionTree->SetBranchStatus("*",0);
        if(sampleName.Contains("mc16")) {
                CollectionTree->SetBranchStatus("HGamEventInfoAuxDyn.crossSectionBRfilterEff",1);
                CollectionTree->SetBranchAddress("HGamEventInfoAuxDyn.crossSectionBRfilterEff",&HGamEventInfoAuxDyn_crossSectionBRfilterEff);
                CollectionTree->SetBranchStatus("HGamPhotonsAuxDyn.parentPdgId", 1);
                CollectionTree->SetBranchAddress("HGamPhotonsAuxDyn.parentPdgId", &HGamPhotonsAuxDyn_parentPdgId);
                CollectionTree->SetBranchStatus("HGamPhotonsAuxDyn.scaleFactor", 1);
                CollectionTree->SetBranchAddress("HGamPhotonsAuxDyn.scaleFactor", &HGamPhotonsAuxDyn_scaleFactor);
        }
        CollectionTree->SetBranchStatus("HGamPhotonsAuxDyn.conversionType", 1);
        CollectionTree->SetBranchAddress("HGamPhotonsAuxDyn.conversionType", &HGamPhotonsAuxDyn_conversionType);
        CollectionTree->SetBranchStatus("HGamPhotonsAuxDyn.eta",1);
        CollectionTree->SetBranchAddress("HGamPhotonsAuxDyn.eta",&HGamPhotonsAuxDyn_eta);
        CollectionTree->SetBranchStatus("HGamEventInfoAuxDyn.weight",1);
        CollectionTree->SetBranchAddress("HGamEventInfoAuxDyn.weight",&HGamEventInfoAuxDyn_weight);
        CollectionTree->SetBranchStatus("HGamEventInfoAuxDyn.weightInitial",1);
        CollectionTree->SetBranchAddress("HGamEventInfoAuxDyn.weightInitial",&HGamEventInfoAuxDyn_weightInitial);
        CollectionTree->SetBranchStatus("HGamEventInfoAuxDyn.m_yy",1);
        CollectionTree->SetBranchAddress("HGamEventInfoAuxDyn.m_yy",&HGamEventInfoAuxDyn_m_yy);
        CollectionTree->SetBranchStatus("HGamEventInfoAuxDyn.cutFlow",1);
        CollectionTree->SetBranchAddress("HGamEventInfoAuxDyn.cutFlow",&HGamEventInfoAuxDyn_cutFlow);
        CollectionTree->SetBranchStatus("HGamEventInfoAuxDyn.isPassed",1);
        CollectionTree->SetBranchAddress("HGamEventInfoAuxDyn.isPassed",&HGamEventInfoAuxDyn_isPassed);

        int y1_matched=0, y2_matched=0;
        int y1_parentPdgId, y2_parentPdgId;
        int y1_conversionType, y2_conversionType;
        float y1_eta, y2_eta;
        float y1_scaleFactor=1., y2_scaleFactor=1.;
        float weightFinal;
        float m_yy;


        vector<int> the_jet;
        ///
        system("mkdir -vp /eos/home-z/zijia/HGamRun3/skim");
        TFile* outFile=new TFile(Form("/eos/home-z/zijia/HGamRun3/skim/%s_01.root",sampleName.Data()),"recreate");
        //TFile* outFile=new TFile(Form("/eos/home-z/zijia/HGamRun3/skim/%s.root",sampleName.Data()),"recreate");
        TTree * outTree = new TTree("skimTree", "skimTree");
        outTree->Branch("weightFinal",&weightFinal);
        outTree->Branch("m_yy",&m_yy);
        outTree->Branch("y1_matched",&y1_matched);
        outTree->Branch("y2_matched",&y2_matched);
        outTree->Branch("y1_parentPdgId",&y1_parentPdgId);
        outTree->Branch("y2_parentPdgId",&y2_parentPdgId);
        outTree->Branch("y1_conversionType",&y1_conversionType);
        outTree->Branch("y2_conversionType",&y2_conversionType);
        outTree->Branch("y1_eta",&y1_eta);
        outTree->Branch("y2_eta",&y2_eta);
        outTree->Branch("y1_scaleFactor",&y1_scaleFactor);
        outTree->Branch("y2_scaleFactor",&y2_scaleFactor);

        map<TString,float> lumi;
        lumi["mc16a"] = 36.64674;
        lumi["mc16d"] = 44.6306;
        lumi["mc16e"] = 58.7916;

        TH1F* cutFlowHist=new TH1F("cutFlow","", 2, 0.5, 2.5);
        TH1I* eventHist=new TH1I("event","", 2, 0.5, 2.5);

        cout<<"numev = "<<CollectionTree->GetEntries()<<endl;
        int count=0;
        for(int iEvt=0;iEvt<CollectionTree->GetEntries();++iEvt)    {
                CollectionTree->GetEntry(iEvt);

                //PrintProgressBar(iEvt, CollectionTree->GetEntries());

                //=====================================
                //========= weight ============
                //=====================================
                //
                cutFlowHist->Fill(1, weightFinal);
                eventHist->Fill(1);

                if (!(bool)HGamEventInfoAuxDyn_isPassed) continue;

                float lumi_to_use = 1.;
                if(sampleName.Contains("mc16a")) {
                        lumi_to_use = lumi["mc16a"];
                }
                else if (sampleName.Contains("mc16d")) {
                        lumi_to_use = lumi["mc16d"];
                }
                else if (sampleName.Contains("mc16e")) {
                        lumi_to_use = lumi["mc16e"];
                }
                else if (sampleName.Contains("mc21a")) {
                        lumi_to_use = lumi["mc21a"];
                }
                if(sampleName.Contains("mc16e_PowhegPy8_ttH125_fixweight")) {
                        SOW = sum_of_weight["mc16e_ttH"];
                }
                else if (sampleName.Contains("mc16e_PowhegPy8_NNLOPS_ggH125")) {
                        SOW = sum_of_weight["mc16e_ggH"];
                }
                else if (sampleName.Contains("mc16e_Sherpa2_diphoton_myy_90_175_AF2")) {
                        SOW = sum_of_weight["mc16e_myy"];
                }

                if (sampleName.Contains("data")) {weightFinal=1;}
                else {
                        weightFinal = lumi_to_use*1000*HGamEventInfoAuxDyn_crossSectionBRfilterEff*HGamEventInfoAuxDyn_weight/SOW;
                }
                //cout<<"lumi_to_use= "<<lumi_to_use<<endl;
                //cout<<"HGamEventInfoAuxDyn_crossSectionBRfilterEff= "<<HGamEventInfoAuxDyn_crossSectionBRfilterEff<<endl;
                //cout<<"HGamEventInfoAuxDyn_weight= "<<HGamEventInfoAuxDyn_weight<<endl;
                //cout<<"SOW= "<<SOW<<endl;
                //cout<<"weightFinal= "<<weightFinal<<endl;

                //cout<<"hello "<<count<<endl;
                m_yy = HGamEventInfoAuxDyn_m_yy;
                y1_eta = (*HGamPhotonsAuxDyn_eta)[0];
                y2_eta = (*HGamPhotonsAuxDyn_eta)[1];
                y1_conversionType = (*HGamPhotonsAuxDyn_conversionType)[0];
                y2_conversionType = (*HGamPhotonsAuxDyn_conversionType)[1];
                if(sampleName.Contains("mc16")) {
                        y1_scaleFactor = (*HGamPhotonsAuxDyn_scaleFactor)[0];
                        y2_scaleFactor = (*HGamPhotonsAuxDyn_scaleFactor)[1];
                        y1_parentPdgId = (*HGamPhotonsAuxDyn_parentPdgId)[0];
                        y2_parentPdgId = (*HGamPhotonsAuxDyn_parentPdgId)[1];
                        //cout<<"HGamPhotonsAuxDyn_parentPdgId: "<<y1_parentPdgId<<" "<<y2_parentPdgId<<endl;
                        if ((*HGamPhotonsAuxDyn_parentPdgId)[0] == 25 ) {
                                y1_matched = 1;
                        }
                        else {
                                y1_matched = 0;}
                        if ((*HGamPhotonsAuxDyn_parentPdgId)[1] == 25 ) {
                                y2_matched = 1;
                        }
                        else {
                                y2_matched = 0;}
                }

                cutFlowHist->Fill(2, weightFinal);
                eventHist->Fill(2);
                outTree->Fill();

                count++;
        }
        //cutFlowHist->Print("all");
        eventHist->Print("all");
        std::cout<<"ZIHANG "<<count<<std::endl;
        outFile->cd();
        cutFlowHist->Write();
        eventHist->Write();
        outTree->Write();
        delete outFile;
        delete CollectionTree;

}
