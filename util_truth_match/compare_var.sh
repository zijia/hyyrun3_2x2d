region="Loose"
var="y1_matched y2_matched"
sample1="mc21a_aMCPy8EG_aa_FxFx_2j_myy_90_175"
sample2="mc21a_PhPy8EG_PDF4LHC21_ggH_MINLO_gammagamma"
sample3="mc21a_PhPy8EG_PDF4LHC21_VBFH125_gammagamma"
sample4="mc21a_PhPy8EG_PDF4LHC21_ggZH125_Zincl"
sample5="mc21a_PhPy8EG_PDF4LHC21_ttH125_tincl"
sample6="mc21a_PhPy8EG_PDF4LHC21_WmH125J_Wincl_MINLO"
sample7="mc21a_PhPy8EG_PDF4LHC21_WpH125J_Wincl_MINLO"
sample8="mc21a_PhPy8EG_PDF4LHC21_ZH125J_Zincl_MINLO"

for v in $var;do
./compare_var ${region} ${v} ${region} ${sample1} ${sample2} ${sample3} ${sample4} ${sample5} ${sample6} ${sample7} ${sample8}
done
